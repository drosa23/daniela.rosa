// a partir desta implementação,
// todos objetos criados com a função String
// poderão chamar esta função. Ex:
// "oi".shimeji()
// let texto = "Kamisama pode ser cruel"
// texto.shimeji()

let funcShimeji = function(uppercase = false) {
  let texto = this + " Shimeji!!!"
  return uppercase ? texto.toUpperCase() : texto
}

String.prototype.shimeji = funcShimeji
Boolean.prototype.shimeji = funcShimeji
console.log("melhor cogumelo é:".shimeji(true))
console.log("melhor cogumelo é:".shimeji(false))
let flag = true
console.log(flag.shimeji()) // true Shimeji!!!
