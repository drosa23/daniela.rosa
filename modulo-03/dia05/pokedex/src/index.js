let pokeApi = new PokeApi()
let ultimoPesquisado

function receberIdPoke(){
  let idPoke = document.getElementById("idPokemon").value;
  buscarPokemon(idPoke);
}

function buscarPokemon(id){
  if( id && id === ultimoPesquisado ){ return } //verfica se o id digitado eh o pokemon que esta aparecendo na tela
  pokeApi.buscar(id)
  .then(pokemonServidor => {
    let poke = new Pokemon(pokemonServidor)
    ultimoPesquisado = id
    renderizaPokemonNaTela(poke)
  })
}

//EXEMPLO ASYNC AWAIT
// async function rodar() {
//   const pokemonServidor = await pokeApi.buscar( 123 )
//   const poke = new Pokemon( pokemonServidor )
//   renderizaPokemonNaTela( poke )
// }
// rodar()

function renderizaPokemonNaTela(pokemon) {
  let dadosPokemon = document.getElementById('dadosPokemon')
  let imagem = document.getElementById('picture')
  //nome
  let nome = dadosPokemon.querySelector('.nome')
  nome.innerText = "Nome: " + pokemon.nome
  //imagem
  let imgPokemon = imagem.querySelector('.thumb')
  imgPokemon.src = pokemon.thumbUrl
  //id
  let idPokemon = dadosPokemon.querySelector('.id')
  idPokemon.innerText = "Id: " + pokemon.id
  //altura
  let altura = dadosPokemon.querySelector('.altura')
  altura.innerText = pokemon.alturaEmCm
  //peso
  let peso = dadosPokemon.querySelector('.peso')
  peso.innerText = pokemon.pesoEmKg
  //tipos
  let tipos = dadosPokemon.querySelector('.tipos')
  tipos.innerText = "Tipos: " + pokemon.tipos
  //estatisticas
  let estatisticas = dadosPokemon.querySelector('.estatisticas')
  estatisticas.innerText = "Estatísticas: " + pokemon.estatisticas
}

function estouComSorte(){
  let numeroSorteado = Math.floor((Math.random() * 802) + 1);
  let itemEncontrado =  window.localStorage.getItem(numeroSorteado.toString());
  if(itemEncontrado != null) {
    estouComSorte();
  }
  window.localStorage.setItem(numeroSorteado.toString(), numeroSorteado.toString())
  buscarPokemon(numeroSorteado)
}
