import React from 'react'
import moment from 'moment'
import Episodio from '../models/episodio';

// https://reactjs.org/docs/components-and-props.html
const EpisodioUi = props => {
  const { episodio } = props
  let episodioEp = new Episodio()
  episodioEp.duracao = episodio.duracao
  episodioEp.temporada = episodio.temporada
  episodioEp.ordemEpisodio = episodio.ordemEpisodio

  return (
    <React.Fragment>
      <h2>{episodio.nome}</h2>
      <img src={episodio.thumbUrl} alt={episodio.nome}></img>
      <span>Já assisti? {episodio.assistido ? 'sim' : 'não'}, {episodio.qtdVezesAssistido} ve(zes)</span>
      <span>Duração: {episodioEp.duracaoEmMinutos}</span>
      <span>Temporada/Episódio: {episodioEp.temporadaEpisodio}</span>
      <span>{episodio.nota || 'Sem Nota'}</span>
      {
        episodio.sinopse &&
        <React.Fragment>
          <span> {episodio.sinopse}</span>
          <span>Data Estréia: { moment(episodio.dataEstreia).format("DD/MM/YYYY")}</span>
          <span>Nota IMDB: {episodio.notaImdb}</span>
        </React.Fragment>
        
      }
    </React.Fragment>
  )
}

export default EpisodioUi

// export default class EpisodioUi extends Component {

//   render() {
//     const { episodio } = this.props
//     return (
//       // <>
//       // </>
//       <React.Fragment>
//         <h2>{ episodio.nome }</h2>
//         <img src={ episodio.thumbUrl } alt={ episodio.nome }></img>
//         <span>Já assisti? { episodio.assistido ? 'sim' : 'não' }, { episodio.qtdVezesAssistido } ve(zes)</span>
//         <span>{ episodio.duracaoEmMin }</span>
//         <span>{ episodio.temporadaEpisodio }</span>
//         <span>{ episodio.nota || 'sem nota' }</span>
//       </React.Fragment>
//     )
//   }
// }  
