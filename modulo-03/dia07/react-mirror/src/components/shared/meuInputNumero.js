import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './meuInputNumero.css'

export default class MeuInputNumero extends Component {

  perderFoco = evt => {
    const { obrigatorio, atualizarValor } = this.props
    const valor = evt.target.value
    const erro = obrigatorio && !valor
    atualizarValor( { valor, erro } )
  }

  render() {
    const { placeholder, visivel, mensagemCampo, deveExibirErro } = this.props
    return visivel ? (
      <React.Fragment>
        {
          mensagemCampo && <span>{ mensagemCampo }</span>
        }
        {
          <input type="number" className={ deveExibirErro ? 'erro' : '' } placeholder={ placeholder } onBlur={ this.perderFoco } />
        }
        {
          deveExibirErro && <span className="mensagem-erro">* obrigatório</span>
        }
      </React.Fragment>
    ) : null
  }
}

MeuInputNumero.propTypes = {
  visivel: PropTypes.bool.isRequired,
  atualizarValor: PropTypes.func.isRequired,
  deveExibirErro: PropTypes.bool.isRequired,
  obrigatorio: PropTypes.bool,
  mensagemCampo: PropTypes.string,
  placeholder: PropTypes.string,
}

MeuInputNumero.defaultProps = {
  obrigatorio: false
}