import React, { Component } from 'react'
import './mensagemFlash.css'
import PropTypes from 'prop-types'

export default class MensagemFlash extends Component {

  constructor( props ) {
    super( props )
    this.idsTimeouts = []
    this.animacao = ''
  }

  fechar = () => {
    this.props.atualizarMensagem( false )
  }

  limparTimeouts() {
    this.idsTimeouts.forEach( clearTimeout )
  }

  // a casa vai cair
  componentWillUnmount() {
    this.limparTimeouts()
  }

  componentDidUpdate( prevProps ) {
    const { deveExibirMensagem, segundos } = this.props
    if ( prevProps.deveExibirMensagem !== deveExibirMensagem ) {
      this.limparTimeouts()
      const novoIdTimeout = setTimeout( () => {
        this.fechar()
      }, segundos * 1000 )
      this.idsTimeouts.push( novoIdTimeout )
    }
  }

  render() {
    const { deveExibirMensagem, mensagem, cor } = this.props

    if( this.animacao || deveExibirMensagem){
      this.animacao = deveExibirMensagem ? 'fade-in' : 'fade-out'
    }

    return <span onClick={ this.fechar } className={ `flash ${ cor } ${ this.animacao }` }>{ mensagem }</span>
  }
}

// https://reactjs.org/docs/typechecking-with-proptypes.html
MensagemFlash.propTypes = {
  atualizarMensagem: PropTypes.func.isRequired,
  deveExibirMensagem: PropTypes.bool.isRequired,
  mensagem: PropTypes.string.isRequired,
  cor: PropTypes.oneOf( ['verde', 'vermelho'] ),
  segundos: PropTypes.number,
}

MensagemFlash.defaultProps = {
  cor: 'verde',
  segundos: 3
}
