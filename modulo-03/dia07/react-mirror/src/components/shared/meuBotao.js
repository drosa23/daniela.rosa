import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './meuBotao.css'
import PropTypes from 'prop-types'

export default class MeuBotao extends Component {

  geraHtmlInterno = ( { link, textoBotao, dadosNavegacao } ) => {
    return link ? (
      <Link to = {{ pathname: link, state: dadosNavegacao}}>{ textoBotao }  </Link>
    ) : textoBotao

  }

  render() {
    const { corBotao, textoBotao, funcBotao, link, dadosNavegacao } = this.props
    return (
      <React.Fragment>
        <button className={`btn ${corBotao}`} onClick={ funcBotao }>
        { this.geraHtmlInterno( { link, textoBotao, dadosNavegacao } ) }
        </button>
      </React.Fragment>
    )
  }
}

MeuBotao.propTypes = {
  textoBotao: PropTypes.string.isRequired,
  corBotao: PropTypes.oneOf(['verde', 'azul', 'vermelho']),
  funcBotao: PropTypes.func,
  link: PropTypes.string
}

MeuBotao.defaultProps = {
  cor: 'verde'
}
