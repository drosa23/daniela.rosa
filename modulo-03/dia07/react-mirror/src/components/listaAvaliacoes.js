import React, { Component } from 'react';
import ListaEpisodios from '../models/listaEpisodios';
import { Link } from 'react-router-dom'

export default class ListaAvaliacoes extends Component {
  render() {
     let listaEpisodios = new ListaEpisodios(this.props.location.state.todos)
    return listaEpisodios.ordenarEpisodiosTempEpiCrescente().map(e => {
      return (
        <li key= {e.nome}>
           <Link to = {{ pathname: `/episodio/${e.id}`, state: e}}>{ `${ e.nome } - ${ e.nota }` }</Link>
        </li>
      )
    })
  }
}

