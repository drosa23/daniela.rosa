import React, { Component } from 'react';
import EpisodiosApi from '../models/episodiosApi';
import EpisodioUi from '../components/episodioUi'
import ListaEpisodios from '../models/listaEpisodios'

export default class TelaDetalheEpisodio extends Component {
  constructor(props) {
    super(props)
    this.state = {
      episodio: ""
    }
  }

  componentDidMount() {
    const episodiosApi = new EpisodiosApi()
 
    this.id = this.props.match.params.id
    // TODO: passar para Promise.all
    episodiosApi.getEpisodios().then(episodiosDoServidor => {
      this.listaEpisodios = new ListaEpisodios(episodiosDoServidor.data)
      this.episodioDados = this.listaEpisodios.todos[(this.id - 1)]
      this.episodio = Object.assign({}, this.episodioDados)
      episodiosApi.getEpisodioDetalhes(this.id).then(episodioDetalhes => {
        this.episodioDetalhes = episodioDetalhes.data[0]
        this.episodio.sinopse =  episodioDetalhes.data[0].sinopse
        this.episodio.dataEstreia =  episodioDetalhes.data[0].dataEstreia
        this.episodio.notaImdb =  episodioDetalhes.data[0].notaImdb
        const teste = Object.assign( this.episodio, episodioDetalhes.data[0] )
        const teste2 = { ...this.episodio, ...episodioDetalhes.data[0] }
        this.setState({ episodio: this.episodio })
      })

    })
  }

  render() {
    const { episodio } = this.state
    return (
      <React.Fragment>
        {episodio && <EpisodioUi episodio={episodio} />}
      </React.Fragment>
    )
  }
}
