import React, { Component } from 'react';
import ListaEpisodios from '../models/listaEpisodios';
import { Link } from 'react-router-dom'
import MeuBotao from './shared/meuBotao';
import EpisodiosApi from '../models/episodiosApi'

export default class TodosEpisodios extends Component {

  constructor(props) {
    super(props)
    this.episodiosApi = new EpisodiosApi()
    
    let episodios = this.props.location.state.todos
    const requisicoesNotas = episodios.map(e => this.episodiosApi.getNota(e.id))
    Promise.all(requisicoesNotas).then(resultadosNotas => {
      episodios.forEach(e => {
        const notas = resultadosNotas.find(r => r.data[0].episodioId === e.id)
        let lastIndex = notas.data.length - 1
        e.nota = notas.data[lastIndex].nota
      })
    })

    this.state = {
      dataEstreia: "decrescente",
      duracao: "decrescente",
      inicial: true,
      aparecerDuracao: false,
      aparecerDataEstreia: false,
      funcaoOrdenacao: () => { },
      listaEpisodios: new ListaEpisodios(episodios)
    }
  }

  componentDidMount() {
    let episodios = this.props.location.state.todos
    const requisicoes = episodios.map(e => this.episodiosApi.getEpisodioDetalhes(e.id))
    Promise.all(requisicoes).then(resultados => {
      episodios.forEach(e => {
        const detalhes = resultados.find(r => r.data[0].episodioId === e.id)
        e.dataEstreia = detalhes.data[0].dataEstreia
      })
      
      
      this.setState({
        listaEpisodios: new ListaEpisodios(episodios),
      })
    })
  }

  ordenarDataEstreia = () => {
    const { dataEstreia } = this.state
    this.setState({
      funcaoOrdenacao: (a, b) => dataEstreia === "crescente" ? new Date(a.dataEstreia) - new Date(b.dataEstreia) : new Date(b.dataEstreia) - new Date(a.dataEstreia),
      dataEstreia: dataEstreia === "crescente" ? "decrescente" : "crescente"
    })

  }

  ordenarDuracao = () => {
    const { duracao } = this.state
    this.setState({
      funcaoOrdenacao: (a, b) => duracao === "crescente" ? a.duracao - b.duracao : b.duracao - a.duracao,
      duracao: duracao === "crescente" ? "decrescente" : "crescente"
    })
  }


  render() {
    return (
      <div>
        <h2>Todos Episódios de Black Mirror</h2>
        {
          this.state.listaEpisodios.todos.sort(this.state.funcaoOrdenacao).map(e => {
            let nota;
            !e.nota ? nota = "não avaliado" : nota = e.nota;
            return (
              <li key={e.nome}>
                <Link to={{ pathname: `/episodio/${e.id}`, state: e }}>{`${e.nome} - ${nota}`}</Link>
              </li>
            )
          })
        }
        <MeuBotao corBotao="verde" textoBotao="Ordenar por data de estréia" funcBotao={this.ordenarDataEstreia} />
        <MeuBotao corBotao="azul" textoBotao="Ordenar duração" funcBotao={this.ordenarDuracao} />
      </div>
    )
  }
}
