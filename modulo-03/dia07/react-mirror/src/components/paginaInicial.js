import React, { Component } from 'react';
import ListaEpisodios from '../models/listaEpisodios'
import EpisodioUi from '../components/episodioUi'
import MensagemFlash from '../components/shared/mensagemFlash'
import MeuInputNumero from '../components/shared/meuInputNumero'
import MeuBotao from '../components/shared/meuBotao';
import EpisodiosApi from '../models/episodiosApi';

export default class PaginaInicial extends Component {
  constructor(props) {
    super(props)
    const episodiosApi = new EpisodiosApi()
    // TODO: componentDidMount
    episodiosApi.getEpisodios().then(episodiosDoServidor => {
      this.listaEpisodios = new ListaEpisodios(episodiosDoServidor.data)
      this.setState({ episodio: this.listaEpisodios.episodioAleatorio })
    })
    // definindo estado inicial de um componente
    this.state = {
      deveExibirMensagem: false,
      mensagem: '',
      deveExibirErro: false
    }
  }

  sortear = () => {
    const episodio = this.listaEpisodios.episodioAleatorio
    this.setState({
      episodio, deveExibirMensagem: false
    })
  }

  marcarComoAssistido = () => {
    const { episodio } = this.state
    this.listaEpisodios.marcarComoAssistido(episodio)
    this.setState({
      episodio,
    })
  }

  exibeMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor, mensagem, deveExibirMensagem: true
    })
  }

  registrarNota = ({ valor, erro }) => {
    this.setState({
      deveExibirErro: erro
    })
    if (erro) {
      // se tiver erro no campo, atualizamos estado com exibição e já retornamos,
      // para não rodar o código como se estivesse válida a obrigatoriedade
      return
    }
    const { episodio } = this.state
    let cor, mensagem
    if (episodio.validarNota(valor)) {
      episodio.avaliar(valor).then(() => {
        cor = 'verde'
        mensagem = 'Registramos sua nota!'
        this.exibeMensagem({ cor, mensagem })
      })

    } else {
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida (entre 1 e 5)'
      this.exibeMensagem({ cor, mensagem })
    }
    
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      deveExibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, deveExibirMensagem, mensagem, cor, deveExibirErro } = this.state

    return (
      !episodio ?
        <h2>Aguarde...</h2> :
        (
          <React.Fragment>
            <MensagemFlash atualizarMensagem={this.atualizarMensagem} cor={cor} deveExibirMensagem={deveExibirMensagem} mensagem={mensagem} segundos={5} />

            {episodio && <EpisodioUi episodio={episodio} />}

            <MeuInputNumero placeholder="1 a 5"
              mensagemCampo="Qual sua nota para este episódio?"
              obrigatorio={true}
              atualizarValor={this.registrarNota}
              deveExibirErro={deveExibirErro}
              visivel={episodio && episodio.assistido}
            />

            <div className="botoes">
              <MeuBotao corBotao="verde" textoBotao="Próximo" funcBotao={this.sortear} />
              <MeuBotao corBotao="azul" textoBotao="Já assisti" funcBotao={this.marcarComoAssistido} />
              <MeuBotao corBotao="vermelho" textoBotao="Ver Notas!" link="avaliacoes" dadosNavegacao={this.listaEpisodios} />
            </div>

            <div>
              <MeuBotao corBotao="azul" textoBotao="Todos Episódios" link="todosEpisodios" dadosNavegacao={this.listaEpisodios} />
            </div>

          </React.Fragment>
        )
    )
  }
}
