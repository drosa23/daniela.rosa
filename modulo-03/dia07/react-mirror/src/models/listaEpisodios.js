import Episodio from "./episodio";

// inspiração: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#Getting_a_random_integer_between_two_values
// essa função não está sendo exportada, logo ela é "privada"
function _sortear(min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min
}

export default class ListaEpisodios {
  constructor(todos) {
    this.todos = todos.map(ep => (new Episodio(ep.id, ep.nome, ep.duracao, ep.temporada, ep.ordemEpisodio, ep.thumbUrl, ep.qtdVezesAssistido, ep.nota, ep.sinopse, ep.dataEstreia, ep.notaImdb))
    )
  }

  get episodioAleatorio() {
    const indice = _sortear(0, this.todos.length)
    return this.todos[indice]
  }

  //ordenar por tempora/episodio em ordem crescente
  ordenarEpisodiosTempEpiCrescente() {
    return this.avaliados.sort(function (a, b) {
      if (a.temporada !== b.temporada) {
        return a.temporada - b.temporada
      }
      else {
        return a.ordemEpisodio - b.ordemEpisodio
      }
    })
  }

  get avaliados() {
    return this.todos.filter(e => e.nota)
  }

  marcarComoAssistido(episodio) {
    const episodioParaMarcar = this.todos.find(e => e.nome === episodio.nome)
    episodioParaMarcar.assistido = true
    episodioParaMarcar.qtdVezesAssistido++;
  }
}
