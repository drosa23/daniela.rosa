import axios from 'axios'
export default class EpisodiosApi {
  getEpisodios() {
    return axios.get('http://localhost:9000/episodios')
  }
  getEpisodioDetalhes(id) {
    return axios.get(`http://localhost:9000/episodios/${id}/detalhes`)
  }

  registrarNota({ nota, episodioId }) {
    return axios.post('http://localhost:9000/api/notas', { nota, episodioId })
  }

  getNota(id){
    return axios.get(`http://localhost:9000/api/notas?episodioId=${id}`)
  }
  // obter nota de episódio: http://localhost:9000/api/notas?episodioId=11

}
