import EpisodiosApi from './episodiosApi'

export default class Episodio {
  constructor(id, nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido, nota, sinopse, dataEstreia, notaImdb) {
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordemEpisodio = ordemEpisodio;
    this.thumbUrl = thumbUrl;
    this.qtdVezesAssistido = qtdVezesAssistido || 0;
    this.episodiosApi = new EpisodiosApi()
    this.nota = nota || undefined
    this.sinopse = sinopse || undefined
    this.dataEstreia = dataEstreia || undefined
    this.notaImdb = notaImdb || undefined
  }

  validarNota( nota ){
    nota = parseInt( nota )
    return nota >= 1 && nota <=5
  }

  avaliar( nota ){
    this.nota = parseInt( nota )
    this.assistido = true;
    return this.episodiosApi.registrarNota( { nota: this.nota, episodioId: this.id } )
  }

  get duracaoEmMinutos() {
    return ` ${this.duracao }min `
  }

  get temporadaEpisodio(){
    return `${ this.temporada.toString().padStart(2,'0') }/${ this.ordemEpisodio.toString().padStart(2,'0')}`
  }
}
