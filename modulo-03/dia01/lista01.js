// Exercicio1
function calcularCirculo ([raio, tipoCalculo]){
    if(tipoCalculo === "A"){
        return Math.PI * (raio*raio);
    }
    else if(tipoCalculo === "C"){
        return (2 * Math.PI) * raio;
    }
} 
console.log( "Teste exercicio 1 com C: " + calcularCirculo([raio= 3 , tipoCalculo= "C" ]));
console.log( "Teste exercicio 1 com A: " + calcularCirculo([raio= 3 , tipoCalculo= "A" ]));

// Exercicio2
function naoBissexto (ano){
    if((ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0))){
        return false;
    }
    return true;
}
console.log( "Teste exercicio 2 com bissexto: " + naoBissexto(2016));
console.log( "Teste exercicio 2 com naobissexto: " + naoBissexto(2017));

// Exercicio3
function somarPares(numeros){
    let resultado = 0;
    for(let i = 0; i < numeros.length; i++){
        if (i % 2 === 0){
            resultado = resultado + numeros[i];
        }
    }
    return resultado;
}
console.log( "Teste exercicio 3 deve retornar 4: " + somarPares([1, 2, 3, 4]));

// Exercicio4
// function adicionar(numero) {
//     function dentro(outroNumero) {
//        return numero + outroNumero;
//     }
//     return dentro;
//  }

 let adicionar = op1 => op2 => op1 + op2;
 console.log( "Teste exercicio 4 deve retornar 5: " + adicionar(3)(2));

// Exercicio5
// function imprimirBRL (numero){
//     return numero.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
// }

function arredondar(numero, precisao = 2) {
  const fator = Math.pow( 10, precisao )
  return Math.ceil( numero * fator ) / fator
}

function imprimirBRL(numero) {
  let qtdCasasMilhares = 3
  let separadorMilhar = '.'
  let separadorDecimal = ','

  let stringBuffer = []
  let parteDecimal = arredondar(Math.abs(numero)%1)
  let parteInteira = Math.trunc(numero)
  let parteInteiraString = Math.abs(parteInteira).toString()
  let parteInteiraTamanho = parteInteiraString.length

  let c = 1
  while (parteInteiraString.length > 0) {
      if ( c % qtdCasasMilhares === 0 ) {
          stringBuffer.push( `${ separadorMilhar }${ parteInteiraString.slice( parteInteiraTamanho - c ) }` )
          parteInteiraString = parteInteiraString.slice( 0, parteInteiraTamanho - c )
      } else if ( parteInteiraString.length < qtdCasasMilhares ) {
          stringBuffer.push( parteInteiraString )
          parteInteiraString = ''
      }
      c++
  }
  stringBuffer.push( parteInteiraString )

  let decimalStr = parteDecimal.toString().replace('0.', '').padStart(2, '0')
  return `${ parteInteira >= 0 ? 'R$ ' : '-R$ ' }${ stringBuffer.reverse().join('') }${ separadorDecimal }${ decimalStr }`
}

console.log(imprimirBRL(0)) // "R$ 0,00"
console.log(imprimirBRL(3498.99)) // “R$ 3.498,99”
console.log(imprimirBRL(-3498.99)) // “-R$ 3.498,99”
console.log(imprimirBRL(2313477.0135)) // “R$ 2.313.477,02”
