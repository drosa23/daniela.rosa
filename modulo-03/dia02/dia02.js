console.log(assim())

function assim() {
    console.log("function assim")
}

let assimFn = function() {
    console.log("let assimFn")
}

console.log(assimFn())

//bugEscopo()

function bugEscopo() {
    // for (let i = 0; i < 5; i++) {
    //     console.log(i)
    // }
    // console.log("i é:" + ++i)
    for (let i = 0; i < 5; i++) {
        console.log(i)
    }
    console.log("i é:" + ++i)
}

// const goku = { nome: "Goku" }
// goku.nome = "Son Goku"
// goku = { name: "Other Goku" } // erro

// https://developer.mozilla.org/en-US/docs/Glossary/Truthy
// https://developer.mozilla.org/en-US/docs/Glossary/Falsy

let nome = "Bernardo"
let cidade = "Porto Alegre"
let objeto = { nome, cidade };

function imprimirCidade( cidade = "Londres" ) {
    console.log(cidade)
}
imprimirCidade()



// IIFE - https://developer.mozilla.org/en-US/docs/Glossary/IIFE
!(function() {
    console.log(arguments[0])
    console.log(`Gostando de JS? ${ arguments[1].gostandoDeJs === true ? 'sim' : 'não' }`)
    function interna() {
        let x = 'x'
        return x
    }
    console.log(`o valor de x é: ${ interna() }!!`)
    let x
})('JavaScript é legal!', { nome: 'Nome', gostandoDeJs: 'VOLTA BLUEJ PMDZ' })
//externa()









//let hello = 'global'
// function start() {
//     // console.log(hello)
//     // let hello = 'oi'
//     let hello
//     console.log(hello)
//     hello = 'oi'
// }
// start()
