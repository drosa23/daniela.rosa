function cardapioTiaIFood( veggie = true, comLactose = false ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
  
    if (comLactose ) {
      cardapio.push( 'pastel de queijo' );
    }
    if ( typeof cardapio.concat !== 'function' ) {
      cardapio = {}, cardapio.concat = () => []
    }
  
    cardapio = cardapio.concat( [
      'pastel de carne',
      'empada de legumes marabijosa'
    ] )
  
    if ( veggie ) { 
      cardapio.splice(cardapio.indexOf( 'enroladinho de salsicha' ), 1);
      cardapio.splice(cardapio.indexOf( 'pastel de carne' ), 1);
    }
    return cardapio;
  }
  
  console.log("cardapio final: " + cardapioTiaIFood()); // esperado: [ 'cuca de uva', 'empada de legumes marabijosa' ]
