var series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]')
console.log(series)
/*
Exercício 01 - Séries Inválidas

Nesse exercício deverá ser implementada uma função na prototype de Array chamada invalidas() que verifica quais são as 
séries inválidas e retorna seus títulos em formato String. Exemplo:
series.invalidas() // retorna "Séries Inválidas: Série 1 - Série 2 ..."
Condições para as séries serem inválidas:
Ano de estreia maior que o ano atual;
Possuir algum campo que seja undefined ou null.
*/ 
data = new Date();
let invalidas = function(){
  let seriesInv = this.filter( serie => {
    for (var column in serie) {
        if(typeof serie[column] === undefined || serie[column] === null || serie.anoEstreia > data.getFullYear()){
          return serie;
        }
    }
  })
  let resultado = "Séries Inválidas: "
  seriesInv.map (s => {
    resultado += s.titulo + " - ";
  })
  return resultado
}
Array.prototype.invalidas = invalidas

//SOLUCAO BERNARDO
Array.prototype.seriesInvalidas = function(){
const invalidas = this.filter(serie => {
  const algumCampoInvalido = Object.values( serie )
  .some( v => v === null || typeof v === 'undefined')
  const anoEstreiaInvalido = serie.anoEstreia > data.getFullYear();
  return algumCampoInvalido || anoEstreiaInvalido
  })
  return `Séries inválidas: ${ invalidas.map( s => s.titulo.join( ' - ' ) ) }`
}


/*
Exercício 02 - Séries a partir de um determinado ano
Nesse exercício deverá ser implementada uma função na prototype de Array chamada filtrarPorAno(ano) que recebe o ano (number) e devolve um outro array contendo
 apenas as séries com ano de lançamento maior ou igual ao ano passado por parâmetro.
Exemplo:
series.filtrarPorAno(2017) // retorna um array com todas as séries com ano de estreia igual ou maior que 2017.
*/ 
let filtrarPorAno = function(ano){
   let resultado = this.filter ( s => {
    let anoSerie = parseInt(s.anoEstreia)
    if( anoSerie >= ano){
      return s
    }
  })
  return resultado
}
 Array.prototype.filtrarPorAno = filtrarPorAno

/*
Exercício 03 - Eu sou um ator de séries?
Crie uma função, na prototype de Array, chamada procurarPorNome(nome) que recebe um um nome e caso esse nome esteja no elenco das séries, 
retorna true. Caso contrário, false.
Exemplo:
serie.procurarPorNome("Bernardo") // retorna true se tiver um "Bernardo" em algum dos elencos
*/
let procurarPorNome = function(nome){
  let elencos = this.map( e => {
    return e.elenco;
  })
  let resultado;
  elencos.map( a => {
    a.map( ator => {
      if(!resultado){
        resultado = ator.indexOf(nome) > -1
      }
    })
  })
  return resultado;
}
Array.prototype.procurarPorNome = procurarPorNome

/*
Exercício 04 - Média de Episódios
Crie uma função, na prototype de Array, chamada mediaDeEpisodios() que retorna a média dos episódios de todas as séries contidas no array.
Exemplo:
series.mediaDeEpisodios(); // retorna o valor da média da soma dos episódios/quantidade de séries no array.
*/

let mediaDeEpisodios = function(){
  let quantidadeTotal = 0;
  this.map (serie => {
    quantidadeTotal += serie.numeroEpisodios; 
  })
  let qntdSeries = series.length;
  return quantidadeTotal/qntdSeries;
}
Array.prototype.mediaDeEpisodios = mediaDeEpisodios

//SOLUCAO BERNARDO
Array.prototype.media = function(){
  series.map( s => s.numeroEpisodios).reduce( (acc, elem ) => acc + elem ) / series.length
}

/*
Exercício 05 - Mascada em Série
Uma série tem seu elenco e diretor(es), mas para ela acontecer, eles devem ser pagos. Crie uma função, na prototype de Array, chamada totalSalarios 
que recebe o índice da série no array e retornará o valor total do salário a ser pago por mês. Para isso, suponha que os Big-Bosses, os Diretores, 
ganhem R$ 100.000; Enquanto os operarios (cada um) ganha R$ 40.000;
Exemplo:
series.totalSalarios(0); //Retorna o valor total de gastos da série na posição 0, contando os diretores e o elenco
*/
let totalSalarios = function(indice){
  let serie = this[indice];
  let qtndDiretores = serie.diretor.length;
  let qntdAtores = serie.elenco.length;
  let salarioDiretores = 100000 * qtndDiretores;
  let salarioAtores = 40000 * qntdAtores;
  return salarioAtores + salarioDiretores
}
Array.prototype.totalSalarios = totalSalarios
let aa = series.totalSalarios(0);
// console.log(aa)

/*
Exercício 06 - Buscas!
A) Não sei o que quero assitir, mas quero ver O CAOS! Escreva uma função, na prototype de Array, chamada queroGenero que retorne um array, com os 
títulos das séries que são correspondentes com o genero do parâmetro.
Exemplo:
series.queroGenero("Caos"); // Retorna ["The JS Mirror", "10 Days Why"]
B) Sei exatamente o que quero assisitir! Escreva uma função, na prototype de Array, chamada queroTitulo que retorne um array, com os títulos das 
séries que tem título semelhante ao passado
Exemplo:
serie.queroTitulo("The"); // Retorna ["The Walking Dead", "The JS Mirror"]
*/
let queroGenero = function(genero){
  let series = this.filter( serie =>{
    let encontrou;
    serie.genero.map( g => {
      if(g === genero){
        encontrou = true;
      }
    })
    if(encontrou){
      return serie
    }
    
  })
  let resultado = series.map( e => { return e.titulo })
  return resultado

}
Array.prototype.queroGenero = queroGenero


/*
Exercício 07 - Créditos
Ao final de um episódio, temos os créditos do episódio. Para isso vamos implementar uma função, na prototype de Array,
 chamada de creditos que imprima os créditos da série. Deverá ser impresso, o Título da serie, os Diretores, avisando com um 
 título que é o bloco deles. Em seguida vem o elenco, também com um título de Elenco.
Tranquilo né? Barbadinha! MAS, tem o seguinte: Os créditos são sempre ordenados alfabeticamente, mas pelo ÚLTIMO NOME!! 
Faça os ajustes necessários para que isso seja possível.
*/




/*
Exercício 08 - hashtag secreta
Nos elencos das séries, quando abreviamos um nome colocamos a primeira letra do nome seguida de um ponto final
Exemplo:
Bernardo Bosak Rezende -> Bernardo B. Rezende
Tuanni Carvalho Miranda -> Tuanni C. Miranda
Essa é a informação básica! Construa uma função, no prototype Array, que identificará aquela série que tem TODOS do elenco 
com nomes abreviados.
Dica: Construa uma função separada para identificar se aquela String tem a abreviação;
Show de bola, estamos quase lá!
Uma vez achada a série, vamos modificar um pouquinho a implementação. Coloque todas as palavras abreviadas
 (de preferência sem os pontos finais) 
em uma string que será retornada ao final da função.
Forme uma hashtag com a palavra! #PALAVRA resultante


*/

