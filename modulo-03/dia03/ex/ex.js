let interruptor;
const helsinki = 'Europe/Helsinki';
const tokyo = 'Asia/Tokyo';
const berlin = 'Europe/Berlin';
const athens = 'Europe/Athens';
const beirut = 'Asia/Beirut';
const dataComp = 'LLL'
const horaExata = 'LTS'

function fusoPais(local, formato) {
   return moment.tz(local).format(formato);
}

function clicouFin() {
   alert(fusoPais(helsinki, dataComp))
}

function clicouJap() {
   alert(fusoPais(tokyo, dataComp))
}

function clicouAle() {
   alert(fusoPais(berlin, dataComp))
}

function clicouGre() {
   alert(fusoPais(athens, dataComp))
}

function clicouLib() {
   alert(fusoPais(beirut, dataComp))
}

function atualiza() {
   setTimeout(() => {
       temposPaises()
   }, 1000
   )
}

function temposPaises() {
   if (interruptor) {
       return;
   }

   document.getElementById('finTempo').value = fusoPais(helsinki, horaExata);
   document.getElementById('japTempo').value = fusoPais(tokyo, horaExata);
   document.getElementById('aleTempo').value = fusoPais(berlin, horaExata);
   document.getElementById('greTempo').value = fusoPais(athens, horaExata);
   document.getElementById('libTempo').value = fusoPais(beirut, horaExata);

   atualiza();
}


function pararTempo() {
   if (interruptor) {
       interruptor = false;
       temposPaises();
   } else {
       interruptor = true;
   }
}
