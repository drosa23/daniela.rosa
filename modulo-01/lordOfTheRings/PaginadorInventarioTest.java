import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class PaginadorInventarioTest{
    @Test
    public void pular2Limitar2(){
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo de metal");
        Item pocao = new Item(3, "Poção de HP");
	Item bracelete = new Item(4, "Bracelete");
        
        Inventario inventario = new Inventario();
	inventario.adicionar(espada);
	inventario.adicionar(escudo);
	inventario.adicionar(pocao);
	inventario.adicionar(bracelete);
	
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	
	ArrayList<Item> itensPocaoBracelete = new ArrayList<>();
	itensPocaoBracelete.add(pocao);
	itensPocaoBracelete.add(bracelete);
	
	//Act
	paginador.pular(2);
	assertEquals(itensPocaoBracelete, paginador.limitar(2)); 
    }
    
    @Test
    public void pularLimitarComInventarioVazio(){
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(4);
        assertNull(paginador.limitar(10));
    }
    
    @Test
    public void pular3Limitar5InvMenor(){
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo de metal");
        Item pocao = new Item(3, "Poção de HP");
	Item bracelete = new Item(4, "Bracelete");
        
        Inventario inventario = new Inventario();
	inventario.adicionar(espada);
	inventario.adicionar(escudo);
	inventario.adicionar(pocao);
	inventario.adicionar(bracelete);
	
	PaginadorInventario paginador = new PaginadorInventario(inventario);
	
	ArrayList<Item> itensPocaoBracelete = new ArrayList<>();
	itensPocaoBracelete.add(pocao);
	itensPocaoBracelete.add(bracelete);
	
	//Act
	paginador.pular(3);
	assertNull(paginador.limitar(5)); 
    }
}
