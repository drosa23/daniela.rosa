public class Elfo extends Personagem{
    protected int experiencia, qtdExperienciaPorAtaque;
    private static int contadorInstancias;
    public Elfo (String nome) {
        super(nome);
        this.mochila.adicionar(new Item(1, "Arco"));
        this.mochila.adicionar(new Item(2, "Flecha"));
        this.vida = 100.0;
        this.qtdDano = 0.0;
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        contadorInstancias++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.contadorInstancias--;
    }  
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public Item getFlechas(){
        return this.mochila.obter(1);
    }
    
    public double getVida(){
        return this.vida;
    }
    
    protected void aumentaExperiencia () {
        this.experiencia += this.qtdExperienciaPorAtaque;
    }
    
    protected boolean podeAtirarFlechas(){
        return this.mochila.obter(1).getQuantidade()>0;
    }
    
    public void estaMorto(){
        this.status = Status.MORTO;
        this.vida = 0;
    }
    
    public void atiraFlecha(Dwarf anao){        
        if(!podeAtirarFlechas()){
            System.out.println ("Nao ha flechas suficientes");
        }
        else if(anao.getStatus() != Status.MORTO){
            int quantidade = this.mochila.obter(1).getQuantidade();
            this.mochila.obter(1).setQuantidade(quantidade - 1);
            aumentaExperiencia();
            this.sofreDano();
            anao.sofreDano();
        }
    }
    
    public int getContadorDeInstancias(){
        return this.contadorInstancias;
    }
    
    
    
    
}   