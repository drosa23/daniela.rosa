import java.util.*;
public class PaginadorInventario{
    private Inventario inventario = new Inventario();
    private int marcador = 0;
    //atributo q recebe o q vier no pular, dai o outro so pega o atributo e soma pro get
    public PaginadorInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public int getMarcador(){
        return this.marcador;
    }
    
    public void pular(int n){
        this.marcador = n > 0 ? n : 0;
    }
    
    public ArrayList<Item> limitar (int n){
        ArrayList<Item> itensLimitados = new ArrayList<>();
        for(int i = marcador; i < this.marcador+n; i++){
            if(i < this.inventario.getTamanho() && !this.inventario.estaVazio()){
                Item item = this.inventario.obter(i);
                itensLimitados.add(item);
            }
            else{return null;}
        }
        return itensLimitados;
    }
}
