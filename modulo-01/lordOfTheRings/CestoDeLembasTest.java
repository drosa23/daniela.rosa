import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CestoDeLembasTest{
   
    @Test 
    public void podeDividir30DeveTrue(){
        CestoDeLembas cesto = new CestoDeLembas(30);
        boolean resultado = cesto.podeDividirEmPares();
        assertTrue(resultado);
    }
    
    @Test 
    public void podeDividir6DeveTrue(){
        CestoDeLembas cesto = new CestoDeLembas(6);
        boolean resultado = cesto.podeDividirEmPares();
        assertTrue(resultado);
    }
    
    @Test 
    public void podeDividir2DeveFalse(){
        CestoDeLembas cesto = new CestoDeLembas(2);
        boolean resultado = cesto.podeDividirEmPares();
        assertFalse(resultado);
    }
    
    @Test 
    public void podeDividir100DeveTrue(){
        CestoDeLembas cesto = new CestoDeLembas(100);
        boolean resultado = cesto.podeDividirEmPares();
        assertTrue(resultado);
    }
    
    @Test 
    public void podeDividir11DeveFalse(){
        CestoDeLembas cesto = new CestoDeLembas(11);
        boolean resultado = cesto.podeDividirEmPares();
        assertFalse(resultado);
    }
}
