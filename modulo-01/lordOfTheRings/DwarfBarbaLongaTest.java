import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfBarbaLongaTest{
    @Test
    public void tem33deChanceDeTomarDano(){
        int cont = 0;
        for(int i = 0; i< 100000; i++){
            Elfo elfo = new Elfo("a");
            DwarfBarbaLonga anao = new DwarfBarbaLonga("a");
            double vidaAntes = anao.getVida();
            elfo.atiraFlecha(anao);
            double vidaDepois = anao.getVida();
            if(vidaDepois == vidaAntes){
                cont ++;
            }
        }
        System.out.println(cont);
        boolean resultado = cont > 32000 && cont < 35000;
        assertTrue(resultado);
    }
    
}
