import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    
    @Test
    public void dwarfNasceCom110Vida (){
        Dwarf umDwarf = new Dwarf("Lotroli Alemaker");
        assertEquals(110.0, umDwarf.getVida(), 0.0);
    }
    
    @Test
    public void verificaLevaFlechadaCom110DeVida(){
        Dwarf umDwarf = new Dwarf("Gimli");
        
        umDwarf.sofreDano();
        
        assertEquals(100.0,umDwarf.getVida(), 0.0 );
    }
    
    @Test
    public void verificaLevaFlechadaCom5DeVida(){
        Dwarf umDwarf = new Dwarf("Gimli");
        umDwarf.setVida(5);
        
        umDwarf.sofreDano();
        
        assertEquals(0,umDwarf.getVida(), 0.0 );
    }
    
    @Test
    public void dwarfNascecomStatusRecemCriado(){
        Dwarf umDwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO, umDwarf.getStatus());
    }
    
    @Test
    public void dwarfTomaDanoERecebeStatusSofreuDano(){
        Dwarf umDwarf = new Dwarf("Gimli");
        umDwarf.sofreDano();
        assertEquals(Status.SOFREU_DANO, umDwarf.getStatus());
    }
    
    @Test
    public void dwarfComStatusMorto(){
        Dwarf umDwarf = new Dwarf("Gimli");
        umDwarf.setVida(0);
        
        assertEquals(Status.MORTO, umDwarf.getStatus());
    }
    
    @Test
    public void levaFlechadaCom110DeVidaEscudoEquipado(){
        Dwarf umDwarf = new Dwarf("Gimli");
        
        umDwarf.equiparEscudo();
        umDwarf.sofreDano();
        
        assertEquals(105.0,umDwarf.getVida(), 0.0 );
    }
    
    @Test
    public void levaFlechadaCom3DeVidaEscudoEquipado(){
        Dwarf umDwarf = new Dwarf("Gimli");
        umDwarf.setVida(3);
        umDwarf.equiparEscudo();
        umDwarf.sofreDano();
        
        assertEquals(0,umDwarf.getVida(), 0.0 );
    }
}
