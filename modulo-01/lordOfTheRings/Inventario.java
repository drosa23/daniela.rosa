import java.util.*;

public class Inventario {
    private ArrayList<Item> mochila = new ArrayList<>();

    public Inventario (){
        
    }
    
    public void adicionar(Item novoItem){
        this.mochila.add(novoItem);
    }
    
    public Item obter(int posicao){
        if(posicao >= this.mochila.size()){return null;}
        return this.mochila.get(posicao);
    }
    
    public void remover(int posicao){
        if(posicao >= this.mochila.size()){return;}
        this.mochila.remove(posicao);
    }
    
    public void remover(Item itemRemover){
        for(Item item : mochila){
            if(item.equals(itemRemover)){
                mochila.remove(item);
                return;
            }
        }
    }
    
    public int getTamanho(){
        return this.mochila.size();
    }
    
    public boolean estaVazio(){
        return this.mochila.isEmpty();
    }
    
    //retorna o inventario em forma de array
    public ArrayList<Item> getInventario(){
        ArrayList<Item> itens = new ArrayList<>();
        for(Item item: this.mochila){
            itens.add(item);
        }
        return itens;
    }
    
    public String toString(){
        StringBuilder resultado = new StringBuilder();
        for(Item item : getInventario()){
            String descricao = item.getDescricao();
            resultado.append(descricao);
        }
        return resultado.toString();
    }
    
    public Item buscarPorDescricao(String descricao){
        for(Item item : mochila){
            if(item.getDescricao().equalsIgnoreCase(descricao)){
                return item;
            }
        }
        return null;
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        int ultimaPosicaoPreenchida = this.mochila.size()-1;
        for(int i = 0; i< this.mochila.size(); i++){
            String descricao = this.mochila.get(i).getDescricao();
            descricoes.append(descricao);
            if(ultimaPosicaoPreenchida > i ){
                descricoes.append(",");
            }

        }
        System.out.println(descricoes);
        return descricoes.toString(); 
    }
    
    public Item getItemComMaiorQntd(){
        int i = 1;
        if(this.mochila.isEmpty()){return null;}
        Item maiorItem = this.mochila.get(0);
        for(Item item : mochila){
            if(item.getQuantidade() > maiorItem.getQuantidade()){
                maiorItem = item;
            }
            i++;
        }
        return maiorItem;
    }
    
    //inverte a ordem dos itens do inventario
    public ArrayList<Item> inverter(){
        ArrayList<Item> itensInvert = new ArrayList<>();
        for(int i = this.mochila.size() - 1; i>= 0 ; i--){
            itensInvert.add(this.mochila.get(i));
        }
        return itensInvert;
    }
    
    //ordena os itens por ordem ascendente de quantidade de itens com insertion sort
    public void ordenarItens(){
        for (int i = 1; i < getTamanho(); i++){ 
            Item aux = this.mochila.get(i);
            int j = i;  
            while ((j > 0) && (this.mochila.get(j-1).getQuantidade() > aux.getQuantidade())){
                this.mochila.set(j, this.mochila.get(j-1));
                j -= 1;
            }
            this.mochila.set(j, aux);
        }
        
    }
    
    public void ordenarItens(TipoOrdenacao tipo) {
        if (tipo == TipoOrdenacao.DESC){
            
            for (int i = 1; i < getTamanho(); i++){ 
                Item aux = this.mochila.get(i);
                int j = i;  
                while ((j > 0) && (this.mochila.get(j-1).getQuantidade() < aux.getQuantidade())){
                    this.mochila.set(j, this.mochila.get(j-1));
                    j -= 1;
                }
                this.mochila.set(j, aux);
            }
            return;
        }
        else{
            ordenarItens();
        }
    }
    
    //exercicio extra
    public Inventario unir (Inventario inv){
        Inventario resultado = new Inventario();
        ArrayList<Item> aux = new ArrayList<>();
        aux = inv.getInventario();
        
        for(Item obj : this.mochila){
            resultado.adicionar(obj);
        }
        for(Item item : aux){
            resultado.adicionar(item);
        }
        return resultado;
    }
    
    //exercicio extra
    public Inventario diferenciar(Inventario inv){
        Inventario resultado = new Inventario();

        for(Item item : this.mochila){
            boolean itemDiferente = true;
            for(Item outroItem : inv.getInventario()){
                if (item == outroItem){
                    itemDiferente = false;
                }
            }
            if (!itemDiferente){
                resultado.adicionar(item);
            }
        }
        
        return resultado;
    }
    
    //exercicio extra
    public Inventario cruzar(Inventario inv){
        Inventario resultado = new Inventario();
        
        for(Item item : this.mochila){
            boolean itemCruzado = false;
            for(Item outroItem : inv.getInventario()){
                if (item == outroItem){
                    itemCruzado = true;
                }
            }
            if (itemCruzado){
                resultado.adicionar(item);
            }
        }
        
        return resultado;
    }
}











