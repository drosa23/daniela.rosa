import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{
    @Test
    public void elfoNoturnoDeveSerAlistado(){
        Exercito ex = new Exercito();
        ElfoNoturno not = new ElfoNoturno("Dani");
        ex.alistarElfo(not);
        assertEquals(1, ex.getTamanhoExercito());
    }
    
    @Test
    public void elfoVerdeDeveSerAlistado(){
        Exercito ex = new Exercito();
        ElfoVerde elf = new ElfoVerde("Dani");
        ex.alistarElfo(elf);
        assertEquals(1, ex.getTamanhoExercito());
    }
    
    @Test 
    public void elfoNaoDeveSerAlistado(){
        Exercito ex = new Exercito();
        Elfo elf = new Elfo("Dani");
        ex.alistarElfo(elf);
        assertEquals(0, ex.getTamanhoExercito());
    }
    
    @Test
    public void buscarElfoPorStatusRecemCriado(){
        Exercito ex = new Exercito();
        ElfoVerde elf = new ElfoVerde("Dani");
        ElfoNoturno not = new ElfoNoturno("Galadriel");
        ex.alistarElfo(elf);
        ex.alistarElfo(not);
        ArrayList<Elfo> elfosRecemCriados = ex.getExercito();
        assertEquals(2, elfosRecemCriados.size());
    }

}
