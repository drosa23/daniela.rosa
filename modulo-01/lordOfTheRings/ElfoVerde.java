public class ElfoVerde extends Elfo{
    
    public ElfoVerde(String nome){
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }
        
    public boolean itemPermitido(Item item){
        return (item.getDescricao().equals("Espada de aço valiriano")) || (item.getDescricao().equals("Arco de Vidro")) ||
        (item.getDescricao().equals("Flecha de Vidro"));
    }
    
    @Override
    public void ganharItem(Item item){
        if(this.itemPermitido(item)){
             this.mochila.adicionar(item);
        }
    }
    
    @Override
    public void perderItem(Item item){
        if(this.itemPermitido(item)){
             this.mochila.remover(item);
        }
    }
    
}



