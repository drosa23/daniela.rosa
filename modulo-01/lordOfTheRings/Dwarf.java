import java.util.*;
public class Dwarf extends Personagem{
    private boolean escudoEquipado = false;

    public Dwarf(String nome){
        super(nome);
        this.mochila = new Inventario();
        this.mochila.adicionar(new Item (1, "escudo"));
        this.vida = 110.0;
        this.qtdDano = 10.0;
    }

    public double getVida(){
        return this.vida;
    }

    public void equiparEscudo(){    
        this.escudoEquipado = true;
    }

    public void setVida(double novaVida){
        this.vida = novaVida;
        if(novaVida == 0){
            this.estaMorto();
        }
    }


    public void sofreDano () {
        if(this.escudoEquipado){this.qtdDano = 5;}
        super.sofreDano();
    }

}
