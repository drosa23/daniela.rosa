import java.util.*;
public class Exercito{
    ArrayList<Elfo> elfos;
    
    public Exercito(){
       this.elfos = new ArrayList<>();
    }
    
    public ArrayList<Elfo> getExercito(){
        return elfos;
    }
    
    public int getTamanhoExercito(){
        return elfos.size();
    }
    
    public void alistarElfo(Elfo elfo){
        if ((elfo.getClass().getSimpleName() == "ElfoVerde")|| (elfo.getClass().getSimpleName() == "ElfoNoturno")){
            elfos.add(elfo);
        }
    }
    
    public ArrayList<Elfo> buscarPorStatus(Status status){
        ArrayList<Elfo> resultado = new ArrayList<Elfo>();
        for (Elfo elfo : elfos){
            if(elfo.getStatus() == status){
                resultado.add(elfo);
            }
        }
        
        return resultado;
    }
    
}
