import java.util.*;
public class ElfoDaLuz extends Elfo{
    private int contadorAtaques = 0;
    private final double QTD_VIDA_GANHA = 10;
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Arrays.asList(
                "Espada de Galvorn"
    ));
    
    public ElfoDaLuz(String nome){
        super(nome);
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
        qtdDano = 21;
    }
    
    
    public void perderItem(Item item) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder) {
            super.perderItem(item);
        }
    }

    private Item getEspada() {
        return this.getInventario().buscarPorDescricao(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    private void ganharVida() {
        this.vida += QTD_VIDA_GANHA;
    }
    
    public void atacarComEspada(Dwarf anao){
        if(anao.getStatus() != Status.MORTO){
            this.contadorAtaques++;
            anao.sofreDano();
            if(contadorAtaques % 2 == 0){
                ganharVida();
            }
            else{
                sofreDano();
            }
        }
    }
    
}
