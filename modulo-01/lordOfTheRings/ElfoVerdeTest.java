import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {
    
    @Test
    public void testaSeItemPermitidoComItemNPermi(){
        ElfoVerde elfo = new ElfoVerde("Dani");
        Item espada = new Item(1, "Espada");
        boolean resultado = elfo.itemPermitido(espada);
        
        assertFalse(resultado);
    }
    
    @Test
    public void testaSeItemPermitidoComItemPermi(){
        ElfoVerde elfo = new ElfoVerde("Dani");
        Item espada = new Item(1, "Espada de aço valiriano");
        boolean resultado = elfo.itemPermitido(espada);
        
        assertTrue(resultado);
    }
    
    @Test
    public void ganharItemInvTamanho3(){
        Elfo elfo = new ElfoVerde("Dani");
        Item espada = new Item(1, "Espada de aço valiriano");
        elfo.ganharItem(espada);
        int mochilaTamanho = elfo.getInventario().getTamanho();
        //3 pois ele ja possui um arco e uma flecha;
        assertEquals(3, mochilaTamanho);
    }
    
    @Test
    public void perderItemInvTamanho3(){
        Elfo elfo = new ElfoVerde("Dani");
        Item espada = new Item(1, "Espada de aço valiriano");
        elfo.ganharItem(espada);
        elfo.perderItem(espada);
        int mochilaTamanho = elfo.getInventario().getTamanho();
        assertEquals(2, mochilaTamanho);
    }
    
    
    
}
