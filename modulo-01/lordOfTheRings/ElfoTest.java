import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
        
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXpDiminuirVidaDwar(){
        //Arrange
        Elfo umElfo = new Elfo("Legolas");
        Dwarf umDwarf = new Dwarf("Gimli");
        //Act
        umElfo.atiraFlecha( umDwarf);
        umElfo.atiraFlecha( umDwarf);
        umElfo.atiraFlecha( umDwarf);
        //Assert
        assertEquals(2, umElfo.getExperiencia());
        assertEquals(0, umElfo.getFlechas().getQuantidade());
        assertEquals(90.0, umDwarf.getVida(), 0.0);
        
        
        
    }
    
    @Test
    public void verificaQntdFlechasInicial(){
        Elfo umElfo = new Elfo("Legolas");
        assertEquals(2, umElfo.getFlechas().getQuantidade());
    }
    
    @Test
    public void elfoNascecomStatusRecemCriado(){
        Elfo elfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, elfo.getStatus());
    }
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void testaQuantidadeDeElfosInstanciados(){
        Elfo elfo1 = new Elfo("Legolas");
        Elfo elfo2 = new Elfo("Legolas");
        Elfo elfo3 = new Elfo("Legolas");
        Elfo elfo4 = new Elfo("Legolas");
        Elfo elfo5 = new Elfo("Legolas");
        
        assertEquals(5, elfo5.getContadorDeInstancias());
    }
}
