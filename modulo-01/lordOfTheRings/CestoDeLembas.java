public class CestoDeLembas
{
    private int quantidadeLembas;

    public CestoDeLembas(int quantidadeLembas){
        this.quantidadeLembas = quantidadeLembas;
    }
    
    public boolean podeDividirEmPares(){
        boolean pode = false;
        if((this.quantidadeLembas > 2) && (this.quantidadeLembas%2==0) && this.quantidadeLembas <= 100){pode = true;}
        return pode;
    }

}
