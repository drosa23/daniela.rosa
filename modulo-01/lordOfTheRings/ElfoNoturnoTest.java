import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest
{
    @Test
    public void testaAtirarFlechaPerde15Vida(){
        ElfoNoturno not = new ElfoNoturno("Dani");
        Dwarf anao = new Dwarf ("Gimli");
        not.atiraFlecha(anao);
        
        assertEquals(85.0, not.getVida(), 0.0);
    }
    
    @Test
    public void atira7FlechasEMorre(){
        ElfoNoturno not = new ElfoNoturno("Dani");
        Dwarf anao = new Dwarf ("Gimli");
        not.getFlechas().setQuantidade(1000);
        not.atiraFlecha(anao);
        not.atiraFlecha(anao);
        not.atiraFlecha(anao);
        not.atiraFlecha(anao);
        not.atiraFlecha(anao);
        not.atiraFlecha(anao);
        not.atiraFlecha(anao);
        
        assertEquals(0.0, not.getVida(), 0.0);
    }
   
}
