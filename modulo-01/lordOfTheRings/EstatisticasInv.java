import java.util.*;
public class EstatisticasInv{
    
    private Inventario inventario = new Inventario(); 
    
    public EstatisticasInv(Inventario inventario){
        this.inventario = inventario; 
    }
    
    public double calcularMedia(){
        double media = 0.0;
        int tamanho = this.inventario.getTamanho();
        ArrayList<Item> itens = inventario.getInventario();
        for(Item item : itens){
            media += item.getQuantidade();
        }
        media = media/tamanho;
        return media;
    }
    
    public double calcularMediana(){
        double mediana = new Double(0.0);
        this.inventario.ordenarItens();
        int tamanho = this.inventario.getTamanho();
        int posicaoDoMeio = tamanho/2;
        double valorDoMeio = this.inventario.obter(posicaoDoMeio).getQuantidade();
        if (tamanho%2 == 0){ 
            int posicaoDoMeioAbaixo = posicaoDoMeio-1;
            double valorDoMeioAbaixo = this.inventario.obter(posicaoDoMeioAbaixo).getQuantidade();
            mediana = (valorDoMeio + valorDoMeioAbaixo)/2;
            return mediana;
        }
        else{
            mediana = valorDoMeio;
        }
        return mediana;
    }
    
        public int qtdItensAcimaMedia(){
        int resultado = 0;
        double media = calcularMedia();
        
        for(Item item : this.inventario.getInventario()){
            if(item.getQuantidade() > media){
            resultado++;
            }
        }
        return resultado;
       }

}
