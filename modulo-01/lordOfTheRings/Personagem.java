public abstract class Personagem{
    protected String nome;
    protected Status status;
    protected Inventario mochila;
    protected double vida, qtdDano;
    
    protected Personagem(String nome){
        this.nome = nome;
        this.status = Status.RECEM_CRIADO;
        mochila = new Inventario();
        this.qtdDano = 0.0;
    }
    
    protected String getNome(){
        return this.nome;
    }

    protected Status getStatus(){
        return this.status;
    }
    
    protected Inventario getInventario(){
        return this.mochila;
    }
    
    
    
    protected void setNome(String novoNome){
        this.nome = novoNome;
    }
    
    protected void ganharItem(Item item){
        this.mochila.adicionar(item);
    }
    
    protected void perderItem(Item item){
        this.mochila.remover(item);
    }
    
    protected void estaMorto(){
        this.status = Status.MORTO;
        this.vida = 0;
    }
    
    protected void sofreDano() {   
        if(this.vida >= this.qtdDano){
            this.vida = this.vida - this.qtdDano;
            this.status = Status.SOFREU_DANO;
        }
        else{
            estaMorto();
        }
    }
}
