

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DadoD6Test{
    @Test
    public void DadoD6DeveRetornarEntre1e6(){
        
        for(int i =0; i<1000000; i++){
            DadoD6 dado = new DadoD6();
            int numero = dado.sortear();
            boolean resultado = (numero == 1) || (numero == 2) || (numero == 3) || (numero == 4) || (numero == 5) || (numero == 6);
            assertTrue(resultado);
        }
    }
    
    @Test 
    public void dadoDeveRetornar33DeChance(){
        int contador = 0;
        boolean retorno;
        for(int i = 0; i < 100000; i++){
            DadoD6 dado = new DadoD6();
            retorno = dado.girar33deChance();
            if(retorno){ contador ++;} 
        }
        System.out.println(contador);
        boolean resultado = contador > 32000 && contador < 35000;
        assertTrue(resultado);
    }

}
