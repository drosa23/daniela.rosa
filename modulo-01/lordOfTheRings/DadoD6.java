import java.util.*;
public class DadoD6
{
    public int sortear(){
        Random random = new Random();
        int nAleatorio = random.nextInt(6) + 1;
        return nAleatorio;
    }
    
    public boolean girar33deChance(){
        int n1 = 3;
        int n2 = 6;
        int n3 = sortear();
        if((n1 == n3) || (n2 == n3)){
            return true;
        }
        return false;
        
        // Random rand = new Random();
        // return rand.nextInt(2) == 0;
    }
    
}
