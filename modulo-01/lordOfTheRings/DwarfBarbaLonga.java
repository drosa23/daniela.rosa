public class DwarfBarbaLonga extends Dwarf{
    
    public DwarfBarbaLonga (String nome){
        super(nome);
    }
    
    @Override
    public void sofreDano(){
        DadoD6 dado = new DadoD6();
        if(dado.girar33deChance()){
            this.qtdDano = 0;
            
        }
        super.sofreDano();
    }
    
}
