import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInvTest
{
    @Test
    public void calculaMediaQtndInv(){
        Item escudo = new Item(4, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(4, "Flechas");
        Item pocoes = new Item(4, "Pocoes"); 
        
        Inventario inv = new Inventario();
        
        inv.adicionar(escudo);
        inv.adicionar(arco);
        inv.adicionar(flechas);
        inv.adicionar(pocoes);
        
        EstatisticasInv est = new EstatisticasInv(inv); 
        
        assertEquals(4,est.calcularMedia(),0.0);
    }
    
    @Test
    public void calculaMedianaInvPar(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        EstatisticasInv est = new EstatisticasInv(mochila); 
        
        assertEquals(3.5, est.calcularMediana(), 0.0);
        
    }
    
    @Test
    public void calculaMedianaInvImpar(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        Item capa = new Item(2, "Capa");
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        mochila.adicionar(capa);
        
        EstatisticasInv est = new EstatisticasInv(mochila); 
        
        assertEquals(3, est.calcularMediana(), 0.0);
        
    }
    
    @Test
    public void retorna2ItensQtdAcimaDaMedia(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        Item capa = new Item(2, "Capa");
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        mochila.adicionar(capa);
        
        EstatisticasInv est = new EstatisticasInv(mochila); 
        
        assertEquals(2, est.qtdItensAcimaMedia());
    }
}
