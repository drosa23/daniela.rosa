import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest
{
    @Test
    public void testeAdicionar(){
        Inventario mochila = new Inventario();
        Item arco = new Item(1,"Arco");
        
        mochila.adicionar(arco);
        assertEquals(arco.getDescricao(), mochila.obter(0).getDescricao() );
    }

    @Test
    public void testePegarDescricoes(){
        Inventario mochila = new Inventario();
        Item flechas = new Item(2, "Flecha");
        Item arco = new Item(1,"Arco");
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
     
        assertEquals("Arco,Flecha", mochila.getDescricoesItens());
    }
    
    @Test
    public void testeObter(){
        Inventario mochila = new Inventario();
        Item flechas = new Item(2, "Flecha");
        mochila.adicionar(flechas);
        
        assertEquals(flechas.getDescricao(), mochila.obter(0).getDescricao());
    }
    
    @Test
    public void testeToString(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(4, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(4, "Flechas");
        Item pocoes = new Item(4, "Pocoes");
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        assertEquals("EscudoArcoFlechasPocoes", mochila.toString());
    }
    
    @Test
    public void testeObterItemMaiorQntd(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(4, "Escudo");
        mochila.adicionar(escudo);
        
        assertEquals(escudo.getDescricao(), mochila.getItemComMaiorQntd().getDescricao());
    }
    
    @Test
    public void recebeDescricaoEProcuraItemCorresp(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(4, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(4, "Flechas");
        Item pocoes = new Item(4, "Pocoes");
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        assertEquals(arco.getDescricao(), mochila.buscarPorDescricao("Arco").getDescricao());
    }
    
    @Test
    public void buscarItemComInventarioVazio(){
        Inventario mochila = new Inventario();
        assertNull(mochila.buscarPorDescricao("Arco"));
    }
     
    @Test
    public void recebeInventarioEInverte(){
        Inventario mochila = new Inventario();
        Inventario mochilaInvert = new Inventario();
        Item escudo = new Item(4, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(4, "Flechas");
        Item pocoes = new Item(4, "Pocoes"); 
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        mochilaInvert.adicionar(pocoes);
        mochilaInvert.adicionar(flechas);
        mochilaInvert.adicionar(arco);
        mochilaInvert.adicionar(escudo);
        
        ArrayList<Item> itensInvert = mochila.inverter();
        
        assertEquals(mochilaInvert.obter(0).getDescricao(), itensInvert.get(0).getDescricao());
    }
    
    @Test
    public void transformaInventarioEmArrayList(){
        Inventario mochila = new Inventario();
        ArrayList<Item> arrMochila = new ArrayList<>();
        Item escudo = new Item(4, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(4, "Flechas");
        Item pocoes = new Item(4, "Pocoes"); 
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        arrMochila.add(escudo);
        arrMochila.add(arco);
        arrMochila.add(flechas);
        arrMochila.add(pocoes);
        
        assertEquals(arrMochila, mochila.getInventario());
    }
    
    @Test
    public void ordenaInvOrdemAscDeQuantidade(){
        Inventario mochila = new Inventario();
        Inventario mochilaAsc = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        mochilaAsc.adicionar(pocoes);
        mochilaAsc.adicionar(flechas);
        mochilaAsc.adicionar(arco);
        mochilaAsc.adicionar(escudo);
        //Act
        mochila.ordenarItens();
        
        assertEquals(mochilaAsc.obter(0), mochila.obter(0));
        
    }
    
    @Test
    public void ordenaInvOrdemAscDeQuantidadeTipoOrdenacao(){
        Inventario mochila = new Inventario();
        Inventario mochilaAsc = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        mochilaAsc.adicionar(pocoes);
        mochilaAsc.adicionar(flechas);
        mochilaAsc.adicionar(arco);
        mochilaAsc.adicionar(escudo);
        //Act
        mochila.ordenarItens(TipoOrdenacao.ASC);
        
        assertEquals(mochilaAsc.obter(0), mochila.obter(0));
        
    }
    
    @Test
    public void ordenaInvOrdemDescDeQuantidadeTipoOrdenacao(){
        Inventario mochila = new Inventario();
        Inventario mochilaDesc = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        
        mochilaDesc.adicionar(escudo);
        mochilaDesc.adicionar(arco);
        mochilaDesc.adicionar(flechas);
        mochilaDesc.adicionar(pocoes);
        
        mochila.adicionar(pocoes);
        mochila.adicionar(flechas);
        mochila.adicionar(arco);
        mochila.adicionar(escudo);
        //Act
        mochila.ordenarItens(TipoOrdenacao.DESC);
        
        assertEquals(mochilaDesc.obter(0), mochila.obter(0));
        
    }
    
    @Test
    public void pegaOTamanhoOcupadoDoArray(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item arco = new Item(4, "Arco");
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        
        mochila.adicionar(escudo);
        mochila.adicionar(arco);
        mochila.adicionar(flechas);
        mochila.adicionar(pocoes);
        
        assertEquals(4, mochila.getTamanho());
    }
    
    //exericio extra unir
    @Test
    public void unir2Inventarios(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item capa = new Item(4, "capa");
        mochila.adicionar(escudo);
        mochila.adicionar(capa);
        
        Inventario outraMochila = new Inventario();
        Item espada = new Item(5, "espada");
        Item machado = new Item(4, "machado");
        
        outraMochila.adicionar(espada);
        outraMochila.adicionar(machado);
        Inventario resultado = new Inventario();
        resultado.adicionar(escudo);
        resultado.adicionar(capa);
        resultado.adicionar(espada);
        resultado.adicionar(machado);
        
        Inventario mochilasUnidas = new Inventario();
        mochilasUnidas = mochila.unir(outraMochila);
        
        assertEquals(resultado.toString(), mochilasUnidas.toString());
    }
    
    //exericio extra diferenciar
    @Test
    public void diferenciar2Inventarios(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item capa = new Item(4, "capa");
        Item espada = new Item(5, "espada");
        Item machado = new Item(4, "machado");
        
        mochila.adicionar(escudo);
        mochila.adicionar(capa);
        mochila.adicionar(espada);
        mochila.adicionar(machado);
        
        Inventario outraMochila = new Inventario();
        Item flechas = new Item(3, "Flechas");
        Item pocoes = new Item(1, "Pocoes"); 
        
        outraMochila.adicionar(espada);
        outraMochila.adicionar(machado);
        outraMochila.adicionar(flechas);
        outraMochila.adicionar(pocoes);
    
        Inventario resultado = new Inventario();
        resultado.adicionar(espada);
        resultado.adicionar(machado);
        
        Inventario mochilaA_B = new Inventario();
        mochilaA_B = mochila.diferenciar(outraMochila);
        
        assertEquals(resultado.toString(), mochilaA_B.toString());
    }
    
    //exericio extra diferenciar
    @Test
    public void cruzar2Inventarios(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        Item capa = new Item(4, "capa");
        Item espada = new Item(5, "espada");
        Item machado = new Item(4, "machado");
        Item flechas = new Item(3, "Flechas");
        
        mochila.adicionar(escudo);
        mochila.adicionar(capa);
        mochila.adicionar(espada);
        mochila.adicionar(machado);
        mochila.adicionar(flechas);
        
        Inventario outraMochila = new Inventario();
        
        Item pocoes = new Item(1, "Pocoes"); 
        
        outraMochila.adicionar(escudo);
        outraMochila.adicionar(espada);
        outraMochila.adicionar(machado);
        outraMochila.adicionar(flechas);
        outraMochila.adicionar(pocoes);
    
        Inventario resultado = new Inventario();
        resultado.adicionar(escudo);
        resultado.adicionar(espada);
        resultado.adicionar(machado);
        resultado.adicionar(flechas);
        
        Inventario mochilaAeB = new Inventario();
        mochilaAeB = mochila.cruzar(outraMochila);
        
        assertEquals(resultado.toString(), mochilaAeB.toString());
    }
    
    @Test
    public void removerItemPassandoItem(){
        Inventario mochila = new Inventario();
        Item escudo = new Item(5, "Escudo");
        
        mochila.remover(new Item(5, "Escudo"));
        
        assertEquals(0, mochila.getTamanho());
    }
}




