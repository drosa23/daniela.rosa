import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest
{
    @Test
    public void atacaComEspadaAtaqueImparPerdeVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
	feanor.atacarComEspada(new Dwarf("Farlum")); // perde 21 unidades, foi o ataque 1
	
	assertEquals(79, feanor.getVida(), 0.0);
    }
    
    @Test
    public void atacaComEspadaAtaqueParGanhaVida(){
        ElfoDaLuz feanor = new ElfoDaLuz("Fëanor");
        feanor.atacarComEspada(new Dwarf("Farlum")); // perde 21 unidades, foi o ataque 1
	feanor.atacarComEspada(new Dwarf("Farlum")); // ganha 10 unidades, foi o ataque 2
	
	assertEquals(89.0, feanor.getVida(), 0.0);
    }

}
