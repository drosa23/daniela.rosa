import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class AgendaContatosTest{
    @Test
    public void agendaAdicionaContato(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Dani", "11");
        assertEquals(1, agenda.getTamanho());
    }
    
    @Test
    public void pegaTelefonePeloNumero(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Dani", "11");
        assertEquals("11", agenda.getTelefone("Dani"));
    }
    
    @Test
    public void pegaNumeroPeloTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Dani", "11");
        assertEquals("Dani", agenda.consultarContato("11"));
    }
    
     @Test
    public void adicionarDoisContatosEGerarCSV() {
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Bernardo", "555555");
        agenda.adicionarContato("Mithrandir", "444444");
        String separador = System.lineSeparator();
        String esperado = String.format("Bernardo,555555%sMithrandir,444444%s", separador, separador);
        assertEquals(esperado, agenda.csv());
    }
    
    
   
}
