import java.util.*;
public class AgendaContatos{
    HashMap<String,String> contatos;
    
    public AgendaContatos(){
        this.contatos = new LinkedHashMap<>();
    }
    
    public void adicionarContato(String nome, String numero ){
        this.contatos.put(nome, numero);
    }
    
    public int getTamanho(){
        return contatos.size();
    }
    
    public String getTelefone(String nome){
        return this.contatos.get(nome);
    }
    
    public String consultarContato(String telefone) {
        for (HashMap.Entry<String, String> par : contatos.entrySet()) {
            if (par.getValue().equals(telefone)) {
                return par.getKey();
            }
        }
        return null;
    }
    
    public String csv() {
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for (HashMap.Entry<String, String> par : contatos.entrySet()) {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
        }

        return builder.toString();
    }

   
}
