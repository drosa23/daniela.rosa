package br.com.dbc.bancoDigital.entity;

import javax.persistence.*;

@Entity
@Table(name = "Cliente")

public class Cliente {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)

    @ManyToOne
    @JoinColumn(name = "id_banco")
    private Banco banco;


    private Integer Id;
   
    private String nome;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    } 
}
