package br.com.dbc.bancoDigital;

import br.com.dbc.bancoDigital.entity.Banco;
import br.com.dbc.bancoDigital.entity.HibernateUtil;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Banco banco1 = new Banco();
            Banco banco2 = new Banco();
            Banco banco3 = new Banco();
            Banco banco4 = new Banco();
            banco1.setNome("Itaú");
            banco2.setNome("Caixa");
            banco3.setNome("Bradesco");
            banco4.setNome("Banrisul");
            
            session.save(banco1);
            session.save(banco2);
            session.save(banco3);
            session.save(banco4);
            
            transaction.commit();
        } catch (Exception e) {
            if(transaction != null){
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, e.getMessage(), e);
            System.exit(1);
        } finally {
            if(session != null){
                session.close();
                System.exit(0);
                
            }
            
        }
    }
}
