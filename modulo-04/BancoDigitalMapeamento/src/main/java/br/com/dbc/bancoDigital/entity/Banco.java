/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.bancoDigital.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Banco")

public class Banco {


    @Id
    @SequenceGenerator(allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
    @GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<Cliente> clientes;

    private Integer Id;
   
    private String nome;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}

