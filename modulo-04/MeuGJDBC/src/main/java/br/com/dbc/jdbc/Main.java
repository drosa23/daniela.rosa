/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniela.amaral
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'USUARIO'").executeQuery();
            if (!rs.next()) {
                conn.prepareStatement("CREATE TABLE Usuario (\n"
                        + "	Id number NOT NULL PRIMARY KEY,\n"
                        + "	Nome varchar (100) NOT NULL,\n"
                        + "	Apelido varchar (15) NOT NULL,\n"
                        + "	Senha varchar (15) NOT NULL,\n"
                        + "	CPF number NOT NULL\n"
                        + ")").execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}

