/*Projeto Vem Ser DBC - Edição 2019/01
Módulo 04 - Lista 02

Exercício 01

Criar script de criação de banco de dados conforme nossa modelagem ER.
Deve conter os relacionamentos, default, tamanho dos campos e tipagem
Deve ser criado as SEQUENCE de cada tabela.
Deve ser inserido 3 dados para cada
O personagem deve possuir 2 editar
e deve haver 1 delete de cada tabela que tenha vinculo com outra resolvendo todos os conflitos desse vinculo.
E para finalizar quero saber como eu visualizo os resultados de cada tabela.
*/



CREATE TABLE Usuario (
	Id number NOT NULL PRIMARY KEY,
	Nome varchar (100) NOT NULL,
	Apelido varchar (15) NOT NULL,
	Senha varchar (15) NOT NULL,
	CPF number NOT NULL
);

CREATE TABLE Contato (
	Id number NOT NULL PRIMARY KEY,
	Valor VARCHAR (100),
);

CREATE TABLE Tipo_Contato(
	Id number NOT NULL PRIMARY KEY,
	Nome VARCHAR(100) NOT NULL,
	Quantidade number NOT NULL 
);

CREATE TABLE usuario_endereco(
	id number NOT NULL PRIMARY KEY,
);

CREATE TABLE endereco(
	id number NOT NULL PRIMARY KEY,
	logradouro VARCHAR (100) NOT NULL,
	numero number (5) NOT NULL,
	complemento varchar (50),
	bairro varchar (50) NOT NULL,
	cidade varchar (50) NOT NULL,
);

CREATE TABLE cartao_credito(
	id number NOT NULL PRIMARY KEY,
	bandeira VARCHAR (50) NOT NULL,
	numero number (15) NOT NULL,
	nome_escrito VARCHAR (100) NOT NULL,
	codigo_seguranca number (3) NOT NULL,
	validade number (10) NOT NULL
);

CREATE TABLE status(
	id number NOT NULL PRIMARY KEY,
	nome varchar (100) NOT NULL,
	dano number (3) NOT NULL,
	vida number (3) NOT NULL,
	xp number (3) NOT NULL
);

CREATE TABLE personagem(
	id number NOT NULL PRIMARY KEY,
	nome_status varchar(100) NOT NULL
);

CREATE TABLE inventario(
	id number NOT NULL PRIMARY KEY,
	qtd_maxima number (1) NOT NULL default(3)
);

CREATE TABLE lista_itens_inventario(
	id number NOT NULL PRIMARY KEY,
	quantidade number (4) NOT NULL
);

CREATE TABLE tipo_raca(
	id number NOT NULL PRIMARY KEY,
	nome varchar (100) NOT NULL,
	bonus_vida number (3) NOT NULL,
	bonus_dano number (3) NOT NULL
);

CREATE TABLE tipo_raca_item (
	id number NOT NULL PRIMARY KEY,
);

CREATE TABLE item(
	id number NOT NULL PRIMARY KEY,
	descricao varchar (100) NOT NULL,
	qtd_maxima number (3)
)

CREATE TABLE itens_materiais(
	id number NOT NULL PRIMARY KEY,
	quantidadeMaterial number NOT NULL
);

CREATE TABLE material(
	id number NOT NULL PRIMARY KEY,
	nome varchar (100) NOT NULL,
	unidade varchar (10) NOT NULL
);

CREATE TABLE raca(
	id number NOT NULL PRIMARY KEY,
	nome varchar (100) NOT NULL,
	limitador number (3),
	vida_inicio number (3) NOT NULL,
	dano_inicio number (3) NOT NULL
);

ALTER TABLE Contato 
ADD Id_Tipo_Contato number NOT NULL 
REFERENCES Tipo_Contato(Id)
ON DELETE CASCADE;

alter table Contato 
add constraint fk_id_tipo_contato
foreign key (Id_Tipo_Contato) references tipo_contato
ON DELETE CASCADE;

ALTER TABLE usuario_endereco
add constraint fk_id_usuario
foreign key (id_usuario) references usuario
ON DELETE CASCADE;

ALTER TABLE usuario_endereco
add constraint fk_id_endereco
foreign key (id_endereco) references endereco
ON DELETE CASCADE;

ALTER TABLE cartao_credito
add constraint fk_id_usuario
foreign key (id_usuario) references usuario
ON DELETE CASCADE;

ALTER TABLE personagem
add constraint fk_id_inventario
foreign key (id_inventario) references inventario
ON DELETE CASCADE;

ALTER TABLE personagem
add constraint fk_id_usuario
foreign key (id_usuario) references usuario
ON DELETE CASCADE;

ALTER TABLE personagem
add constraint fk_id_raca
foreign key (id_raca) references raca
ON DELETE CASCADE;

ALTER TABLE personagem
add constraint fk_id_status
foreign key (id_raca) references raca
ON DELETE CASCADE;

ALTER TABLE tipo_raca
add constraint fk_id_raca
foreign key (id_raca) references raca
ON DELETE CASCADE;

ALTER TABLE tipo_raca_item
add constraint fk_id_tipo_raca
foreign key (id_tipo_raca) references tipo_raca
ON DELETE CASCADE;

ALTER TABLE tipo_raca_item
add constraint fk_id_item
foreign key (id_item) references item
ON DELETE CASCADE;

ALTER TABLE tipo_raca_item
add constraint fk_id_tipo_raca
foreign key (id_tipo_raca) references tipo_raca
ON DELETE CASCADE;

ALTER TABLE lista_itens_inventario
add constraint fk_id_inventario
foreign key (id_inventario) references inventario
ON DELETE CASCADE;

ALTER TABLE lista_itens_inventario
add constraint fk_id_item
foreign key (id_item) references item
ON DELETE CASCADE;

ALTER TABLE itens_materiais
add constraint fk_id_item
foreign key (id_item) references item
ON DELETE CASCADE;

ALTER TABLE itens_materiais
add constraint fk_id_material
foreign key (id_material) references material
ON DELETE CASCADE;


CREATE SEQUENCE CONTATO_SEQ
START WITH 1
INCREMENT BY 1;

CREATE SEQUENCE Usuario_Seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE tipo_contato_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE usuario_endereco_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE endereco_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE cartao_credito_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE personagem_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE status_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE inventario_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE lista_itens_inventario_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE tipo_raca_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE tipo_raca_item_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE item_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE itens_materiais_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE material_seq
START WITH 1
INCREMENT BY 1

CREATE SEQUENCE raca_seq
START WITH 1
INCREMENT BY 1


INSERT INTO Usuario
VALUES (Usuario_Seq.NEXTVAL, "USER1", "USR1", "12345", 00000000)
INSERT INTO Usuario
VALUES (Usuario_Seq.NEXTVAL, "USER2", "USR2", "12345", 00000001)
INSERT INTO Usuario
VALUES (Usuario_Seq.NEXTVAL, "USER3", "USR1", "12345", 00000003)

INSERT INTO tipo_contato
VALUES (tipo_contato_seq.NEXTVAL, "user1", 3)
INSERT INTO tipo_contato
VALUES (tipo_contato_seq.NEXTVAL, "user2", 3)
INSERT INTO tipo_contato
VALUES (tipo_contato_seq.NEXTVAL, "user3", 3)

INSERT INTO contato
VALUES (CONTATO_SEQ.NEXTVAL, 1, 1, "a")
INSERT INTO contato
VALUES (CONTATO_SEQ.NEXTVAL, 2, 1, "b")
INSERT INTO contato
VALUES (CONTATO_SEQ.NEXTVAL, 1, 3, "c")

INSERT INTO usuario_endereco 
VALUES (usuario_endereco_seq.NEXTVAL, 1, 1)
INSERT INTO usuario_endereco 
VALUES (usuario_endereco_seq.NEXTVAL, 2, 2)
INSERT INTO usuario_endereco 
VALUES (usuario_endereco_seq.NEXTVAL, 3, 3)

INSERT INTO endereco
VALUES (endereco_seq.NEXTVAL, "rua a", 300, "casa", "vila", "rj")
INSERT INTO endereco
VALUES (endereco_seq.NEXTVAL, "rua a", 301, "casa", "vila", "rj")
INSERT INTO endereco
VALUES (endereco_seq.NEXTVAL, "rua a", 302, "casa", "vila", "rj")

INSERT INTO cartao_credito
VALUES (cartao_credito_seq.NEXTVAL, 1, "visa", 300, "daniela a rosa", 333, 230918)
INSERT INTO cartao_credito
VALUES (cartao_credito_seq.NEXTVAL, 2, "visa", 300, "amanda a rosa", 333, 230918)
INSERT INTO cartao_credito
VALUES (cartao_credito_seq.NEXTVAL, 3, "visa", 300, "adam a rosa", 333, 230918)

INSERT INTO personagem
VALUES (personagem_seq.NEXTVAL, 1 ,1 , 1, "vahar", 10, 100, 0 )
INSERT INTO personagem
VALUES (personagem_seq.NEXTVAL, 2 , 2 , 2, "vaharla", 10, 100, 0 )
INSERT INTO personagem
VALUES (personagem_seq.NEXTVAL, 3 ,3 ,3 , "avahar", 10, 100, 0 )

INSERT INTO status
VALUES (status_seq.NEXTVAL, "recem-criado")
INSERT INTO status
VALUES (status_seq.NEXTVAL, "morto")
INSERT INTO status
VALUES (status_seq.NEXTVAL, "levou dano")

INSERT INTO inventario
VALUES (inventario_seq.NEXTVAL, 10)
INSERT INTO inventario
VALUES (inventario_seq.NEXTVAL, 20)
INSERT INTO inventario
VALUES (inventario_seq.NEXTVAL, 30)

INSERT INTO lista_itens_inventario
VALUES (lista_itens_inventario_seq.NEXTVAL, 1, 1, 3)
INSERT INTO lista_itens_inventario
VALUES (lista_itens_inventario_seq.NEXTVAL, 2, 2, 4)
INSERT INTO lista_itens_inventario
VALUES (lista_itens_inventario_seq.NEXTVAL, 3, 3, 3)

INSERT INTO raca
VALUES (raca_seq.NEXTVAL, "elfo",1 ,100, 15)
INSERT INTO raca
VALUES (raca_seq.NEXTVAL, "humano", 1, 150, 20)
INSERT INTO raca
VALUES (raca_seq.NEXTVAL, "anao",1 , 400, 5)

INSERT INTO item
VALUES (item_seq.NEXTVAL, "arco", 5)
INSERT INTO item
VALUES (item_seq.NEXTVAL, "flecha", 100)
INSERT INTO item
VALUES (item_seq.NEXTVAL, "machado", 5)

INSERT INTO itens_materiais
VALUES (itens_materiais_seq.NEXTVAL, 1, 1, 4)
INSERT INTO itens_materiais
VALUES (itens_materiais_seq.NEXTVAL, 2, 1, 2)
INSERT INTO itens_materiais
VALUES (itens_materiais_seq.NEXTVAL, 3, 1, 1)

INSERT INTO material
VALUES (material_seq.NEXTVAL, "madeira", 1)
INSERT INTO material
VALUES (material_seq.NEXTVAL, "ferro", 1)
INSERT INTO material
VALUES (material_seq.NEXTVAL, "linha", 1)

INSERT INTO tipo_raca
VALUES (tipo_raca_seq.NEXTVAL, 1, "elfo noturno", 2, 3)
INSERT INTO tipo_raca
VALUES (tipo_raca_seq.NEXTVAL, 2, "humano da montanha", 2, 3)
INSERT INTO tipo_raca
VALUES (tipo_raca_seq.NEXTVAL, 3, "anao de ferro", 2, 3 )

INSERT INTO tipo_raca_item
VALUES (tipo_raca_item_seq.NEXTVAL, 1, 1)
INSERT INTO tipo_raca_item
VALUES (tipo_raca_item_seq.NEXTVAL, 2, 2)
INSERT INTO tipo_raca_item
VALUES (tipo_raca_item_seq.NEXTVAL, 3, 3)




UPDATE personagem 
SET Nome = "amenediel"
WHERE
	Id = 1;

UPDATE personagem 
SET Nome = "baco"
WHERE
	Id = 2;
	
DELETE FROM USUARIO
WHERE
	ID = 1;

DELETE FROM Tipo_Contato
WHERE
	ID = 1;

DELETE FROM endereco
WHERE
	ID = 1;

DELETE FROM inventario
WHERE
	ID = 1;

DELETE FROM raca
WHERE
	ID = 1;

DELETE FROM status
WHERE
	ID = 1;

DELETE FROM tipo_raca_item
WHERE
	ID = 1;

DELETE FROM item
WHERE
	ID = 1;]

DELETE FROM material
WHERE
	ID = 1;

DELETE FROM tipo_raca
WHERE
	ID = 1;

-- SELECT * FROM (nome da tabela)



