﻿/*Exercício 02

Criar ER e script de criação de banco de dados para um banco digital conforme requisitos.
É preciso ter uma tabela de bancos, agências, contas, clientes
Cada Cliente ao se cadastrar submeter seus dados completos (com seus endereços, contatos e documentos)
Cada conta possui um tipo (Conjunto, Fisica, PJ)
Cada conta faz movimentações são elas: Saque, depósito, transferência e pagamento.
Cada banco possui seus usuários (Gerentes, Gerentes Gerais, etc)
Um cliente pode fazer um emprestimo, pra isso ele precisa de crédito pré aprovado nos seus dados ou pedir liberação para o Gerente (Até determinado valor pode haver liberação sem consulta, após precisa de liberação do GG também)
Quando é feito um empréstimo, precisa ser gerado um saldo para o cliente sem perder os dados do empréstimo. */



CREATE TABLE endereco (
	Id number NOT NULL PRIMARY KEY,
	rua varchar (100) NOT NULL,
    numero number (6) NOT NULL,
    complemento number (5)
);

CREATE TABLE contato (
	Id number NOT NULL PRIMARY KEY,
	tipo varchar (100) NOT NULL,
    numero number (9) NOT NULL
);

CREATE TABLE banco (
	Id number NOT NULL PRIMARY KEY,
	nome varchar (100) NOT NULL,
);

CREATE TABLE agencia (
	Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_endereco) REFERENCES endereco,
    FOREIGN KEY (id_banco) REFERENCES banco
);

CREATE TABLE conta(
	Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_agencia) REFERENCES agencia,
    
);

CREATE TABLE cliente (
	Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_endereco) REFERENCES endereco,
    FOREIGN KEY (id_contato) REFERENCES contato
);


CREATE TABLE conta_fisica (
	Id number NOT NULL PRIMARY KEY,
    cpf number NOT NULL,
    FOREIGN KEY (id_cliente) REFERENCES cliente,
    FOREIGN KEY (id_conta) REFERENCES conta
);

CREATE TABLE conta_juridica (
	Id number NOT NULL PRIMARY KEY,
    cnpj number NOT NULL,
    FOREIGN KEY (id_conta) REFERENCES conta
);

CREATE TABLE conta_conjunta (
	Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_cliente1) REFERENCES cliente,
    FOREIGN KEY (id_cliente2) REFERENCES cliente,
    FOREIGN KEY (id_conta) REFERENCES conta
);


CREATE TABLE usuario_banco (
	Id number NOT NULL PRIMARY KEY,
    nome varchar(100) NOT NULL,
    tipo varchar(100) NOT NULL,
    FOREIGN KEY (id_banco) REFERENCES banco,
);

CREATE TABLE gerente(
	Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_usuario_banco) REFERENCES usuario_banco,
);

CREATE TABLE gerente_geral(
	Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_usuario_banco) REFERENCES usuario_banco,
);

CREATE TABLE saque(
    Id number NOT NULL PRIMARY KEY,
    valor number NOT NULL,
    FOREIGN KEY (id_conta) REFERENCES conta
);

CREATE TABLE deposito(
    Id number NOT NULL PRIMARY KEY,
    valor number NOT NULL,
    FOREIGN KEY (id_conta) REFERENCES conta
);

CREATE TABLE transferencia(
    Id number NOT NULL PRIMARY KEY,
    valor number NOT NULL,
    FOREIGN KEY (id_conta_origem) REFERENCES conta,
    FOREIGN KEY (id_conta_destino) REFERENCES conta
);

CREATE TABLE pagamento(
    Id number NOT NULL PRIMARY KEY,
    valor number NOT NULL,
    codigo_barras number NOT NULL,
    FOREIGN KEY (id_conta) REFERENCES conta
);

CREATE TABLE emprestimo(
    Id number NOT NULL PRIMARY KEY,
    valor number NOT NULL,
    prazo number NOT NULL,
    saldo number NOT NULL,
    FOREIGN KEY (id_conta) REFERENCES conta
);

CREATE TABLE emprestimo_g(
    Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_gerente) REFERENCES gerente
    FOREIGN KEY (id_emprestimo) REFERENCES emprestimo
);

CREATE TABLE emprestimo_g(
    Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_gerente_geral) REFERENCES gerente_geral
    FOREIGN KEY (id_emprestimo) REFERENCES emprestimo
);

CREATE TABLE emprestimo_aprovado(
    Id number NOT NULL PRIMARY KEY,
    FOREIGN KEY (id_emprestimo) REFERENCES emprestimo
);

INSERT INTO banco
VALUES (0, "banco1");

INSERT INTO banco
VALUES (1, "banco2");

INSERT INTO conta
VALUES (0, 100, 0);

INSERT INTO conta
VALUES (1, 100, 0);

INSERT INTO conta
VALUES (2, 100, 1);

INSERT INTO conta
VALUES (3, 100, 1);

INSERT INTO transferencia
VALUES (0, 100, 1, 0);

INSERT INTO transferencia
VALUES (1, 100, 2, 0);

INSERT INTO transferencia
VALUES (2, 100, 3, 0);

SELECT id
FROM conta 
WHERE saldo = 0;


"CREATE TABLE banco (\n"
                        + "	Id number NOT NULL PRIMARY KEY,\n"
                        + "	nome varchar (100) NOT NULL,\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE conta(\n"
                        + "	Id number NOT NULL PRIMARY KEY,\n"
                        + "    saldo number NOT NULL,\n"
                        + "    FOREIGN KEY (id_banco) REFERENCES banco,\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE transferencia(\n"
                        + "    Id number NOT NULL PRIMARY KEY,\n"
                        + "    valor number NOT NULL,\n"
                        + "    FOREIGN KEY (id_conta_origem) REFERENCES conta,\n"
                        + "    FOREIGN KEY (id_conta_destino) REFERENCES conta\n"
                        + ");\n"
                        + "\n"
                        + "CREATE TABLE emprestimo_aprovado(\n"
                        + "    Id number NOT NULL PRIMARY KEY,\n"
                        + "    valor number NOT NULL,\n"
                        + "    FOREIGN KEY (id_emprestimo) REFERENCES emprestimo\n"
                        + ")














