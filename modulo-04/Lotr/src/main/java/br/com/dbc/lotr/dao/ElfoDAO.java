
package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.service.PersonagemDTO;


public class ElfoDAO extends AbstractDAO<ElfoJoin>{

    public ElfoJoin parseFrom(PersonagemDTO dto, Usuario usuario) {
        ElfoJoin elfoEntity = null;
        if(dto.getId() != null)
            elfoEntity = buscar(dto.getId());
        if(elfoEntity == null)
            elfoEntity = new ElfoJoin();
        elfoEntity.setNome(dto.getNome());
        elfoEntity.setUsuario(usuario);
        elfoEntity.setDanoElfo(dto.getDanoElfo());
        return elfoEntity;
    }
    
    @Override
    protected Class<ElfoJoin> getEntityClass(){
        return ElfoJoin.class;
    }
    
}