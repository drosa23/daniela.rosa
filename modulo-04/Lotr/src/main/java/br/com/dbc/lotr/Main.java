package br.com.dbc.lotr;

import br.com.dbc.lotr.entity.Contato;
import br.com.dbc.lotr.entity.ElfoJoin;
import br.com.dbc.lotr.entity.ElfoPerClass;
import br.com.dbc.lotr.entity.ElfoTabelao;
import br.com.dbc.lotr.entity.Endereco;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.HobbitJoin;
import br.com.dbc.lotr.entity.HobbitPerClass;
import br.com.dbc.lotr.entity.HobbitTabelao;
import br.com.dbc.lotr.entity.RacaType;
import br.com.dbc.lotr.entity.TipoContato;
import br.com.dbc.lotr.entity.Usuario;
import br.com.dbc.lotr.service.EnderecoDTO;
import br.com.dbc.lotr.service.PersonagemDTO;
import br.com.dbc.lotr.service.UsuarioPersonagemDTO;
import br.com.dbc.lotr.service.UsuarioService;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        UsuarioService usuarioService = new UsuarioService();
        UsuarioPersonagemDTO dto = new UsuarioPersonagemDTO();
        
        dto.setNomeUsuario("Gustavo");
        dto.setApelidoUsuario("Gustavo");
        dto.setCpfUsuario(123l);
        dto.setSenhaUsuario("123");
        
        EnderecoDTO enderecoDTO = new EnderecoDTO();
        enderecoDTO.setCidade("Cidade do Gustavo");
        enderecoDTO.setBairro("Bairro do Gustavo");
        enderecoDTO.setLogradouro("Rua do Gustavo");
        enderecoDTO.setNumero(123);
        
        dto.setEndereco(enderecoDTO);
        
        PersonagemDTO personagemDTO = new PersonagemDTO();
        personagemDTO.setRaca(RacaType.ELFO);
        personagemDTO.setNome("Elfo do Gustavo");
        personagemDTO.setDanoElfo(123d);
        dto.setPersonagem(personagemDTO);
        
        usuarioService.cadastrarUsuarioEPersonagem(dto);
        System.exit(0);
        
    }
    
    public static void oldmain(String[] args) {
        Session session = null;
        Transaction transaction = null;

        try {
            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            
            Usuario usuario = new Usuario();
            usuario.setNome("Antonio");
            usuario.setApelido("Antonio");
            usuario.setCpf(1234l);
            usuario.setSenha("12345");
            
            Endereco endereco = new Endereco();
            endereco.setLogradouro("Rua do Antonio");
            endereco.setNumero(123);
            endereco.setCidade("Cidade do Antonio");
            endereco.setBairro("Bairro do Antonio");
            endereco.setComplemento("Apartamento do Antonio");
            
            Endereco endereco2 = new Endereco();
            endereco2.setLogradouro("Rua do Antonio");
            endereco2.setNumero(124);
            endereco2.setCidade("Cidade do Antonio");
            endereco2.setBairro("Bairro do Antonio");
            endereco2.setComplemento("Apartamento do Antonio");
            
            TipoContato tc = new TipoContato();
            tc.setNome("Celular");
            tc.setQuantidade(10);
            
            Contato contato = new Contato();
            contato.setTipoContato(tc);
            contato.setUsuario(usuario);
            contato.setValor("5199999999");
            
            usuario.pushContatos(contato);
            usuario.pushEnderecos(endereco, endereco2);
            session.save(usuario);
            
            ElfoTabelao elfoTabelao = new ElfoTabelao();
            elfoTabelao.setDanoElfo(100d);
            elfoTabelao.setNome("Elfo 1");
            session.save(elfoTabelao);
            
            HobbitTabelao hobbitTabelao = new HobbitTabelao();
            hobbitTabelao.setDanoHobbit(10d);
            hobbitTabelao.setNome("Hobbit 1");
            session.save(hobbitTabelao);
            
            ElfoPerClass elfoClass1 = new ElfoPerClass();
            elfoClass1.setNome("Legolas");
            elfoClass1.setDanoElfo(100d);
            session.save(elfoClass1);
            
            HobbitPerClass hobbitClass1 = new HobbitPerClass();
            hobbitClass1.setNome("Frodo");
            hobbitClass1.setDanoHobbit(110d);
            session.save(hobbitClass1);
            
            ElfoJoin elf1 = new ElfoJoin();
            elf1.setDanoElfo(100d);
            elf1.setNome("elfo30");
            session.save(elf1);
            
            HobbitJoin hob1 = new HobbitJoin();
            hob1.setDanoHobbit(110d);
            hob1.setNome("hobbit30");
            session.save(hob1);
            
            
            
            
            //BUSCA DE DADOS COM CRITERIA
            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.and(
                    Restrictions.ilike("endereco.bairro", "%do antonio"),
                    Restrictions.ilike("endereco.cidade", "%do antonio")
            ));
            List<Usuario> usuarios =  criteria.list();
            usuarios.forEach(System.out::println);
            
            criteria = session.createCriteria(Usuario.class);
            criteria.createAlias("enderecos", "endereco");
            criteria.add(Restrictions.and(
                    Restrictions.ilike("endereco.bairro", "%do antonio"),
                    Restrictions.ilike("endereco.cidade", "%do antonio")
            ));
            criteria.setProjection(Projections.count("id"));
            System.out.println(String.format("Foram encontrados %s registros "
                    + "com os criterios especificados", 
                    criteria.uniqueResult()));
            
            criteria.setProjection(Projections.sum("endereco.numero"));
            System.out.println(String.format("Foram somados numeros de registro "
                    + "e deu %s "
                    + "para os criterios especificados",
                    criteria.uniqueResult()));
            
            //BUSCA DE DADOS COM HQL
            usuarios = session.createQuery("select u from Usuario u "
                + " join u.enderecos endereco "
                + " where lower(endereco.cidade) like '%do antonio' "
                + " and lower(endereco.bairro) like '%do antonio' ").list();
            usuarios.forEach(System.out::println);
            
            
            Long count = (Long)session.createQuery("select count(distinct u.id) from Usuario u "
                + " join u.enderecos endereco "
                + " where lower(endereco.cidade) like '%do antonio' "
                + " and lower(endereco.bairro) like '%do antonio' ").uniqueResult();
            usuarios.forEach(System.out::println);
            System.out.println(String.format("Contamos com HQL %s usuarios", count));
            
            count = (Long)session.createQuery("select count(endereco.id) from Usuario u "
                + " join u.enderecos endereco "
                + " where lower(endereco.cidade) like '%do antonio' "
                + " and lower(endereco.bairro) like '%do antonio' ").uniqueResult();
            usuarios.forEach(System.out::println);
            System.out.println(String.format("Contamos com HQL %s enderecos", count));
            
            count = (Long)session
                    .createQuery("select sum(endereco.numero) from Usuario u "
                + " join u.enderecos endereco "
                + " where lower(endereco.cidade) like '%do antonio' "
                + " and lower(endereco.bairro) like '%do antonio' ").uniqueResult();
            usuarios.forEach(System.out::println);
            System.out.println(String.format("Somamos com HQL os numeros "
                    + "dos enderecos e deu %s", count));
            
            
            
            transaction.commit();
        } catch (Exception e) {
            if(transaction != null){
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, e.getMessage(), e);
            System.exit(1);
        } finally {
            if(session != null){
                session.close();   
            }
        }
        System.exit(0);
    }
}
