/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author daniela.amaral
 */
@Entity
@Table(name = "USUARIO")
public class Usuario extends AbstractEntity{

    @Id
    @SequenceGenerator(allocationSize = 1, name = "usuario_seq", sequenceName = "usuario_seq")
    @GeneratedValue(generator = "usuario_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;
    private String apelido;
    private Long cpf;
    private String senha;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "usuario_endereco",
            joinColumns = {
                @JoinColumn(name = "id_usuario")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_endereco")})
    private List<Endereco> enderecos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
    private List<Contato> contatos = new ArrayList<>();
    
    @OneToMany(mappedBy = "usuario")
    private List<PersonagemJoin> personagens = new ArrayList<>();

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }
    
    public List<Endereco> getEnderecos() {
        return enderecos;
    }
    
    public void pushEnderecos(Endereco ... enderecos){
        this.enderecos.addAll(Arrays.asList(enderecos));
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void pushContatos(Contato ... contatos) {
        this.contatos.addAll(Arrays.asList(contatos));
    }

    public List<PersonagemJoin> getPersonagens() {
        return personagens;
    }
    
    public void pushPersonagens(PersonagemJoin ... personagens) {
        this.personagens.addAll(Arrays.asList(personagens));
    }
    

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nome=" + nome + ", apelido=" + apelido + ", cpf=" + cpf + ", senha=" + senha +  '}';
    }
    
    

}
