package br.com.dbc.lotr.dao;

import br.com.dbc.lotr.entity.AbstractEntity;
import br.com.dbc.lotr.entity.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class AbstractDAO <E extends AbstractEntity>{
    protected abstract Class <E> getEntityClass();
    public void criar(E entity){
        Session session = HibernateUtil.getSession();
//        Transaction transaction = session.beginTransaction();
        session.save(entity);
//        transaction.commit();
    }
    
    public void atualizar(E entity){
        criar(entity);
    }
    
    public void remover(Integer id){
        Session session = HibernateUtil.getSession();
        session.createQuery("delete from " + getEntityClass().getSimpleName()
        + " where id = " + id).executeUpdate();
    }
    
    public void remover(E entity){
        remover(entity.getId());
    }
    
    public E buscar(Integer id){ 
        Session session = HibernateUtil.getSession();
        return (E) session.createCriteria("select e from" + getEntityClass().getSimpleName()
        + " where id = " + id).uniqueResult();
        
    }
    
    public List<E> listar(){ 
        Session session = HibernateUtil.getSession();
        return session.createCriteria(getEntityClass()).list();
    }
}