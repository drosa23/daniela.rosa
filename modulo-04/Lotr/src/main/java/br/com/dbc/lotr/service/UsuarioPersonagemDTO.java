/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.lotr.service;

public class UsuarioPersonagemDTO {
    
    private Integer idUsuario;
    private String nomeUsuario;
    private String apelidoUsuario;
    private Long cpfUsuario;
    private String senhaUsuario;
    private EnderecoDTO endereco;
    
    //Personagem
    private PersonagemDTO personagem;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public String getApelidoUsuario() {
        return apelidoUsuario;
    }

    public void setApelidoUsuario(String apelidoUsuario) {
        this.apelidoUsuario = apelidoUsuario;
    }

    public Long getCpfUsuario() {
        return cpfUsuario;
    }

    public void setCpfUsuario(Long cpfUsuario) {
        this.cpfUsuario = cpfUsuario;
    }

    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }

    public EnderecoDTO getEndereco() {
        return endereco;
    }

    public void setEndereco(EnderecoDTO endereco) {
        this.endereco = endereco;
    }

    public PersonagemDTO getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemDTO personagem) {
        this.personagem = personagem;
    }

    
    
    
    public EnderecoDTO getEnderecoUsuario() {
        return endereco;
    }

    public void setEnderecoUsuario(EnderecoDTO enderecoUsuario) {
        this.endereco = enderecoUsuario;
    }
    
    
}