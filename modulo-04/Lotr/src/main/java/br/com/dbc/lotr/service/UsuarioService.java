/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.service;

import br.com.dbc.lotr.dao.UsuarioDAO;
import br.com.dbc.lotr.entity.HibernateUtil;
import br.com.dbc.lotr.entity.Usuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author daniela.amaral
 */
public class UsuarioService {

    private static final UsuarioDAO USUARIO_DAO = new UsuarioDAO();
    private static final PersonagemService PERSONAGEM_SERVICE
            = new PersonagemService();
    private static final Logger LOG = Logger.getLogger(UsuarioService.class.getName());

    public void cadastrarUsuarioEPersonagem(UsuarioPersonagemDTO dto) {
        boolean started = HibernateUtil.beginTransaction();
        Transaction transaction = HibernateUtil.getSession().getTransaction();
        try {
            Usuario usuario = USUARIO_DAO.parseFrom(dto);
            USUARIO_DAO.criar(usuario);

            PERSONAGEM_SERVICE.salvarPersonagem(dto.getPersonagem(), usuario);

            if (started) {
                transaction.commit();
            }
        } catch (Exception e) {
            transaction.rollback();
            LOG.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    
}
