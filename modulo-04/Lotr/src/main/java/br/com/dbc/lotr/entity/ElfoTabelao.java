/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "ELFO_TABELAO")
@DiscriminatorValue("ELFO")
public class ElfoTabelao extends PersonagemTabelao{
    @Column(name = "DANO_ELFO")
    private Double danoElfo;

    public Double getDanoElfo() {
        return danoElfo;
    }

    public void setDanoElfo(Double danoElfo) {
        this.danoElfo = danoElfo;
    }
}
