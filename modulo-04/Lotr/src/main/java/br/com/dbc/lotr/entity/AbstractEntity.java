/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.lotr.entity;

import java.io.Serializable;

/**
*
* @author daniela.amaral
*/
public abstract class AbstractEntity implements Serializable{
    public abstract Integer getId();
}