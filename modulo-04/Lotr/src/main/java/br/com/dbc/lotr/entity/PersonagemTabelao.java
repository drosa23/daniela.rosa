/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PERONAGEM_TABELAO")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn (name = "raca", discriminatorType = DiscriminatorType.STRING)
public class PersonagemTabelao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_TAB_SEQ", sequenceName = "PERSONAGEM_TAB_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_TAB_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer Id;
   
    private String nome;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
    
    
    
}
