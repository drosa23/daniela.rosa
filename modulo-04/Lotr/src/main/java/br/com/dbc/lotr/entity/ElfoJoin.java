/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.lotr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name = "ELFO_Join")
@PrimaryKeyJoinColumn(name = "id_personagem")
public class ElfoJoin extends PersonagemJoin{

    public ElfoJoin() {
        super.setRaca(RacaType.ELFO);
    }
    
    @Column(name = "DANO_ELFO")
    private Double danoElfo;

    public Double getDanoElfo() {
        return danoElfo;
    }

    public void setDanoElfo(Double danoElfo) {
        this.danoElfo = danoElfo;
    }
}
