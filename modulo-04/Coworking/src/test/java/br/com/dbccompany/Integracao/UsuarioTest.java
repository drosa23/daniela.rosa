package br.com.dbccompany.Integracao;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Service.UsuarioService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;


@RunWith(SpringRunner.class)

public class UsuarioTest extends CoworkingApplicationTests {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    ApplicationContext context;

//    @Before
//    public void setUp(){
//        Usuarios usuario = new Usuarios();
//        usuario.setNome("Marlon");
//        usuario.setEmail("teste@email");
//        usuario.setLogin("@marlon");
//        usuario.setSenha("xavrauuuuu");
//        Mockito.when(usuarioRepository.findByNome(usuario.getNome())).thenReturn(usuario);
//    }

    @Test
    public void testarSenhaCriptograda() {
        Usuario usuario = new Usuario();
        usuario.setNome("Marlon");
        usuario.setEmail("teste@email");
        usuario.setLogin("@marlon");
        usuario.setSenha("xavrauuuuu");

        try {
            usuarioService.salvar(usuario);
        } catch (Exception e) {
        } finally {
            Usuario result = usuarioService.buscarNome("Marlon");
            System.out.println(result.getNome() + "/n" + result.getSenha());
            boolean resultado = !(result.getSenha().equals("xavrauuuuu"));
            assertThat(resultado).isEqualTo(true);
        }
    }

    @Test
    public void testarGerarExcecaoParaSenhaPequena(){
        Usuario usuario = new Usuario();
        usuario.setNome("Marlon");
        usuario.setEmail("teste@email");
        usuario.setLogin("@marlon");
        usuario.setSenha("a");

        try {
            usuarioService.salvar(usuario);
            fail("Falha. Uma exceção deve ser lançada!");
        } catch (Exception e) {
            assertThat(e).isEqualTo(e);
        }

    }
}
