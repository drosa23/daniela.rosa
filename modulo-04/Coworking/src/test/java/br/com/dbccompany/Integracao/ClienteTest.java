package br.com.dbccompany.Integracao;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.ClienteService;
import br.com.dbccompany.Service.ContatoService;
import br.com.dbccompany.Service.TipoContatoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;


@RunWith(SpringRunner.class)

public class ClienteTest extends CoworkingApplicationTests {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Autowired
    ApplicationContext context;

//    @Before
//    public void setUp(){
//        Usuarios usuario = new Usuarios();
//        usuario.setNome("Marlon");
//        usuario.setEmail("teste@email");
//        usuario.setLogin("@marlon");
//        usuario.setSenha("xavrauuuuu");
//        Mockito.when(usuarioRepository.findByNome(usuario.getNome())).thenReturn(usuario);
//    }

    @Test
    public void testarGerarExcecaoParaNaoTer2ContatosCadastrados(){
        Cliente cliente = new Cliente();
        cliente.setNome("Dani");
        cliente.setCpf("022291112");

        TipoContato tipoContato1 = new TipoContato();
        tipoContato1.setNome("email");
        TipoContato tipoContato2 = new TipoContato();
        tipoContato2.setNome("telefone");

        Contato contato1 = new Contato();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("dani@dani");
        contato1.setCliente(cliente);

        Contato contato2 = new Contato();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("984864572");
//        contato2.setCliente(cliente);
        List<Contato> contatos = new ArrayList<>();
        contatos.add(contato1);
//        contatos.add(contato2);
        cliente.setContatos(contatos);

        try {
            clienteService.salvar(cliente);
            fail("Falha. Uma exceção deve ser lançada!");
        } catch (Exception e) {
            assertThat(e).isEqualTo(e);
        }

    }

    @Test
    public void testarNaoGerarExcecaoPara2ContatosCadastrados(){
        Cliente cliente = new Cliente();
        cliente.setNome("Dani");
        cliente.setCpf("022291112");

        TipoContato tipoContato1 = new TipoContato();
        tipoContato1.setNome("email");
        TipoContato tipoContato2 = new TipoContato();
        tipoContato2.setNome("telefone");

        Contato contato1 = new Contato();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("dani@dani");
        contato1.setCliente(cliente);

        Contato contato2 = new Contato();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("984864572");
        contato2.setCliente(cliente);
        List<Contato> contatos = new ArrayList<>();
        contatos.add(contato1);
        contatos.add(contato2);
        cliente.setContatos(contatos);

        try {
            Cliente resultado = clienteService.salvar(cliente);
            assertThat(resultado.getNome()).isEqualTo(cliente.getNome());
        } catch (Exception e) {
        }

    }

}

