package br.com.dbccompany.Integracao;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class TipoContatoTest extends CoworkingApplicationTests {

    @MockBean
    private TipoContatoRepository tipoContatoRepository;

    @Autowired
    ApplicationContext context;

    @Before
    public void setUp() {
        TipoContato tipoContato = new TipoContato();
        tipoContato.setNome("email");

        Mockito.when(tipoContatoRepository.findByNome(tipoContato.getNome())).thenReturn(tipoContato);
    }

    @Test
    public void acharTipoContatoPorNome() {
        String nome = "email";
        TipoContato found = tipoContatoRepository.findByNome(nome);
        assertThat(found.getNome()).isEqualTo(nome);
    }

}
