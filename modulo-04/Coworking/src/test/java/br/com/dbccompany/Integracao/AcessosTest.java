package br.com.dbccompany.Integracao;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import br.com.dbccompany.Service.AcessosService;
import br.com.dbccompany.Service.ClienteService;
import br.com.dbccompany.Service.EspacoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)

public class AcessosTest extends CoworkingApplicationTests{
    @Autowired
    private AcessosService acessosService;
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private EspacoService espacoService;
    @MockBean
    private SaldoClienteRepository saldoClienteRepository;


    @Autowired
    ApplicationContext context;

    @Before
    public void setUp() {
        Cliente cliente = new Cliente();
        cliente.setNome("Dani");cliente.setCpf("022291112");
        TipoContato tipoContato1 = new TipoContato();tipoContato1.setNome("email");TipoContato tipoContato2 = new TipoContato();tipoContato2.setNome("telefone");
        Contato contato1 = new Contato();contato1.setTipoContato(tipoContato1);contato1.setValor("dani@dani");contato1.setCliente(cliente);
        Contato contato2 = new Contato();contato2.setTipoContato(tipoContato2);contato2.setValor("984864572");contato2.setCliente(cliente);
        List<Contato> contatos = new ArrayList<>();contatos.add(contato1);contatos.add(contato2);
        cliente.setContatos(contatos);
        try { clienteService.salvar(cliente); } catch (Exception e) { }

        Espaco espaco = new Espaco();espaco.setNome("a"); espaco.setValorString("R$ 100"); espaco.setQtdPessoas(10);
        espacoService.salvar(espaco);

        SaldoClienteId saldoId = new SaldoClienteId();
        saldoId.setCliente(cliente);
        saldoId.setEspaco(espaco);

        SaldoCliente saldoCliente = new SaldoCliente();
        saldoCliente.setId(saldoId);
        saldoCliente.setVencimento(LocalDateTime.now().plusDays(60));
        saldoCliente.setTipoContratacao(TipoContratacao.MESES);
        saldoCliente.setQuantidade(10);
        Mockito.when(saldoClienteRepository.findByQuantidade(10)).thenReturn(saldoCliente);
    }
    @Transactional
    @Test
    public void testaSeNaoSetandoUmaDataEhAtribuidaDataHoraAtual(){
        Acessos acesso = new Acessos();
        SaldoCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        acesso.setSaldoCliente(encontrado);
        acessosService.salvar(acesso);
        LocalDateTime data = acesso.getData();
        assertThat(data).isNotNull();
        assertThat(LocalDateTime.now()).isEqualToIgnoringSeconds(data);
    }

    @Transactional
    @Test
    public void testaSeSaldoVencidoRetornaMensagemDeSaldoInvalido(){
        Acessos acesso = new Acessos();
        SaldoCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        encontrado.setVencimento(LocalDateTime.now().minusDays(10));
        acesso.setSaldoCliente(encontrado);
        String retorno = acessosService.salvar(acesso);
        assertThat("O saldo esta vencido").isEqualTo(retorno);
    }

    @Transactional
    @Test
    public void testaRetornoDeSaldoValido(){
        Acessos acesso = new Acessos();
        SaldoCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        acesso.setSaldoCliente(encontrado);
        String retorno = acessosService.salvar(acesso);
        assertThat("Saldo:" + acesso.getSaldoCliente().getVencimento()).isEqualTo(retorno);
    }

    @Transactional
    @Test
    public void testaRetornoDeNaoTerSaldo(){
        Acessos acesso = new Acessos();
        SaldoCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        encontrado.setQuantidade(0);
        acesso.setSaldoCliente(encontrado);
        String retorno = acessosService.salvar(acesso);
        assertThat("Saldo Insuficiente").isEqualTo(retorno);
    }

    @Transactional
    @Test
    public void testaAtribuicaoDeIsEntradaTrue(){
        Acessos acesso = new Acessos();
        SaldoCliente encontrado = saldoClienteRepository.findByQuantidade(10);
        acesso.setSaldoCliente(encontrado);
        acessosService.salvar(acesso);
        boolean retorno = acesso.getisEntrada();
        assertThat(retorno).isTrue();
    }
}
