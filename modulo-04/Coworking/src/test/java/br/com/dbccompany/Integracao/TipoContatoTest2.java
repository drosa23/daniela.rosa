package br.com.dbccompany.Integracao;

import br.com.dbccompany.Controller.TipoContatoController;
import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Service.TipoContatoService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


public class TipoContatoTest2 extends CoworkingApplicationTests{

    private MockMvc mvc;

    @Mock
    private TipoContatoService tipoContatoService;

    @Autowired
    private TipoContatoController controller;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void test_get_all_sucess() throws Exception{
        this.mvc.perform(MockMvcRequestBuilders.get("/api/tipoContato/")).andExpect(MockMvcResultMatchers.status().isOk());
    }




}
