package br.com.dbccompany.Integracao;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Service.ClienteService;
import br.com.dbccompany.Service.ContratacaoService;
import br.com.dbccompany.Service.EspacoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)

public class ContratacaoTest extends CoworkingApplicationTests {

    @Autowired
    private ContratacaoService contratacaoService;

    @Autowired
    private EspacoService espacoService;

    @Autowired
    private ClienteService clienteService;



    @Autowired
    ApplicationContext context;

//    @Before
//    public void setUp(){
//        Usuarios usuario = new Usuarios();
//        usuario.setNome("Marlon");
//        usuario.setEmail("teste@email");
//        usuario.setLogin("@marlon");
//        usuario.setSenha("xavrauuuuu");
//        Mockito.when(usuarioRepository.findByNome(usuario.getNome())).thenReturn(usuario);
//    }

    @Test
    public void testarSalvarRetornaValorCobrado(){
        Cliente cliente = new Cliente();
        cliente.setNome("Dani");
        cliente.setCpf("022291112");
        TipoContato tipoContato1 = new TipoContato();
        tipoContato1.setNome("email");
        TipoContato tipoContato2 = new TipoContato();
        tipoContato2.setNome("telefone");

        Contato contato1 = new Contato();
        contato1.setTipoContato(tipoContato1);
        contato1.setValor("dani@dani");
        contato1.setCliente(cliente);

        Contato contato2 = new Contato();
        contato2.setTipoContato(tipoContato2);
        contato2.setValor("984864572");
        contato2.setCliente(cliente);
        List<Contato> contatos = new ArrayList<>();
        contatos.add(contato1);
        contatos.add(contato2);
        cliente.setContatos(contatos);
        try {
            clienteService.salvar(cliente);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Espaco espaco = new Espaco();
        espaco.setNome("Cafe");
        espaco.setValorString("R$ 10");
        espaco.setQtdPessoas(100);
        espacoService.salvar(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setCliente(cliente);
        contratacao.setEspaco(espacoService.buscarNome("Cafe"));
        contratacao.setQuantidade(10);
        contratacao.setTipoContratacao(TipoContratacao.DIARIAS);

        String valorCobrado = contratacaoService.salvar(contratacao);
        assertThat(valorCobrado).isEqualTo("Valor a ser cobrado: R$100.0");
    }


}

