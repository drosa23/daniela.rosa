package br.com.dbccompany.Integracao;

import br.com.dbccompany.CoworkingApplicationTests;
import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)

public class PagamentoTest extends CoworkingApplicationTests {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    private ClientePacoteService clientePacoteService;
    @Autowired
    private PacoteService pacoteService;
    @Autowired
    private ContratacaoService contratacaoService;
    @Autowired
    private EspacoService espacoService;
    @Autowired
    private SaldoClienteService saldoClienteService;
    @Autowired
    private PagamentoService pagamentoService;
    @Autowired
    private EspacoPacoteService espacoPacoteService;


    @Autowired
    ApplicationContext context;

    @Transactional
    @Test
    public void testarGerarPagamentoGera1SaldoCliente(){
        Cliente cliente = new Cliente();
        cliente.setNome("Dani");cliente.setCpf("022291112");
        TipoContato tipoContato1 = new TipoContato();tipoContato1.setNome("email");TipoContato tipoContato2 = new TipoContato();tipoContato2.setNome("telefone");
        Contato contato1 = new Contato();contato1.setTipoContato(tipoContato1);contato1.setValor("dani@dani");contato1.setCliente(cliente);
        Contato contato2 = new Contato();contato2.setTipoContato(tipoContato2);contato2.setValor("984864572");contato2.setCliente(cliente);
        List<Contato> contatos = new ArrayList<>();contatos.add(contato1);contatos.add(contato2);
        cliente.setContatos(contatos);
        try { clienteService.salvar(cliente); } catch (Exception e) { }

        Espaco espaco = new Espaco();espaco.setNome("a"); espaco.setValorString("R$ 100"); espaco.setQtdPessoas(10);
        espacoService.salvar(espaco);

        Contratacao contratacao = new Contratacao();
        contratacao.setEspaco(espaco);contratacao.setCliente(cliente);contratacao.setPrazo(30);contratacao.setTipoContratacao(TipoContratacao.MESES);contratacao.setDesconto(20.0);contratacao.setQuantidade(5);
        contratacaoService.salvar(contratacao);

        Pagamento pagamento = new Pagamento();pagamento.setContratacao(contratacao);pagamento.setTipoPagamento(TipoPagamento.DEBITO);

        int quantidadeAntes = saldoClienteService.allSaldosClientes().size();
        pagamentoService.salvar(pagamento);
        int quantidadeDepois = saldoClienteService.allSaldosClientes().size();
        assertThat(quantidadeAntes).isEqualTo(0);
        assertThat(quantidadeDepois).isEqualTo(1);
    }

    @Test
   public void testarGerarPagamentoCom2EspacosDeveGerar2SaldoCliente(){
        Cliente cliente = new Cliente();
        cliente.setNome("Dani");cliente.setCpf("022291112");
        TipoContato tipoContato1 = new TipoContato();tipoContato1.setNome("email");TipoContato tipoContato2 = new TipoContato();tipoContato2.setNome("telefone");
        Contato contato1 = new Contato();contato1.setTipoContato(tipoContato1);contato1.setValor("dani@dani");contato1.setCliente(cliente);
        Contato contato2 = new Contato();contato2.setTipoContato(tipoContato2);contato2.setValor("984864572");contato2.setCliente(cliente);
        List<Contato> contatos = new ArrayList<>();contatos.add(contato1);contatos.add(contato2);
        cliente.setContatos(contatos);
        try { clienteService.salvar(cliente); } catch (Exception e) { }

        Espaco espaco = new Espaco();espaco.setNome("a"); espaco.setValorString("R$ 100"); espaco.setQtdPessoas(10);
        espacoService.salvar(espaco);
        Espaco espaco2 = new Espaco();espaco2.setNome("b"); espaco2.setValorString("R$ 100"); espaco2.setQtdPessoas(10);
        espacoService.salvar(espaco2);

        Pacote pacote = new Pacote();pacote.setValorString("R$ 100");pacoteService.salvar(pacote);
        ClientePacote clientePacote = new ClientePacote();clientePacote.setCliente(cliente);clientePacote.setPacote(pacote);clientePacote.setQuantidade(10);
        clientePacoteService.salvar(clientePacote);

        EspacoPacote espacoPacote = new EspacoPacote();espacoPacote.setEspaco(espaco);espacoPacote.setPacote(pacote);espacoPacote.setPrazo(30);
        espacoPacote.setQuantidade(10);espacoPacote.setTipoContratacao(TipoContratacao.MESES);
        EspacoPacote espacoPacote2 = new EspacoPacote();espacoPacote2.setEspaco(espaco2);espacoPacote2.setPacote(pacote);espacoPacote2.setPrazo(30);
        espacoPacote2.setQuantidade(10);espacoPacote2.setTipoContratacao(TipoContratacao.MESES);
        espacoPacoteService.salvar(espacoPacote);
        espacoPacoteService.salvar(espacoPacote2);

        Pagamento pagamento = new Pagamento();pagamento.setClientePacote(clientePacote);pagamento.setTipoPagamento(TipoPagamento.DEBITO);

        int quantidadeAntes = saldoClienteService.allSaldosClientes().size();
        pagamentoService.salvar(pagamento);
        int quantidadeDepois = saldoClienteService.allSaldosClientes().size();
        assertThat(quantidadeAntes).isEqualTo(0);
        assertThat(quantidadeDepois).isEqualTo(2);
    }
}


