package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {
    @Autowired
    ContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contato> lstContatos(){
        return service.allContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Contato novoContato(@RequestBody Contato b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Contato editarContato(@PathVariable long id, @RequestBody Contato b) {
        return service.editarDados(id, b);
    }
}
