package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {
    @Autowired
    AcessosService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Acessos> lstAcessos(){
        return service.allAcessos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public String novoAcesso(@RequestBody Acessos b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Acessos editarAcessos(@PathVariable long id, @RequestBody Acessos b) {
        return service.editarDados(id, b);
    }
}
