package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {
    @Autowired
    private ContratacaoRepository repository;

    @Autowired
    private EspacoService esp;

    @Autowired
    private ClienteService clienteService;

    @Transactional(rollbackFor = Exception.class)
    public String salvar(Contratacao obj) {
        Cliente cliente = clienteService.buscarCliente(obj.getCliente().getId()).get();
        Espaco espaco = esp.buscarEspaco(obj.getEspaco().getId()).get();
        obj.setCliente(cliente);
        obj.setEspaco(espaco);

        double valor = obj.getEspaco().getValor();
        double valorCobrado = obj.getQuantidade()* valor;
        repository.save(obj);
        return "Valor a ser cobrado: R$" + valorCobrado;
    }

    public Optional<Contratacao> buscarContratacao(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao editarDados(long id, Contratacao obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Contratacao> allContratacoes(){
        return (List<Contratacao>) repository.findAll();
    }

    public Contratacao buscarPorEspaco(Espaco espaco) {
        return repository.findByEspaco(espaco);
    }

    public Contratacao buscarPorCliente(Cliente cliente) {
        return repository.findByCliente(cliente);
    }
}
