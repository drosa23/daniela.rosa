package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {
    @Autowired
    PagamentoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pagamento> lstPagamentos(){
        return service.allPagamentos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pagamento novoPagamento(@RequestBody Pagamento b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Pagamento editarPagamento(@PathVariable long id, @RequestBody Pagamento b) {
        return service.editarDados(id, b);
    }
}
