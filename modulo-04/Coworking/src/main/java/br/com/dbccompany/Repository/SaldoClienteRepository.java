package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;

public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteId> {
    SaldoCliente findByQuantidade (int quantidade);
}
