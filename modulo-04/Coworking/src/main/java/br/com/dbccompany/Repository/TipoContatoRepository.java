package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

public interface TipoContatoRepository extends CrudRepository<TipoContato, Long> {
    TipoContato findByNome(String nome);
}
