package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pagamento.class)
public class Pagamento {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "pagamento_seq", sequenceName = "pagamento_seq")
    @GeneratedValue(generator = "pagamento_seq", strategy = GenerationType.SEQUENCE)

    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_clientes_pacotes", nullable = true)
    private ClientePacote clientePacote;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_contratacao", nullable = true)
    private Contratacao contratacao;

    @Column(name = "tipo_pagamento")
    private TipoPagamento tipoPagamento;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ClientePacote getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientePacote clientePacote) {
        this.clientePacote = clientePacote;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
