package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {
}
