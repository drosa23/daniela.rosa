package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {
    @Autowired
    PacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pacote> lstPacotes(){
        return service.allPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pacote novoPacote(@RequestBody Pacote b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Pacote editarPacote(@PathVariable long id, @RequestBody Pacote b) {
        return service.editarDados(id, b);
    }
}
