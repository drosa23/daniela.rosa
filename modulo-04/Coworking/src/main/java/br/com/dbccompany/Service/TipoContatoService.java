package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {
    @Autowired
    private TipoContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContato salvar(TipoContato obj) {
        return repository.save(obj);
    }

    public Optional<TipoContato> buscarTipoContato(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContato editarDados(long id, TipoContato obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<TipoContato> allTiposContatos(){
        return (List<TipoContato>) repository.findAll();
    }

    public TipoContato buscarNome(String nome) {
        return repository.findByNome(nome);
    }
}
