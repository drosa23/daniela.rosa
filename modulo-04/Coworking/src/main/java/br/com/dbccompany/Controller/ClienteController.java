package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {
    @Autowired
    ClienteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Cliente> lstClientes(){
        return service.allClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Cliente novoCliente(@RequestBody Cliente b) {
        try {
            return service.salvar(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Cliente editarCliente(@PathVariable long id, @RequestBody Cliente b) {
        return service.editarDados(id, b);
    }
}
