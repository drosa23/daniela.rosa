package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {
    @Autowired
    SaldoClienteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<SaldoCliente> lstSaldoClientes(){
        return service.allSaldosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente b) {
        return service.salvar(b);
    }

    /*
    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable long id, @RequestBody SaldoCliente b) {
        return service.editarDados(id, b);
    } */
}
