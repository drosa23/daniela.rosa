package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientePacote")
public class ClientePacoteController {
    @Autowired
    ClientePacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<ClientePacote> lstCliPacotes(){
        return service.allClientesPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public ClientePacote novoClientePacote(@RequestBody ClientePacote b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public ClientePacote editarClientePacote(@PathVariable long id, @RequestBody ClientePacote b) {
        return service.editarDados(id, b);
    }
}
