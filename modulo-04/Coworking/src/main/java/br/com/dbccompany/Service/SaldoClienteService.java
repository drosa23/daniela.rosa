package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Entity.SaldoClienteId;
import br.com.dbccompany.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {
    @Autowired
    private SaldoClienteRepository repository;

    public LocalDateTime calcularVencimento(long prazo){
        LocalDateTime data = LocalDateTime.now();
        return data.plusDays(prazo);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente obj) {
        return repository.save(obj);
    }

    public Optional<SaldoCliente> buscarSaldoCliente(SaldoClienteId id) {
        return repository.findById(id);
    }

    public SaldoCliente buscarQuantidade(int quantidade) {
        return repository.findByQuantidade(quantidade);
    }


    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editarDados(SaldoClienteId id, SaldoCliente obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<SaldoCliente> allSaldosClientes(){
        return (List<SaldoCliente>) repository.findAll();
    }
}
