package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Acessos.class)
public class Acessos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "acessos_seq", sequenceName = "acessos_seq")
    @GeneratedValue(generator = "acessos_seq", strategy = GenerationType.SEQUENCE)

    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    private SaldoCliente saldoCliente;

    @Column(name = "is_Entrada")
    private boolean isEntrada;

    private LocalDateTime data;

    @Column(name = "is_Excecao")
    private boolean isExcecao;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getisEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public boolean getisExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
