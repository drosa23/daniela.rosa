package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Entity.Espaco;
import org.springframework.data.repository.CrudRepository;

public interface ContratacaoRepository extends CrudRepository<Contratacao, Long> {
    Contratacao findByEspaco(Espaco espaco);
    Contratacao findByCliente(Cliente cliente);
}
