package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Contratacao;
import br.com.dbccompany.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {
    @Autowired
    ContratacaoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contratacao> lstContratacoes(){
        return service.allContratacoes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public String novoContratacao(@RequestBody Contratacao b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Contratacao editarContratacao(@PathVariable long id, @RequestBody Contratacao b) {
        return service.editarDados(id, b);
    }
}
