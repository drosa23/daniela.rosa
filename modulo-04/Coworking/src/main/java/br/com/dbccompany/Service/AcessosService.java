package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Entity.SaldoCliente;
import br.com.dbccompany.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {
    @Autowired
    private AcessosRepository repository;

    @Autowired
    private SaldoClienteService saldoClienteService;


    @Transactional(rollbackFor = Exception.class)
    public String salvar(Acessos obj) {
        List<Acessos> acessos = repository.findBySaldoCliente(obj.getSaldoCliente());

        Acessos ultimoAcesso = new Acessos();
        if(acessos.size() > 0){
            ultimoAcesso = acessos.get(acessos.size() - 1);

            if(ultimoAcesso.getisEntrada()){ obj.setEntrada(false); }
            else{ obj.setEntrada(true); }
        }
       else{ obj.setEntrada(true); }

       if(obj.getData() == null){
            obj.setData(LocalDateTime.now());
       }
        if(obj.getisEntrada()){
            LocalDateTime vencimento = obj.getSaldoCliente().getVencimento();
            LocalDateTime dataAtual = LocalDateTime.now();

            if(obj.getSaldoCliente() != null && obj.getSaldoCliente().getQuantidade() > 0){
                if(dataAtual.isAfter(vencimento)){
                    obj.getSaldoCliente().setQuantidade(0);
                    return "O saldo esta vencido";
                }
                repository.save(obj);
                SaldoCliente saldoCliente = obj.getSaldoCliente();
                saldoCliente.adicionarAcessos(obj);
                saldoClienteService.editarDados(saldoCliente.getId(), obj.getSaldoCliente());
                return "Saldo:" + obj.getSaldoCliente().getVencimento();
            }
            else{
                return "Saldo Insuficiente";
            }
        }

        else{
            LocalDateTime horaEntrada = ultimoAcesso.getData();
            long minutes = ChronoUnit.MINUTES.between(horaEntrada, obj.getData());
            long hours = ChronoUnit.HOURS.between(horaEntrada, obj.getData());
            long days = ChronoUnit.DAYS.between(horaEntrada, obj.getData());
            long weeks = ChronoUnit.WEEKS.between(horaEntrada, obj.getData());
            SaldoCliente novoSaldo = new SaldoCliente();
            novoSaldo.setQuantidade(obj.getSaldoCliente().getQuantidade());
            novoSaldo.setId(obj.getSaldoCliente().getId());
            novoSaldo.setTipoContratacao(obj.getSaldoCliente().getTipoContratacao());
            novoSaldo.setVencimento(obj.getSaldoCliente().getVencimento().minusHours(hours).minusMinutes(minutes).minusDays(days).minusWeeks(weeks));
            saldoClienteService.salvar(novoSaldo);
        }
        repository.save(obj);
        return "Acesso salvo";
    }

    public Optional<Acessos> buscarAcesso(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acessos editarDados(long id, Acessos obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Acessos> allAcessos(){
        return (List<Acessos>) repository.findAll();
    }

    public Acessos buscarPorData(LocalDateTime data) {
        return repository.findByData(data);
    }
}
