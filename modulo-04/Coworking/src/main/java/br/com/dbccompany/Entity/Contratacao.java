package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Contratacao.class)
public class Contratacao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "contratacao_seq", sequenceName = "contratacao_seq")
    @GeneratedValue(generator = "contratacao_seq", strategy = GenerationType.SEQUENCE)

    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_espacos")
    private Espaco espaco;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_clientes")
    private Cliente cliente;

    @Column(name = "tipo_contratacao", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private long quantidade;

    private double desconto;

    @Column(nullable = false)
    private long prazo;

    @OneToMany(mappedBy = "contratacao")
    private List<Pagamento> pagamentos = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public long getPrazo() {
        return prazo;
    }

    public void setPrazo(long prazo) {
        this.prazo = prazo;
    }

    public List<Pagamento> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamento> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
