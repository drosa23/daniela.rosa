package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {
    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<TipoContato> lstTipoContatos(){
        return service.allTiposContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public TipoContato novoTipoContato(@RequestBody TipoContato b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public TipoContato editarTipoContato(@PathVariable long id, @RequestBody TipoContato b) {
        return service.editarDados(id, b);
    }
}
