package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.EspacoPacote;
import org.springframework.data.repository.CrudRepository;

public interface EspacoPacoteRepository extends CrudRepository<EspacoPacote, Long> {

}
