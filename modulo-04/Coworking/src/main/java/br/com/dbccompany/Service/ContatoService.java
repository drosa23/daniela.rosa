package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import br.com.dbccompany.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {
    @Autowired
    private ContatoRepository repository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private TipoContatoService tipoService;

    @Transactional(rollbackFor = Exception.class)
    public Contato salvar(Contato obj) {
        Cliente cliente = clienteService.buscarCliente(obj.getCliente().getId()).get();
        TipoContato tipoContato = tipoService.buscarTipoContato(obj.getTipoContato().getId()).get();
        obj.setTipoContato(tipoContato);
        obj.setCliente(cliente);
        return repository.save(obj);
    }

    public Optional<Contato> buscarContato(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contato editarDados(long id, Contato obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Contato> allContatos(){
        return (List<Contato>) repository.findAll();
    }

    public Contato buscarPorValor(String valor) { return repository.findByValor(valor); }

    public Contato buscarPorCliente(Cliente cliente) {
        return repository.findByCliente(cliente);
    }

    public Contato buscarPorNomeTipo(String nomeTipo) {
        TipoContato tipoContato = tipoService.buscarNome(nomeTipo);
        return repository.findByTipoContato(tipoContato);
    }
}
