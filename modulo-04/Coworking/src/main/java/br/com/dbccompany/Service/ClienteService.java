package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository repository;

    @Autowired
    private ContatoService contatoService;

    @Transactional(rollbackFor = Exception.class)
    public Cliente salvar(Cliente obj) throws Exception{
        boolean email = false;
        boolean telefone = false;
        for(Contato contato : obj.getContatos()){
            if(contato.getTipoContato().getNome().equalsIgnoreCase("email")){ email = true; }
            if(contato.getTipoContato().getNome().equalsIgnoreCase("telefone")){ telefone = true; }
        }
        if(telefone && email){return repository.save(obj);}
        else{throw new Exception("É necessário ter 1 contato do tipo email e 1 contato do tipo telefone");}
    }

    public Optional<Cliente> buscarCliente(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Cliente editarDados(long id, Cliente obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Cliente> allClientes(){
        return (List<Cliente>) repository.findAll();
    }

    public Cliente buscarPorNome(String nome) {
        return repository.findByNome(nome);
    }

    public Cliente buscarPorCpf(String cpf) {
        return repository.findByCpf(cpf);
    }

}
