package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.PacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacoteService {
    @Autowired
    private PacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Pacote salvar(Pacote obj) {
        String[] array = obj.getValorString().split(" ");
        Double valor = Double.parseDouble(array[1]);
        obj.setValor(valor);
        return repository.save(obj);
    }

    public Optional<Pacote> buscarPacote(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pacote editarDados(long id, Pacote obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Pacote> allPacotes(){
        return (List<Pacote>) repository.findAll();
    }

    public List<Pacote> buscarValor(double valor) {
        return repository.findByValor(valor);
    }
}
