package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuario> lstTipoContatos(){
        return service.allUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Usuario novoUsuario(@RequestBody Usuario b) {
        try {
            return service.salvar(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Usuario editarUsuario(@PathVariable long id, @RequestBody Usuario b) {
        return service.editarDados(id, b);
    }

    @GetMapping(value = "/buscarLogin/{login}")
    @ResponseBody
    public Usuario buscarPorLogin(String login){
        return service.buscarLogin(login);
    }

    @GetMapping(value = "/buscarEmail/{email}")
    @ResponseBody
    public Usuario buscarPorEmail(String email){
        return service.buscarLogin(email);
    }
}
