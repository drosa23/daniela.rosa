package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacoPacote")
public class EspacoPacoteController {
    @Autowired
    EspacoPacoteService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<EspacoPacote> lstContratacoes(){
        return service.allEspacosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EspacoPacote novoEspacoPacote(@RequestBody EspacoPacote b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public EspacoPacote editarEspacoPacote(@PathVariable long id, @RequestBody EspacoPacote b) {
        return service.editarDados(id, b);
    }
}
