package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Acessos;
import br.com.dbccompany.Entity.SaldoCliente;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface AcessosRepository extends CrudRepository<Acessos, Long> {
    Acessos findByData(LocalDateTime data);
    List<Acessos> findBySaldoCliente(SaldoCliente saldoCliente);
}
