package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {
    @Autowired
    EspacoService service;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espaco> lstContratacoes(){
        return service.allEspacos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espaco novoEspaco(@RequestBody Espaco b) {
        return service.salvar(b);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Espaco editarEspaco(@PathVariable long id, @RequestBody Espaco b) {
        return service.editarDados(id, b);
    }
}
