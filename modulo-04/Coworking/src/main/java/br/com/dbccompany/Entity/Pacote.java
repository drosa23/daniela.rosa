package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Pacote.class)
public class Pacote {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "pacote_seq", sequenceName = "pacote_seq")
    @GeneratedValue(generator = "pacote_seq", strategy = GenerationType.SEQUENCE)

    private long id;

    private double valor;

    @Transient
    private String valorString;

    @OneToMany(mappedBy = "pacote")
    private List<EspacoPacote> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote")
    private List<ClientePacote> clientesPacotes = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getValorString() {
        return valorString;
    }

    public void setValorString(String valorString) {
        this.valorString = valorString;
    }

    public List<EspacoPacote> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacote> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientePacote> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientePacote> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
