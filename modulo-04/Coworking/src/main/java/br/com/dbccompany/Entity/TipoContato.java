package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tipo_contato")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = TipoContato.class)
public class TipoContato {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "tipo_contato_seq", sequenceName = "tipo_contato_seq")
    @GeneratedValue(generator = "tipo_contato_seq", strategy = GenerationType.SEQUENCE)

    private long id;

    private String nome;

    @OneToMany(mappedBy = "tipoContato")
    private List<Contato> contatos= new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }
}
