package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table(name = "ESPACOS_PACOTES")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EspacoPacote.class)
public class EspacoPacote {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "espaco_pacote_seq", sequenceName = "espaco_pacote_seq")
    @GeneratedValue(generator = "espaco_pacote_seq", strategy = GenerationType.SEQUENCE)

    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pacotes")
    private Pacote pacote;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_espacos", nullable = false)
    private Espaco espaco;

    @Column(name = "tipo_contratacao")
    private TipoContratacao tipoContratacao;

    private long quantidade;

    private long prazo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote pacote) {
        this.pacote = pacote;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public long getPrazo() {
        return prazo;
    }

    public void setPrazo(long prazo) {
        this.prazo = prazo;
    }
}
