package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Entity.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClientePacoteRepository extends CrudRepository<ClientePacote, Long> {
    List<ClientePacote> findByCliente(Cliente cliente);
    List<ClientePacote> findByPacote(Pacote pacote);
    ClientePacote findByPagamentos(Pagamento pagamento);
}
