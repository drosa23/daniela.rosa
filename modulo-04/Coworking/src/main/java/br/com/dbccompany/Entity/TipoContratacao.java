package br.com.dbccompany.Entity;

public enum TipoContratacao {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}
