package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Entity.EspacoPacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoPacoteService {
    @Autowired
    private EspacoPacoteRepository repository;

    @Autowired
    private EspacoService espacoService;

    @Autowired
    private PacoteService pacoteService;

    @Transactional(rollbackFor = Exception.class)
    public EspacoPacote salvar(EspacoPacote obj) {
        Espaco espaco = espacoService.buscarEspaco(obj.getEspaco().getId()).get();
        Pacote pacote = pacoteService.buscarPacote(obj.getPacote().getId()).get();

        obj.setEspaco(espaco);
        obj.setPacote(pacote);
        return repository.save(obj);
    }

    public Optional<EspacoPacote> buscarEspacoPacote(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public EspacoPacote editarDados(long id, EspacoPacote obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<EspacoPacote> allEspacosPacotes(){
        return (List<EspacoPacote>) repository.findAll();
    }
}
