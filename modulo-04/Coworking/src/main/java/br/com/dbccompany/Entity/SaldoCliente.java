package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SALDO_CLIENTE")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = SaldoCliente.class)
public class SaldoCliente {
    @EmbeddedId
    private SaldoClienteId id;

    @Column(name = "tipo_contratacao", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private long quantidade;

    @Column(nullable = false)
    private LocalDateTime vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<Acessos> acessos = new ArrayList<>();

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void adicionarAcessos(Acessos acessos) {
        this.acessos.add(acessos);
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(long quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDateTime vencimento) {
        this.vencimento = vencimento;
    }
}
