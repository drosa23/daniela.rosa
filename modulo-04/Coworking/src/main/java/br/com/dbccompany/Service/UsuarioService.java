package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.UsuarioRepository;
import br.com.dbccompany.Security.MD5Crypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Usuario salvar(Usuario obj) throws Exception{
        if(obj.getSenha().length() < 6){
            throw new Exception("Senha Inválida");
        }
        String senhaDescrip= obj.getSenha();
        obj.setSenha(new MD5Crypt().encode(senhaDescrip));
        return repository.save(obj);
    }

    public Optional<Usuario> buscarUsuario(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario editarDados(long id, Usuario obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Usuario> allUsuarios(){
        return (List<Usuario>) repository.findAll();
    }

    public Usuario buscarNome(String nome) {
        return repository.findByNome(nome);
    }

    public Usuario buscarEmail(String email) {
        return repository.findByEmail(email);
    }

    public Usuario buscarLogin(String login) {
        return repository.findByLogin(login);
    }
}
