package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Espaco;
import br.com.dbccompany.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoService {
    @Autowired
    private EspacoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Espaco salvar(Espaco obj) {
        if(obj.getValorString() == null){return repository.save(obj);}
        String[] array = obj.getValorString().split(" ");
        Double valor = Double.parseDouble(array[1]);
        obj.setValor(valor);
        return repository.save(obj);
    }

    public Optional<Espaco> buscarEspaco(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Espaco editarDados(long id, Espaco obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Espaco> allEspacos(){
        return (List<Espaco>) repository.findAll();
    }

    public Espaco buscarNome(String nome) {
        return repository.findByNome(nome);
    }
}
