package br.com.dbccompany.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ESPACOS")
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = Espaco.class)
public class Espaco {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "espacos_seq", sequenceName = "espacos_seq")
    @GeneratedValue(generator = "espacos_seq", strategy = GenerationType.SEQUENCE)

    private long id;

    @Column(unique = true, nullable = false)
    private String nome;

    @Column(nullable = false)
    private long qtdPessoas;

    @Column
    private double valor;

    @Transient
    private String valorString;

    @OneToMany(mappedBy = "espaco")
    private List<EspacoPacote> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco")
    private List<Contratacao> contratacoes = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(long qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getValorString() {
        return valorString;
    }

    public void setValorString(String valorString) {
        this.valorString = valorString;
    }

    public List<EspacoPacote> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacoPacote> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
