package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.ClientePacote;
import br.com.dbccompany.Entity.Pacote;
import br.com.dbccompany.Entity.Pagamento;
import br.com.dbccompany.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientePacoteService {
    @Autowired
    private ClientePacoteRepository repository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private PacoteService pacoteService;

    @Transactional(rollbackFor = Exception.class)
    public ClientePacote salvar(ClientePacote obj) {
        Cliente cliente = clienteService.buscarCliente(obj.getCliente().getId()).get();
        Pacote pacote = pacoteService.buscarPacote(obj.getPacote().getId()).get();
        obj.setCliente(cliente);
        obj.setPacote(pacote);
        return repository.save(obj);
    }

    public Optional<ClientePacote> buscarClientePacote(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public ClientePacote editarDados(long id, ClientePacote obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<ClientePacote> allClientesPacotes(){
        return (List<ClientePacote>) repository.findAll();
    }

    public List<ClientePacote> buscarPorCliente(Cliente cliente) {
        return repository.findByCliente(cliente);
    }

    public List<ClientePacote> buscarPorPacote(Pacote pacote) {
        return repository.findByPacote(pacote);
    }

    public ClientePacote buscarPorPagamento(Pagamento pagamento) { return repository.findByPagamentos(pagamento); }
}
