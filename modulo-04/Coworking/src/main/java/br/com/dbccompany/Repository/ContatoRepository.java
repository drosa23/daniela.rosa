package br.com.dbccompany.Repository;

import br.com.dbccompany.Entity.Cliente;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<Contato, Long> {
    Contato findByValor(String valor);
    Contato findByCliente(Cliente cliente);
    Contato findByTipoContato(TipoContato tipoContato);
}
