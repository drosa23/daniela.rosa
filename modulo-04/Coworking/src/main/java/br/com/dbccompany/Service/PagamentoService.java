package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.*;
import br.com.dbccompany.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {
    @Autowired
    private PagamentoRepository repository;
    @Autowired
    private ClientePacoteService cliPacService;
    @Autowired
    private PacoteService pacoteService;
    @Autowired
    private EspacoService espacoService;
    @Autowired
    private SaldoClienteService saldoClienteService;
    @Autowired
    private ContratacaoService contratacaoService;

    @Transactional(rollbackFor = Exception.class)
    public Pagamento salvar(Pagamento obj) {

        if(obj.getClientePacote() != null){
            Optional<ClientePacote> cliPac = cliPacService.buscarClientePacote(obj.getClientePacote().getId());
            obj.setClientePacote(cliPac.get());
            Optional<Pacote> pacote = pacoteService.buscarPacote(cliPac.get().getPacote().getId());
            List<EspacoPacote> espPacotes = pacote.get().getEspacosPacotes();

            for(EspacoPacote espPac : espPacotes){

                SaldoClienteId saldoId = new SaldoClienteId();
                SaldoCliente saldo = new SaldoCliente();
                Espaco espaco = espacoService.buscarEspaco(espPac.getEspaco().getId()).get();
                espacoService.salvar(espaco);
                saldoId.setCliente(cliPac.get().getCliente());
                saldoId.setEspaco(espaco);
                saldo.setQuantidade(cliPac.get().getQuantidade());
                saldo.setTipoContratacao(espPac.getTipoContratacao());
                saldo.setId(saldoId);
                saldo.setVencimento(saldoClienteService.calcularVencimento(espPac.getPrazo()));
                saldoClienteService.salvar(saldo);
            }
        }
        else if(obj.getContratacao() != null){
            SaldoClienteId saldoId = new SaldoClienteId();
            SaldoCliente saldo = new SaldoCliente();
            Contratacao contratacao = contratacaoService.buscarContratacao(obj.getContratacao().getId()).get();
            obj.setContratacao(contratacao);
            saldoId.setEspaco(contratacao.getEspaco());
            saldoId.setCliente(contratacao.getCliente());
            saldo.setId(saldoId);
            saldo.setQuantidade(contratacao.getQuantidade());
            saldo.setTipoContratacao(contratacao.getTipoContratacao());
            saldo.setVencimento(saldoClienteService.calcularVencimento(contratacao.getPrazo()));
            saldoClienteService.salvar(saldo);
        }

        return repository.save(obj);
    }

    public Optional<Pagamento> buscarPagamento(long id) {
        return repository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pagamento editarDados(long id, Pagamento obj) {
        obj.setId(id);
        return repository.save(obj);
    }

    public List<Pagamento> allPagamentos(){
        return (List<Pagamento>) repository.findAll();
    }
}
