/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE2")
public class Cliente2 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cliente2_seq", sequenceName = "cliente2_seq")
    @GeneratedValue(generator = "cliente2_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(mappedBy = "clientes2")
    private List<Loja> lojas;
}