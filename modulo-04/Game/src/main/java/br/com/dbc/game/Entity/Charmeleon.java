package br.com.dbc.game.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CHARMELEON")
public class Charmeleon {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CHARMELEON_SEQ", sequenceName = "CHARMELEON_SEQ")
    @GeneratedValue(generator = "CHARMELEON_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    
    @OneToMany(mappedBy = "charmeleon", cascade = CascadeType.ALL)
    private List<AtaqueEspecial> ataquesEspeciais = new ArrayList<>();
}