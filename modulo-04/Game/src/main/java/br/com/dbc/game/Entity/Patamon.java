package br.com.dbc.game.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;

import javax.persistence.Table;

@Entity
@Table(name = "PATAMON")
@PrimaryKeyJoinColumn(name = "id_digimon")
public class Patamon extends Digimon {
    @Column(name = "forca_luz")
    private Integer forcaLuz;
    
    
}