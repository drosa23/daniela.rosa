/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SCORE_SERASA")
public class ScoreSerasa {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "SCORE_SERASA_SEQ", sequenceName = "SCORE_SERASA_SEQ")
    @GeneratedValue(generator = "SCORE_SERASA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @OneToOne(mappedBy = "scoreSerasa")
    private Pessoa pessoa;
}