

package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTATO")
public class Pokemon {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "POKEMON_SEQ", sequenceName = "POKEMON_SEQ")
    @GeneratedValue(generator = "POKEMON_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToOne(mappedBy = "pokemon")
    @JoinColumn(name = "id_charmander")
    private Charmander charmander;
}