
package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTA3")
public class Conta3 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA3_SEQ", sequenceName = "CONTA3_SEQ")
    @GeneratedValue(generator = "CONTA3_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "id_agencia")
    private Agencia agencia;
}