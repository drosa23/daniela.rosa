/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.PrimaryKeyJoinColumn;

import javax.persistence.Table;

@Entity
@Table(name = "AGUMON")
@PrimaryKeyJoinColumn(name = "id_digimon")
public class Agumon extends Digimon {
    @Column(name = "forca_fogo")
    private Integer forcaFogo;
    
}