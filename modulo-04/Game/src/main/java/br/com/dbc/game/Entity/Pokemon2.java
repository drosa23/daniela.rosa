
package br.com.dbc.game.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "POKEMON2")
public class Pokemon2 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "pokemon2_SEQ", sequenceName = "pokemon2_SEQ")
    @GeneratedValue(generator = "pokemon2_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(mappedBy = "pokemons2")
    private List<Treinador> treinadores = new ArrayList<>();
}