/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CARTAO")
public class Cartao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CARTAO_SEQ", sequenceName = "CARTAO_SEQ")
    @GeneratedValue(generator = "CARTAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(mappedBy = "cartoes")
    private List<Cliente> clientes;
}