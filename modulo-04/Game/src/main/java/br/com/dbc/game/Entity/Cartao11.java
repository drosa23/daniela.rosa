/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CARTAO_TABELAO")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn (name = "tipo_cartao", discriminatorType = DiscriminatorType.STRING)
public class Cartao11 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_TAB_SEQ", sequenceName = "PERSONAGEM_TAB_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_TAB_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer Id;
    
    @Column(name = "dia_vencimento")
    private Integer diaVencimento;
}