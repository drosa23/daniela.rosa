/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTA")
public class Conta {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToOne(mappedBy = "conta")
    @JoinColumn(name = "id_cheque_especial")
    private ChequeEspecial chequeEspecial;
}