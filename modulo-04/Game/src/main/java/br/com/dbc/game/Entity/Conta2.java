
package br.com.dbc.game.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTA2")
public class Conta2 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA2_SEQ", sequenceName = "CONTA2_SEQ")
    @GeneratedValue(generator = "CONTA2_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CONTA_PRODUTO",
            joinColumns = {
                @JoinColumn(name = "id_conta")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_produto")})
    private List<Produto> produtos = new ArrayList<>();
}