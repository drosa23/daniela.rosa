
package br.com.dbc.game.Entity;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "PIKACHU")
public class Pikachu {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "pikachu_seq", sequenceName = "pikachu_seq")
    @GeneratedValue(generator = "pikachu_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToMany(mappedBy = "pikachu", cascade = CascadeType.ALL)
    private List<Evolucao> evolucoes = new ArrayList<>();
}