/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "AGENCIA")
public class Agencia {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
    @GeneratedValue(generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToMany(mappedBy = "agencia", cascade = CascadeType.ALL)
    private List<Conta> contas = new ArrayList<>();
}