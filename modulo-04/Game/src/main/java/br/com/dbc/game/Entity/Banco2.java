/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BANCO")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Banco2 {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "BANCO2_SEQ", sequenceName = "BANCO2_SEQ")
    @GeneratedValue(generator = "BANCO2_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer Id;
}
