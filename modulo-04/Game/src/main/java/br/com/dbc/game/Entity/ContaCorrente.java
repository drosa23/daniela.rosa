/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "conta_corrente")
public class ContaCorrente {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "conta_corrente_SEQ", sequenceName = "conta_corrente_SEQ")
    @GeneratedValue(generator = "conta_corrente_SEQ", strategy = GenerationType.SEQUENCE)
    
    private Integer id;
   
    @ManyToOne
    @JoinColumn(name = "id_conta")
    private Conta10 conta;
}