/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.SequenceGenerator;


@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Digimon {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "DIGIMON_SEQ", sequenceName = "DIGIMON_SEQ")
    @GeneratedValue(generator = "DIGIMON_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_digimon")
    private Integer Id;
    private String nome;
}