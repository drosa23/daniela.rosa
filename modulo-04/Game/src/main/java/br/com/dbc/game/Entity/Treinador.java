/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "TREINADOR")
public class Treinador {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "treinador_SEQ", sequenceName = "treinador_SEQ")
    @GeneratedValue(generator = "treinador_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "treinador_pokemon",
            joinColumns = {
                @JoinColumn(name = "id_treinador")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_pokemon")})
    private List<Pokemon2> pokemons2 = new ArrayList<>();

}