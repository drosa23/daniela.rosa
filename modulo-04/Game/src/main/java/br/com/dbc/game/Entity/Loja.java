/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOJA")
public class Loja {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "loja_seq", sequenceName = "loja_seq")
    @GeneratedValue(generator = "loja_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "cliente_loja",
            joinColumns = {
                @JoinColumn(name = "id_loja")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_cliente")})
    private List<Cliente2> clientes2 = new ArrayList<>();
}