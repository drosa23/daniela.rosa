/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CONTA")
public class Conta10 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTA10_SEQ", sequenceName = "CONTA10_SEQ")
    @GeneratedValue(generator = "CONTA10_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToMany(mappedBy = "conta", cascade = CascadeType.ALL)
    private List<ContaCorrente> contaCorrente = new ArrayList<>();
    private List<ContaSalario> contaSalario = new ArrayList<>();
    private List<ContaPoupanca> contaPoupanca = new ArrayList<>();
}