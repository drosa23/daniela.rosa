package br.com.dbc.game.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CHARMANDER")
public class Charmander {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CHARMANDER_SEQ", sequenceName = "CHARMANDER_SEQ")
    @GeneratedValue(generator = "CHARMANDER_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pokemon")
    private Pokemon pokemon;
}