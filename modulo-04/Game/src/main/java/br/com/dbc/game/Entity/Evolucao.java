

package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EVOLUCAO")
public class Evolucao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "evolucao_seq", sequenceName = "evolucao_seq")
    @GeneratedValue(generator = "evolucao_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "id_pikachu")
    private Pikachu pikachu;
}