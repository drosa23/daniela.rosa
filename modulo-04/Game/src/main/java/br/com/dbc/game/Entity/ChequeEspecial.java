/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CHEQUE_ESPECIAL")
public class ChequeEspecial {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CHEQUE_ESPECIAL_SEQ", sequenceName = "CHEQUE_ESPECIAL_SEQ")
    @GeneratedValue(generator = "CHEQUE_ESPECIAL_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_conta")
    private Conta conta;
}