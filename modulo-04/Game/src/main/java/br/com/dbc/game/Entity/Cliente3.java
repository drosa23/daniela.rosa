/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.dbc.game.Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Cliente3")
public class Cliente3 {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cliente3_seq", sequenceName = "cliente3_seq")
    @GeneratedValue(generator = "cliente3_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    
    @OneToOne(mappedBy = "cliente3")
    @JoinColumn(name = "id_crediario")
    private Crediario crediario;
}