/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniela.amaral
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Connection conn = Connector.connect();
        try {
                conn.prepareStatement("INSERT INTO banco\n"
                        + "VALUES (0, \"banco1\");\n"
                        + "\n"
                        + "INSERT INTO banco\n"
                        + "VALUES (1, \"banco2\");\n"
                        + "\n"
                        + "INSERT INTO conta\n"
                        + "VALUES (0, 100, 0);\n"
                        + "\n"
                        + "INSERT INTO conta\n"
                        + "VALUES (1, 100, 0);\n"
                        + "\n"
                        + "INSERT INTO conta\n"
                        + "VALUES (2, 100, 1);\n"
                        + "\n"
                        + "INSERT INTO conta\n"
                        + "VALUES (3, 100, 1);\n"
                        + "\n"
                        + "INSERT INTO transferencia\n"
                        + "VALUES (0, 100, 1, 0);\n"
                        + "\n"
                        + "INSERT INTO transferencia\n"
                        + "VALUES (1, 100, 2, 0);\n"
                        + "\n"
                        + "INSERT INTO transferencia\n"
                        + "VALUES (2, 100, 3, 0);").executeUpdate();
                System.out.println("Inserts feitos");
                ResultSet rs = conn.prepareStatement("SELECT id\n"
                        + "FROM conta \n"
                        + "WHERE saldo = 0;").executeQuery();
                while(rs.next()){
                    System.out.println(String.format("Id do usuario: %s", rs.getInt("id")));
                

            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
