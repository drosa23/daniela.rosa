package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EstadoIntegrationTests {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Test
	public void achaEstadoPorNome() {
		Estado estado = new Estado();
		estado.setNome("Pará");
		entityManager.persist(estado);
		entityManager.flush();
		
		Estado found = estadoRepository.findByNome(estado.getNome());
		
		assertThat(found.getNome()).isEqualTo(estado.getNome());
	}
}
