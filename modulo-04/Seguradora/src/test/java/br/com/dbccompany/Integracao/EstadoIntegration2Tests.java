package br.com.dbccompany.Integracao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;

@RunWith(SpringRunner.class)
public class EstadoIntegration2Tests {
	
	@MockBean
	private EstadoRepository estadoRepository;
	
	@Autowired
	ApplicationContext context;
	
	@Before
	public void setUp() {
		Estado estado = new Estado();
		estado.setNome("Pará");
		
		Mockito.when(estadoRepository.findByNome(estado.getNome())).thenReturn(estado);
	}
	
	@Test
	public void acharEstadoPorNome2() {
		String nome = "Pará";
		Estado found = estadoRepository.findByNome(nome);
		assertThat(found.getNome()).isEqualTo(nome);
	}
	
}
