package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoa;

public interface PessoaRepository<E extends Pessoa> extends CrudRepository<E, Long>{
	E findByNome(String nome);
	E findByCpf(long cpf);
	List<E> findByPai(String pai);
	List<E> findByMae(String mae);
	E findByTelefone(long telefone);
	E findByEmail(String email);
	List<E> findByEnderecos(Endereco endereco);
} 
