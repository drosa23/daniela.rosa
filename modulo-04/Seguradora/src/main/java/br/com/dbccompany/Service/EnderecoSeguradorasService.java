package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Repository.EnderecoSeguradorasRepository;

@Service
public class EnderecoSeguradorasService {
    @Autowired
    private EnderecoSeguradorasRepository a;

    @Transactional(rollbackFor = Exception.class)
    public EnderecoSeguradoras salvar(EnderecoSeguradoras e) {
        return a.save(e);
    }

    public Optional<EnderecoSeguradoras> buscarEndereco(long id) {
        return a.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public EnderecoSeguradoras editarDados(long id, EnderecoSeguradoras e) {
        e.setId(id);
        return salvar(e);
    }

    public List<EnderecoSeguradoras> allEnderecosSeguradoras(){
        return (List<EnderecoSeguradoras>) a.findAll();
    }

    public EnderecoSeguradoras buscarPorSeguradora(Seguradora l){
        return a.findBySeguradora(l);
    }


}
