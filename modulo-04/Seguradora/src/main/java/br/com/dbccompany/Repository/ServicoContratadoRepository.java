package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;

public interface ServicoContratadoRepository extends CrudRepository<ServicoContratado, Long>{
	ServicoContratado findByDescricao(String descricao);
	List<ServicoContratado> findByValor(double valor);
	List<ServicoContratado> findByServico(Servico servico);
	List<ServicoContratado> findBySeguradora(Seguradora seguradora);
	List<ServicoContratado> findBySegurado (Segurado segurado);
	List<ServicoContratado> findByCorretor(Corretor corretor);
}
