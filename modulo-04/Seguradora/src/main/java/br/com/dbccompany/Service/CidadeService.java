package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.CidadeRepository;

@Service
public class CidadeService {
    @Autowired
    private CidadeRepository cidadeRepository;

    @Transactional(rollbackFor = Exception.class)
    public Cidade salvar(Cidade cidade) {
        return cidadeRepository.save(cidade);
    }

    public Optional<Cidade> buscarCidade(long id) {
        return cidadeRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Cidade editarDados(long id, Cidade cidade) {
        cidade.setId(id);
        return salvar(cidade);
    }

    public List<Cidade> allCidades(){
        return (List<Cidade>) cidadeRepository.findAll();
    }

    public Cidade buscarPorNome(String nome){
        return cidadeRepository.findByNome(nome);
    }

    public List<Cidade> buscarPorEstado(Estado estado){
        return cidadeRepository.findByEstado(estado);
    }

    public Cidade buscarPorBairro(Bairro bairro){
        return cidadeRepository.findByBairros(bairro);
    }
}
