package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.CorretorRepository;

@Service
public class CorretorService {
    @Autowired
    private CorretorRepository corretorRepository;

    @Transactional(rollbackFor = Exception.class)
    public Corretor salvar(Corretor corretor) {
        return corretorRepository.save(corretor);
    }

    public Optional<Corretor> buscarCorretor(long id) {
        return corretorRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Corretor editarDados(long id, Corretor cidade) {
        cidade.setId(id);
        return salvar(cidade);
    }

    public List<Corretor> allCorretores(){
        return (List<Corretor>) corretorRepository.findAll();
    }

    public List<Corretor> buscarPorCargo(String cargo){
        return corretorRepository.findByCargo(cargo);
    }

    public List<Corretor> buscarPorComissao(double comissao){
        return corretorRepository.findByComissao(comissao);
    }

    public Corretor buscarPorServicoContratado(ServicoContratado s){
        return corretorRepository.findByServicosContratados(s);
    }
}
