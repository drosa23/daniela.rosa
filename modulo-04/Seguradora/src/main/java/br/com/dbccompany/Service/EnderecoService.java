package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoa;
import br.com.dbccompany.Repository.EnderecoRepository;

@Service
public class EnderecoService {
    @Autowired
    private EnderecoRepository enderecoRepository;

    @Transactional(rollbackFor = Exception.class)
    public Endereco salvar(Endereco e) {
        return (Endereco) enderecoRepository.save(e);
    }

    public Optional<Endereco> buscarEndereco(long id) {
        return enderecoRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Endereco editarDados(long id, Endereco e) {
        e.setId(id);
        return salvar(e);
    }

    public List<Endereco> allEnderecos(){
        return (List<Endereco>) enderecoRepository.findAll();
    }

    public List<Endereco> buscarPorLogradouro(String l){
        return enderecoRepository.findByLogradouro(l);
    }

    public List<Endereco> buscarPorNumero(long n){
        return enderecoRepository.findByNumero(n);
    }

    public List<Endereco> buscarPorComplemento(String s){
        return enderecoRepository.findByComplemento(s);
    }

    public List<Endereco> buscarPorBairro(Bairro s){
        return enderecoRepository.findByBairro(s);
    }

    public Endereco buscarPorPessoa(Pessoa s){
        return enderecoRepository.findByPessoas(s);
    }
}
