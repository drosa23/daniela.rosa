package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
public class Seguradora {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "seguradora_seq", sequenceName = "seguradora_seq")
	@GeneratedValue(generator = "seguradora_seq", strategy = GenerationType.SEQUENCE)
	private long id;
	private String nome;
	
	@Column(unique = true)
	private long cnpj;
	
	@OneToOne(mappedBy = "seguradora")
    @JoinColumn(name = "id_enderecos_endereco_seguradoras")
    private EnderecoSeguradoras enderecoSeguradora;
	
	@ManyToMany
    @JoinTable(name = "seguradoras_servicoes",
            joinColumns = {
                @JoinColumn(name = "id_seguradora")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_servico")})
    private List<Servico> servicos= new ArrayList<>();
	
	@OneToMany(mappedBy = "seguradora", cascade = CascadeType.ALL)
    private List<ServicoContratado> servicoContratado = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}

	public EnderecoSeguradoras getEnderecoSeguradora() {
		return enderecoSeguradora;
	}

	public void setEnderecoSeguradora(EnderecoSeguradoras enderecoSeguradora) {
		this.enderecoSeguradora = enderecoSeguradora;
	}

	public List<Servico> getServicos() {
		return servicos;
	}

	public void setServicos(List<Servico> servicos) {
		this.servicos = servicos;
	}

	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	
	
}
