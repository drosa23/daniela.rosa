package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Entity.ServicoContratado;

public interface SeguradoRepository extends PessoaRepository<Segurado>{
	List<Segurado> findByQtdServicos(long qtd);
	Segurado findByServicosContratados(ServicoContratado ser);
}
