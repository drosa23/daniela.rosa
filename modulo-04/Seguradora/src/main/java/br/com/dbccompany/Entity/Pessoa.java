package br.com.dbccompany.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "PESSOAS")
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoa {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "pessoa_seq", sequenceName = "pessoa_seq")
	@GeneratedValue(generator = "pessoa_seq", strategy = GenerationType.SEQUENCE)
	private long id;
	private String nome;
	
	@Column(unique = true)
	private long cpf;
	private String pai;
	private String mae;
	private long telefone;
	
	@Column(unique = true)
	private String email;
	
	@ManyToMany
    @JoinTable(name = "endereco_pessoas",
            joinColumns = {
                @JoinColumn(name = "id_pessoas")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_enderecos")})
    private List<Endereco> enderecos= new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public String getPai() {
		return pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getMae() {
		return mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public long getTelefone() {
		return telefone;
	}

	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}
	
}
