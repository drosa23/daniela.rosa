package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name= "ESTADO")
public class Estado {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "estado_seq", sequenceName = "estado_seq")
	@GeneratedValue(generator = "estado_seq", strategy = GenerationType.SEQUENCE)
	private long id;
	
	private String nome;
	
	@OneToMany(mappedBy = "estado")
    private List<Cidade> cidades = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
	
	
}
