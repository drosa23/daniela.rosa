package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
@Table(name = "SERVICO_CONTRATADO")
public class ServicoContratado {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "servicoContrado_seq", sequenceName = "servicoContrado_seq")
	@GeneratedValue(generator = "servicoContrado_seq", strategy = GenerationType.SEQUENCE)
	private long id;
	
	private String descricao;
	private double valor;
	
	@ManyToOne
    @JoinColumn(name = "id_servicos")
    private Servico servico;
	
	@ManyToOne
    @JoinColumn(name = "id_seguradora")
    private Seguradora seguradora;
	
	@ManyToOne
    @JoinColumn(name = "id_pessoas_segurados")
    private Segurado segurado;
	
	@ManyToOne
    @JoinColumn(name = "id_pessoas_corretor")
    private Corretor corretor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Servico getServico() {
		return servico;
	}

	public void setServico(Servico servico) {
		this.servico = servico;
	}

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}

	public Segurado getSegurado() {
		return segurado;
	}

	public void setSegurado(Segurado segurado) {
		this.segurado = segurado;
	}

	public Corretor getCorretor() {
		return corretor;
	}

	public void setCorretor(Corretor corretor) {
		this.corretor = corretor;
	}
	
}
