package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Endereco;

public interface BairroRepository extends CrudRepository<Bairro, Long>{
	Bairro findByNome(String nome);
	Bairro findByCidade(Cidade cidade);
	Bairro findByEnderecos(Endereco endereco);
}
