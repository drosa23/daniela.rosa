package br.com.dbccompany.Service;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Repository.BairroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BairroService {
    @Autowired
    private BairroRepository bairroRepository;

    @Transactional(rollbackFor = Exception.class)
    public Bairro salvar(Bairro bairro) {
        return bairroRepository.save(bairro);
    }

    public Optional<Bairro> buscarBairro(long id) {
        return bairroRepository.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Bairro editarDados(long id, Bairro agencia) {
        agencia.setId(id);
        return bairroRepository.save(agencia);
    }

    public List<Bairro> allBairros(){
        return (List<Bairro>) bairroRepository.findAll();
    }

    public Bairro buscarPorNome(String nome){
        return bairroRepository.findByNome(nome);
    }

    public Bairro buscarPorCidade(Cidade cidade){
        return bairroRepository.findByCidade(cidade);
    }

    public Bairro buscarPorEndereco(Endereco endereco){
        return bairroRepository.findByEnderecos(endereco);
    }
}
