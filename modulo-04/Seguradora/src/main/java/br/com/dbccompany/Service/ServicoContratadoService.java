package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.CorretorRepository;
import br.com.dbccompany.Repository.SeguradoRepository;
import br.com.dbccompany.Repository.ServicoContratadoRepository;

@Service
public class ServicoContratadoService {
    @Autowired
    private ServicoContratadoRepository a;
    
    @Autowired
    private CorretorRepository c;
    
    @Autowired
    private SeguradoRepository seg;

    @Transactional(rollbackFor = Exception.class)
    public ServicoContratado salvar(ServicoContratado serCon) {
    	Corretor corretor = c.findById(serCon.getCorretor().getId()).get();
    	Segurado segurado = seg.findById(serCon.getSegurado().getId()).get();
    	segurado.setQtdServicos(segurado.getQtdServicos() + 1);
    	corretor.setComissao(corretor.getComissao()+ (serCon.getValor() * 0.1));
    	serCon.setCorretor(corretor);
    	serCon.setSegurado(segurado);
        return a.save(serCon);
    }

    public Optional<ServicoContratado> buscarServicoContratado(long id) {
        return a.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public ServicoContratado editarDados(long id, ServicoContratado s) {
        s.setId(id);
        return salvar(s);
    }

    public List<ServicoContratado> allServicosContratados(){
        return (List<ServicoContratado>) a.findAll();
    }

    public ServicoContratado buscarPorDescricao(String s){
        return a.findByDescricao(s);
    }

    public List<ServicoContratado> buscarPorValor(long q){
        return a.findByValor(q);
    }

    public List<ServicoContratado> buscarPorServico(Servico q){
        return a.findByServico(q);
    }

    public List<ServicoContratado> buscarPorSeguradora(Seguradora q){
        return a.findBySeguradora(q);
    }

    public List<ServicoContratado> buscarPorSegurado(Segurado q){
        return a.findBySegurado(q);
    }

    public List<ServicoContratado> buscarPorCorretor(Corretor q){
        return a.findByCorretor(q);
    }


}
