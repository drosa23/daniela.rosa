package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Service.EnderecoSeguradorasService;


@Controller
@RequestMapping("/api/enderecoSeguradoras")
public class EnderecoSeguradorasController {

    @Autowired
    EnderecoSeguradorasService endSegService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<EnderecoSeguradoras> lstEnderecoSeguradoras(){
        return endSegService.allEnderecosSeguradoras();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public EnderecoSeguradoras novaEndSeg(@RequestBody EnderecoSeguradoras b) {
        return endSegService.salvar(b);
    }


    @PostMapping(value = "/buscarPorSeguradora")
    @ResponseBody
    public EnderecoSeguradoras buscarPorSerguradora(@RequestBody Seguradora e ) { return endSegService.buscarPorSeguradora(e); }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public EnderecoSeguradoras editarEndSeg(@PathVariable long id, @RequestBody EnderecoSeguradoras b) {
        return endSegService.editarDados(id, b);
    }

}
