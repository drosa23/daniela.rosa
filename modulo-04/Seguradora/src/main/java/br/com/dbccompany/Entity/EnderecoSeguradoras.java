package br.com.dbccompany.Entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "ID_ENDERECOS")
public class EnderecoSeguradoras extends Endereco{
	
	@OneToOne
    @JoinColumn(name = "id_seguradora")
    private Seguradora seguradora;

	public Seguradora getSeguradora() {
		return seguradora;
	}

	public void setSeguradora(Seguradora seguradora) {
		this.seguradora = seguradora;
	}
	
	
}
