package br.com.dbccompany.Entity;

import java.util.*;

import javax.persistence.*;

@Entity
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class Corretor extends Pessoa{
	private String cargo;
	private double comissao;
	
	@OneToMany(mappedBy = "corretor")
    private List<ServicoContratado> servicosContratados = new ArrayList<>();

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getComissao() {
		return comissao;
	}

	public void setComissao(double comissao) {
		this.comissao = comissao;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}
	
	
}
