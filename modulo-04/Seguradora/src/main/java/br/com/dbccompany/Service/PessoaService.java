package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoa;
import br.com.dbccompany.Repository.PessoaRepository;

@Service
public class PessoaService {
    @Autowired
    private PessoaRepository<Pessoa> a;

    @Transactional(rollbackFor = Exception.class)
    public Pessoa salvar(Pessoa e) {
        return (Pessoa) a.save(e);
    }

    public Optional<Pessoa> buscarPessoa(long id) {
        return a.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Pessoa editarDados(long id, Pessoa e) {
        e.setId(id);
        return salvar(e);
    }

    public List<Pessoa> allPessoas(){
        return (List<Pessoa>) a.findAll();
    }

    public Pessoa buscarPorNome(String l){
        return a.findByNome(l);
    }

    public Pessoa buscarPorCpf(long l){
        return a.findByCpf(l);
    }

    public List<Pessoa> buscarPorPai(String l){
        return a.findByPai(l);
    }

    public List<Pessoa> buscarPorMae(String l){
        return a.findByMae(l);
    }

    public Pessoa buscarPorTelefone(long l){
        return a.findByTelefone(l);
    }

    public Pessoa buscarPorEmail(String l){
        return a.findByEmail(l);
    }

    public List<Pessoa> buscarPorEndereco(Endereco l){
        return a.findByEnderecos(l);
    }





}
