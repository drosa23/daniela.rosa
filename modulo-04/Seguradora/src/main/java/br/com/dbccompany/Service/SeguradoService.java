package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Segurado;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.SeguradoRepository;

@Service
public class SeguradoService {
    @Autowired
    private SeguradoRepository a;

    @Transactional(rollbackFor = Exception.class)
    public Segurado salvar(Segurado corretor) {
        return a.save(corretor);
    }

    public Optional<Segurado> buscarCorretor(long id) {
        return a.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Segurado editarDados(long id, Segurado cidade) {
        cidade.setId(id);
        return salvar(cidade);
    }

    public List<Segurado> allSegurados(){
        return (List<Segurado>) a.findAll();
    }

    public List<Segurado> buscarPorCargo(long q){
        return a.findByQtdServicos(q);
    }

    public Segurado buscarPorServicoContratado(ServicoContratado s){
        return a.findByServicosContratados(s);
    }
}
