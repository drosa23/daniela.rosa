package br.com.dbccompany.Entity;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "SERVICOS")
public class Servico {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "servicos_seq", sequenceName = "servicos_seq")
	@GeneratedValue(generator = "servicos_seq", strategy = GenerationType.SEQUENCE)
	private long id;
	private String nome;
	private String descricao;
	
	@Column(name = "valor_padrao")
	private double valorPadrao;
	
	@ManyToMany(mappedBy = "servicos")
    private List<Seguradora> seguradoras= new ArrayList<>();
	
	@OneToMany(mappedBy = "servico")
    private List<ServicoContratado> servicoContratado = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getValorPadrao() {
		return valorPadrao;
	}

	public void setValorPadrao(double valorPadrao) {
		this.valorPadrao = valorPadrao;
	}

	public List<Seguradora> getSeguradoras() {
		return seguradoras;
	}

	public void setSeguradoras(List<Seguradora> seguradoras) {
		this.seguradoras = seguradoras;
	}

	public List<ServicoContratado> getServicoContratado() {
		return servicoContratado;
	}

	public void setServicoContratado(List<ServicoContratado> servicoContratado) {
		this.servicoContratado = servicoContratado;
	}
	
	
}
