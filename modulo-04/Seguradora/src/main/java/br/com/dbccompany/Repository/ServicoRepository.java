package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;

public interface ServicoRepository extends CrudRepository<Servico, Long>{
	Servico findByNome(String nome);
	Servico findByDescricao(String descricao);
	List<Servico> findByValorPadrao(double valor);
	List<Servico> findBySeguradoras(Seguradora seguradora);
	Servico findByServicoContratado(ServicoContratado ser);
}
