package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Service.CorretorService;


@Controller
@RequestMapping("/api/corretor")
public class CorretorController {

    @Autowired
    CorretorService corretorService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Corretor> lstCorretores(){
        return corretorService.allCorretores();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Corretor novoCorretor(@RequestBody Corretor b) {
        return corretorService.salvar(b);
    }

    @GetMapping(value = "/{cargo}")
    @ResponseBody
    public List<Corretor> corretorEspecifico(@PathVariable String cargo ) {
        return corretorService.buscarPorCargo(cargo);
    }

    @PostMapping(value = "/buscarPorServicoContratado")
    @ResponseBody
    public Corretor buscarPorSerCont(@RequestBody ServicoContratado e ) { return corretorService.buscarPorServicoContratado(e); }

    @GetMapping(value = "/{comissao}")
    @ResponseBody
    public List<Corretor> corretorEspecifico(@PathVariable double comissao ) {
        return corretorService.buscarPorComissao(comissao);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Corretor editarCorretor(@PathVariable long id, @RequestBody Corretor b) {
        return corretorService.editarDados(id, b);
    }

}
