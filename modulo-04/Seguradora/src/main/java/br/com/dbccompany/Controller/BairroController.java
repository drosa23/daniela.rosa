package br.com.dbccompany.Controller;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Service.BairroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/api/bairro")
public class BairroController {

    @Autowired
    BairroService bairroService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Bairro> lstBairros(){
        return bairroService.allBairros();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Bairro novoBairro(@RequestBody Bairro b) {
        return bairroService.salvar(b);
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public Bairro bairroEspecifico(@PathVariable String nome ) {
        return bairroService.buscarPorNome(nome);
    }

    @PostMapping(value = "/buscarPorCidade")
    @ResponseBody
    public Bairro bairroEspecificoPorCidade(@RequestBody Cidade cidade ) {
        return bairroService.buscarPorCidade(cidade);
    }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Bairro editarBairro(@PathVariable long id, @RequestBody Bairro b) {
        return bairroService.editarDados(id, b);
    }

}
