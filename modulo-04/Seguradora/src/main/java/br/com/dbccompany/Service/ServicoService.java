package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.ServicoRepository;

@Service
public class ServicoService {
    @Autowired
    private ServicoRepository a;

    @Transactional(rollbackFor = Exception.class)
    public Servico salvar(Servico corretor) {
        return a.save(corretor);
    }

    public Optional<Servico> buscarServico(long id) {
        return a.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Servico editarDados(long id, Servico s) {
        s.setId(id);
        return salvar(s);
    }

    public List<Servico> allServicos(){
        return (List<Servico>) a.findAll();
    }

    public Servico buscarPorDescricao(String s){
        return a.findByDescricao(s);
    }

    public Servico buscarPorNome(String s){
        return a.findByNome(s);
    }

    public List<Servico> buscarPorValor(double q){
        return a.findByValorPadrao(q);
    }

    public List<Servico> buscarPorSeguradoras(Seguradora q){
        return a.findBySeguradoras(q);
    }

    public Servico buscarPorServicoContratado(ServicoContratado q){ return a.findByServicoContratado(q); }




}
