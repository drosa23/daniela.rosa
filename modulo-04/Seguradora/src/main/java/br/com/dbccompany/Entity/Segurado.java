package br.com.dbccompany.Entity;

import java.util.*;

import javax.persistence.*;

@Entity
@Table(name = "SEGURADOS")
@PrimaryKeyJoinColumn(name = "ID_PESSOA")
public class Segurado extends Pessoa{
	@Column(name= "qtd_servicos")
	private long qtdServicos;
	
	@OneToMany(mappedBy = "segurado")
    private List<ServicoContratado> servicosContratados = new ArrayList<>();

	public long getQtdServicos() {
		return qtdServicos;
	}

	public void setQtdServicos(long qtdServicos) {
		this.qtdServicos = qtdServicos;
	}

	public List<ServicoContratado> getServicosContratados() {
		return servicosContratados;
	}

	public void setServicosContratados(List<ServicoContratado> servicosContratados) {
		this.servicosContratados = servicosContratados;
	}
	
	
}
