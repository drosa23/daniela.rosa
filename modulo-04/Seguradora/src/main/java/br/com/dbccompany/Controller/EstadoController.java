package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Service.EstadoService;

@Controller
@RequestMapping("/api/estado")
public class EstadoController {
	@Autowired
    EstadoService estadoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Estado> lstEstados(){
        return estadoService.allEstados();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Estado novoEstado(@RequestBody Estado b) {
        return estadoService.salvar(b);
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public Estado estadoEspecifico(@PathVariable String nome ) {
        return estadoService.buscarPorNome(nome);
    }
}
