package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoa;
import br.com.dbccompany.Service.EnderecoService;


@Controller
@RequestMapping("/api/endereco")
public class EnderecoController {

    @Autowired
    EnderecoService enderecoService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Endereco> lstEnderecos(){
        return enderecoService.allEnderecos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Endereco novoEndereco(@RequestBody Endereco b) {
        return enderecoService.salvar(b);
    }

    @GetMapping(value = "/{complemento}")
    @ResponseBody
    public List<Endereco> buscarPorComplemento(@PathVariable String complemento ) {
        return enderecoService.buscarPorComplemento(complemento);
    }

    @PostMapping(value = "/buscarPorBairro")
    @ResponseBody
    public List<Endereco> buscarPorBairro(@RequestBody Bairro e ) { return enderecoService.buscarPorBairro(e); }

    @GetMapping(value = "/{logradouro}")
    @ResponseBody
    public List<Endereco> buscarPorLogradouro(@PathVariable String logradouro ) {
        return enderecoService.buscarPorLogradouro(logradouro);
    }

    @GetMapping(value = "/{numero}")
    @ResponseBody
    public List<Endereco> buscarPorNumero(@PathVariable long numero) {
        return enderecoService.buscarPorNumero(numero);
    }

    @PostMapping(value = "/buscarPorPessoa")
    @ResponseBody
    public Endereco buscarPorPessoa(@RequestBody Pessoa e ) { return enderecoService.buscarPorPessoa(e); }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Endereco editarEndereco(@PathVariable long id, @RequestBody Endereco b) {
        return enderecoService.editarDados(id, b);
    }

}
