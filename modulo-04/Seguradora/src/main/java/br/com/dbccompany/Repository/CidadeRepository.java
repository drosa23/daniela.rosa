package br.com.dbccompany.Repository;

import java.util.List;

import br.com.dbccompany.Entity.Cidade;
import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Estado;

public interface CidadeRepository extends CrudRepository<Cidade, Long>{
	Cidade findByNome(String nome);
	List<Cidade> findByEstado(Estado estado);
	Cidade findByBairros(Bairro bairro);
}
