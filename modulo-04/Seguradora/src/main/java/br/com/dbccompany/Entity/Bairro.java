package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
public class Bairro {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "bairro_seq", sequenceName = "bairro_seq")
	@GeneratedValue(generator = "bairro_seq", strategy = GenerationType.SEQUENCE)

	private long id;

	private String nome;

	@ManyToOne
	@JoinColumn(name = "id_cidade")
	private Cidade cidade;

	@OneToMany(mappedBy = "bairro")
	private List<Endereco> enderecos = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

}
