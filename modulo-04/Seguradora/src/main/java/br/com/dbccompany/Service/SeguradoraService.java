package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;
import br.com.dbccompany.Repository.SeguradoraRepository;

@Service
public class SeguradoraService {
    @Autowired
    private SeguradoraRepository a;

    @Transactional(rollbackFor = Exception.class)
    public Seguradora salvar(Seguradora e) {
        return a.save(e);
    }

    public Optional<Seguradora> buscarPessoa(long id) {
        return a.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Seguradora editarDados(long id, Seguradora e) {
        e.setId(id);
        return salvar(e);
    }

    public List<Seguradora> allSeguradoras  (){
        return (List<Seguradora>) a.findAll();
    }

    public Seguradora buscarPorNome(String l){
        return a.findByNome(l);
    }

    public Seguradora buscarPorCnpj(long l){
        return a.findByCnpj(l);
    }

    public Seguradora buscarPorEnderecoSeguradoras(EnderecoSeguradoras l){
        return a.findByEnderecoSeguradora(l);
    }

    public Seguradora buscarPorServico(Servico l){
        return a.findByServicos(l);
    }

    public Seguradora buscarPorServicoContratado(ServicoContratado l){
        return a.findByServicoContratado(l);
    }






}
