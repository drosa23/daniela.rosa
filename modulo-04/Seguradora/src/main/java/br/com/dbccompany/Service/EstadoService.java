package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Repository.EstadoRepository;

@Service
public class EstadoService {
    @Autowired
    private EstadoRepository a;

    @Transactional(rollbackFor = Exception.class)
    public Estado salvar(Estado e) {
        return a.save(e);
    }

    public Optional<Estado> buscarEstado(long id) {
        return a.findById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public Estado editarDados(long id, Estado e) {
        e.setId(id);
        return salvar(e);
    }

    public List<Estado> allEstados(){
        return (List<Estado>) a.findAll();
    }

    public Estado buscarPorNome(String l){
        return a.findByNome(l);
    }

    public Estado buscarPorCidade(Cidade l){
        return a.findByCidades(l);
    }


}
