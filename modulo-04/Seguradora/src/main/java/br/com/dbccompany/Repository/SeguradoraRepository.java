package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.EnderecoSeguradoras;
import br.com.dbccompany.Entity.Seguradora;
import br.com.dbccompany.Entity.Servico;
import br.com.dbccompany.Entity.ServicoContratado;

public interface SeguradoraRepository extends CrudRepository<Seguradora, Long>{
	Seguradora findByNome(String nome);
	Seguradora findByCnpj(long cnpj);
	Seguradora findByEnderecoSeguradora(EnderecoSeguradoras end);
	Seguradora findByServicos(Servico servico);
	Seguradora findByServicoContratado(ServicoContratado ser);
}
