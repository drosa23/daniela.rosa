package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
public class Cidade {
	@Id
	@SequenceGenerator(allocationSize = 1, name = "cidade_seq", sequenceName = "cidade_seq")
	@GeneratedValue(generator = "cidade_seq", strategy = GenerationType.SEQUENCE)
	private long id;

	private String nome;

	@ManyToOne
	@JoinColumn(name = "id_Estado")
	private Estado estado;

	@OneToMany(mappedBy = "cidade")
	private List<Bairro> bairros = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Bairro> getBairros() {
		return bairros;
	}

	public void setBairros(List<Bairro> bairros) {
		this.bairros = bairros;
	}

}
