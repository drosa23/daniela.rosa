package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Pessoa;

public interface EnderecoRepository<E extends Endereco> extends CrudRepository<E, Long>{
	List<E> findByLogradouro(String logradouro);
	List<E> findByNumero(long numero);
	List<E> findByComplemento(String complemento);
	List<E> findByBairro(Bairro bairro);
	E findByPessoas(Pessoa pessoa);
}
