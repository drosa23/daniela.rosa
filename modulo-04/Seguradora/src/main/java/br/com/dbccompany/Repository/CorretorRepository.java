package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Corretor;
import br.com.dbccompany.Entity.ServicoContratado;

public interface CorretorRepository extends PessoaRepository<Corretor >{
	List<Corretor> findByCargo(String cargo);
	List<Corretor> findByComissao(double comissao);
	Corretor findByServicosContratados(ServicoContratado serCont);
}
