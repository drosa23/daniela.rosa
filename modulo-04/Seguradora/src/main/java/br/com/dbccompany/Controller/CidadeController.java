package br.com.dbccompany.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dbccompany.Entity.Bairro;
import br.com.dbccompany.Entity.Cidade;
import br.com.dbccompany.Entity.Estado;
import br.com.dbccompany.Service.CidadeService;


@Controller
@RequestMapping("/api/cidade")
public class CidadeController {

    @Autowired
    CidadeService cidadeService;

    @GetMapping(value = "/")
    @ResponseBody
    public List<Cidade> lstCidades(){
        return cidadeService.allCidades();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Cidade novaCidade(@RequestBody Cidade b) {
        return cidadeService.salvar(b);
    }

    @GetMapping(value = "/{nome}")
    @ResponseBody
    public Cidade cidadeEspecifico(@PathVariable String nome ) {
        return cidadeService.buscarPorNome(nome);
    }

    @PostMapping(value = "/buscarPorEstado")
    @ResponseBody
    public List<Cidade> buscarPorEstado(@RequestBody Estado e ) { return cidadeService.buscarPorEstado(e); }

    @PostMapping(value = "/buscarPorBairro")
    @ResponseBody
    public Cidade buscarPorBairro(@RequestBody Bairro e ) { return cidadeService.buscarPorBairro(e); }

    @PutMapping(value="/editar/{id}")
    @ResponseBody
    public Cidade editarCidade(@PathVariable long id, @RequestBody Cidade b) {
        return cidadeService.editarDados(id, b);
    }

}
