package br.com.dbc.cartoes.entity;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CARTAO")
public class Cartao {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "cartao_seq", sequenceName = "cartao_seq")
    @GeneratedValue(generator = "cartao_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private Integer chip;
    private Integer vencimento;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    @ManyToOne
    @JoinColumn(name = "id_bandeira")
    private Bandeira bandeira;

    @ManyToOne
    @JoinColumn(name = "id_emissor")
    private Emissor emissor;

    @OneToMany(mappedBy = "cartao", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentoList;


    public Cliente getCliente() {
        return cliente;
    }

    public Bandeira getBandeira() {
        return bandeira;
    }

    public Emissor getEmissor() {
        return emissor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getChip() {
        return chip;
    }

    public void setChip(Integer chip) {
        this.chip = chip;
    }

    public Integer getVencimento() {
        return vencimento;
    }

    public void setVencimento(Integer vencimento) {
        this.vencimento = vencimento;
    }
}
