package br.com.dbc.cartoes.entity;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "BANDEIRA")
public class Bandeira {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "bandeira_seq", sequenceName = "bandeira_seq")
    @GeneratedValue(generator = "bandeira_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    @Column(name = "TAXA", length = 6, nullable = false)
    private Double taxa;

    @OneToMany(mappedBy = "bandeira", cascade = CascadeType.ALL)
    private List<Cartao> cartao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }
}
