package br.com.dbc.cartoes.entity;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CREDENCIADOR")
public class Credenciador {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "credenciador_seq", sequenceName = "credenciador_seq")
    @GeneratedValue(generator = "credenciador_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    @OneToMany(mappedBy = "credenciador", cascade = CascadeType.ALL)
    private List<LojaCredenciador> lojaCredenciador;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<LojaCredenciador> getLojaCredenciador() {
        return lojaCredenciador;
    }

    public void setLojaCredenciador(List<LojaCredenciador> lojaCredenciador) {
        this.lojaCredenciador = lojaCredenciador;
    }
}
