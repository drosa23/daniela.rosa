package br.com.dbc.cartoes;

import br.com.dbc.cartoes.entity.HibernateUtil;
import br.com.dbc.cartoes.entity.Loja;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        Session session = null;
        Transaction transaction = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            
            Loja loja1 = new Loja();
            loja1.setNome("Zara");
            
            session.save(loja1);
            
            
            transaction.commit();
        } catch (Exception e) {
            if(transaction != null){
                transaction.rollback();
            }
            LOG.log(Level.SEVERE, e.getMessage(), e);
            System.exit(1);
        } finally {
            if(session != null){
                session.close();
                System.exit(0);
                
            }
            
        }
    }
}
