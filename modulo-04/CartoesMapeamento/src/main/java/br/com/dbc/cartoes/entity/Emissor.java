package br.com.dbc.cartoes.entity;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "EMISSOR")
public class Emissor {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "emissor_seq", sequenceName = "emissor_seq")
    @GeneratedValue(generator = "emissor_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    @Column(name = "TAXA", length = 6, nullable = false)
    private Double taxa;

    @OneToMany(mappedBy = "emissor", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentoList;
    private List<Cartao> cartao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public List<Lancamento> getLancamentoList() {
        return lancamentoList;
    }

    public List<Cartao> getCartao() {
        return cartao;
    }
}
