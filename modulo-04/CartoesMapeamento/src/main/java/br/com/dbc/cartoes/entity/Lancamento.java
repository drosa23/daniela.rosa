package br.com.dbc.cartoes.entity;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "LANCAMENTO")
public class Lancamento {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "lancamento_seq", sequenceName = "lancamento_seq")
    @GeneratedValue(generator = "lancamento_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String descricao;
    private Double valor;
    private Integer dataCompra;

    @ManyToOne
    @JoinColumn(name = "id_cartao")
    private Cartao cartao;

    @ManyToOne
    @JoinColumn(name = "id_loja")
    private Loja loja;

    @ManyToOne
    @JoinColumn(name = "id_emissor")
    private Emissor emissor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(Integer dataCompra) {
        this.dataCompra = dataCompra;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Emissor getEmissor() {
        return emissor;
    }

    public void setEmissor(Emissor emissor) {
        this.emissor = emissor;
    }
}
