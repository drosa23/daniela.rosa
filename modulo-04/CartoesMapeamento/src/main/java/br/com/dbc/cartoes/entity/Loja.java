package br.com.dbc.cartoes.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "LOJA")
public class Loja {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "loja_seq", sequenceName = "loja_seq")
    @GeneratedValue(generator = "loja_seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME", length = 100, nullable = false)
    private String nome;

    @OneToMany(mappedBy = "loja", cascade = CascadeType.ALL)
    private List<Lancamento> lancamentoList;
    private List<LojaCredenciador> lojaCredenciador;

    public List<Lancamento> getLancamentoList() {
        return lancamentoList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setLancamentoList(List<Lancamento> lancamentoList) {
        this.lancamentoList = lancamentoList;
    }

    public List<LojaCredenciador> getLojaCredenciador() {
        return lojaCredenciador;
    }

    public void setLojaCredenciador(List<LojaCredenciador> lojaCredenciador) {
        this.lojaCredenciador = lojaCredenciador;
    }
}
