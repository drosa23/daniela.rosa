package br.com.dbc.cartoes.entity;
import javax.persistence.*;

@Entity
@Table(name = "LOJA_CREDENCIADOR")
public class LojaCredenciador {
    private Double taxa;

    @ManyToOne
    @JoinColumn(name = "id_loja")
    private Loja loja;

    @ManyToOne
    @JoinColumn(name = "id_credenciador")
    private Credenciador credenciador;

    public Double getTaxa() {
        return taxa;
    }

    public void setTaxa(Double taxa) {
        this.taxa = taxa;
    }

    public Loja getLoja() {
        return loja;
    }

    public void setLoja(Loja loja) {
        this.loja = loja;
    }

    public Credenciador getCredenciador() {
        return credenciador;
    }

    public void setCredenciador(Credenciador credenciador) {
        this.credenciador = credenciador;
    }
}
