package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Documento;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.DocumentoRepository;

@Service
public class DocumentoService {
	
	@Autowired
	private DocumentoRepository documentoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Documento salvar(Documento documento) {
		return documentoRepository.save(documento);
	}
	
	public Optional<Documento> buscarDocumento(long id) {
		return documentoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Documento editarDados(long id, Documento documento) {
		documento.setId(id);
		return documentoRepository.save(documento);
	}
	
	public List<Documento> allBancos(){
		return (List<Documento>) documentoRepository.findAll();
	}
	
	public Documento buscarPorUsuario(Usuario usuario) {
		return documentoRepository.findByUsuarios(usuario);
	}
	
	public Documento buscarPorTipo(String tipo) {
		return documentoRepository.findByTipo(tipo);
	}
	
	public Documento buscarPorValor(String valor) {
		return documentoRepository.findByTipo(valor);
	}
	
}
