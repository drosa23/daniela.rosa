package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.ContatoRepository;

@Service
public class ContatoService {
	
	@Autowired
	private ContatoRepository contatoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Contato salvar(Contato contato) {
		return contatoRepository.save(contato);
	}
	
	public Optional<Contato> buscarBanco(long id) {
		return contatoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Contato editarDados(long id, Contato contato) {
		contato.setId(id);
		return contatoRepository.save(contato);
	}
	
	public List<Contato> allBancos(){
		return (List<Contato>) contatoRepository.findAll();
	}
	
	public Contato encontrarPorUsuario(Usuario usuario) {
		return contatoRepository.findByUsuarios(usuario);
	}
	
	public Contato encontrarPorTipo(String tipo) {
		return contatoRepository.findByTipo(tipo);
	}
	
	public Contato encontrarPorValor(String valor) {
		return contatoRepository.findByValor(valor);
	}
}
