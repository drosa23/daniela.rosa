package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.TipoMovimentacao;

public interface TipoMovimentacaoRepository extends CrudRepository<TipoMovimentacao, Long> {
	TipoMovimentacao findByTipo(String tipo);
	TipoMovimentacao findByMovimentacoes(Movimentacao movimentacao);
}
