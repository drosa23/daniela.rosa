package br.com.dbccompany.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.TipoMovimentacao;
import br.com.dbccompany.Repository.MovimentacaoRepository;

@Service
public class MovimentacaoService {
	@Autowired
	private MovimentacaoRepository movimentacaoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Movimentacao salvar(Movimentacao movimentacao) {
		return movimentacaoRepository.save(movimentacao);
	}
	
	public Optional<Movimentacao> buscarDocumento(long id) {
		return movimentacaoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Movimentacao editarDados(long id, Movimentacao movimentacao) {
		movimentacao.setId(id);
		return movimentacaoRepository.save(movimentacao);
	}
	
	public List<Movimentacao> allBancos(){
		return (List<Movimentacao>) movimentacaoRepository.findAll();
	}
	
	public Movimentacao buscarPorDataMovimentacao(Date data) {
		return movimentacaoRepository.findByDataMovimentacao(data);
	}
	
	public List<Movimentacao> buscarPorValor(long valor) {
		return movimentacaoRepository.findByValor(valor);
	}
	
	public Movimentacao buscarPorEmprestimo(Emprestimo emprestimo) {
		return movimentacaoRepository.findByEmprestimo(emprestimo);
	}
	
	public List<Movimentacao> buscarPorConta(Conta conta) {
		return movimentacaoRepository.findByConta(conta);
	}
	
	public List<Movimentacao> buscarPorTipoMovimentacao(TipoMovimentacao tipoMov) {
		return movimentacaoRepository.findByTipoMovimentacao(tipoMov);
	}

}
