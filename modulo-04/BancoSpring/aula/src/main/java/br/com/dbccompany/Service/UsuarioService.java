package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Usuario salvar(Usuario tpMov) {
		return usuarioRepository.save(tpMov);
	}
	
	public Optional<Usuario> buscarDocumento(long id) {
		return usuarioRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Usuario editarDados(long id, Usuario tpMov) {
		tpMov.setId(id);
		return usuarioRepository.save(tpMov);
	}
	
	public List<Usuario> allUsuario(){
		return (List<Usuario>) usuarioRepository.findAll();
	}
	
	public Usuario buscarPorNome(String nome) {
		return usuarioRepository.findByNome(nome);
	}
}
