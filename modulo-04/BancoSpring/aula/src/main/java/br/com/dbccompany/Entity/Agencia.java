package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name= "AGENCIA")
public class Agencia {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	private long codigo;
	
	@ManyToOne
    @JoinColumn(name = "id_banco")
    private Banco banco;
	
	@ManyToOne
    @JoinColumn(name = "id_endereco")
    private Endereco endereco;
	
	@ManyToMany(mappedBy = "agencias")
    private List<Usuario> usuarios = new ArrayList<>();
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	

	
	
	

}
