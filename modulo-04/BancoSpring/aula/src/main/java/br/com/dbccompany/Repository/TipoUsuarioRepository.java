package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Regra;
import br.com.dbccompany.Entity.TipoUsuario;
import br.com.dbccompany.Entity.Usuario;

public interface TipoUsuarioRepository extends CrudRepository<TipoUsuario, Long>{
	TipoUsuario findByUsuarios(Usuario usuario);
	TipoUsuario findByRegras(Regra regra);
}
