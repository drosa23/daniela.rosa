package br.com.dbccompany.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.TipoMovimentacao;

public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Long> {
	Movimentacao findByDataMovimentacao(Date data);
	List<Movimentacao> findByValor(long valor);
	Movimentacao findByEmprestimo(Emprestimo emprestimo);
	List<Movimentacao> findByConta(Conta conta);
	List<Movimentacao> findByTipoMovimentacao(TipoMovimentacao tipoMov);
	
}
