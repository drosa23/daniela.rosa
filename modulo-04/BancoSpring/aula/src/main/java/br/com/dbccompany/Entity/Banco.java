package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
public class Banco {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "CODIGO")
	private long codigo;
	
	@Column(name = "NOME")
	private String nome;
	
	@OneToMany(mappedBy = "banco")
    private List<Agencia> agencias = new ArrayList<>();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Agencia> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencia> agencias) {
		this.agencias = agencias;
	}

	
	
	
	
}
