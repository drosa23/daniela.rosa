package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.TipoMovimentacao;
import br.com.dbccompany.Repository.TipoMovimentacaoRepository;

@Service
public class TipoMovimentacaoService {
	
	@Autowired
	private TipoMovimentacaoRepository tipoMovimentacaoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public TipoMovimentacao salvar(TipoMovimentacao tpMov) {
		return tipoMovimentacaoRepository.save(tpMov);
	}
	
	public Optional<TipoMovimentacao> buscarDocumento(long id) {
		return tipoMovimentacaoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public TipoMovimentacao editarDados(long id, TipoMovimentacao tpMov) {
		tpMov.setId(id);
		return tipoMovimentacaoRepository.save(tpMov);
	}
	
	public List<TipoMovimentacao> allTiposMovimentacoes(){
		return (List<TipoMovimentacao>) tipoMovimentacaoRepository.findAll();
	}
	
	public TipoMovimentacao buscarPorMovimentacao(Movimentacao movimentacao) {
		return tipoMovimentacaoRepository.findByMovimentacoes(movimentacao);
	}
	
	public TipoMovimentacao buscarPorTipo(String tipo) {
		return tipoMovimentacaoRepository.findByTipo(tipo);
	}
}
