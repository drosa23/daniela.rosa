package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.TipoUsuario;
import br.com.dbccompany.Repository.TipoUsuarioRepository;

@Service
public class TipoUsuarioService {
	
	@Autowired
	private TipoUsuarioRepository tipoUsuarioRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public TipoUsuario salvar(TipoUsuario tpMov) {
		return tipoUsuarioRepository.save(tpMov);
	}
	
	public Optional<TipoUsuario> buscarDocumento(long id) {
		return tipoUsuarioRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public TipoUsuario editarDados(long id, TipoUsuario tpMov) {
		tpMov.setId(id);
		return tipoUsuarioRepository.save(tpMov);
	}
	
	public List<TipoUsuario> allTiposUsuarios(){
		return (List<TipoUsuario>) tipoUsuarioRepository.findAll();
	}
}
