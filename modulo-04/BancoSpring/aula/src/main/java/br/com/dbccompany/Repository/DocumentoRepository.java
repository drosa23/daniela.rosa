package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Documento;
import br.com.dbccompany.Entity.Usuario;

public interface DocumentoRepository extends CrudRepository<Documento, Long> {
	Documento findByUsuarios(Usuario usuario);
	Documento findByTipo(String tipo);
	Documento findByValor(String valor);
}
