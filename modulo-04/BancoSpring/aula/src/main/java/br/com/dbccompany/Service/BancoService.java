package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Repository.BancoRepository;

@Service
public class BancoService {
	
	@Autowired
	private BancoRepository bancoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Banco salvar(Banco banco) {
		return bancoRepository.save(banco);
	}
	
	public Optional<Banco> buscarBanco(long id) {
		return bancoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Banco editarDados(long id, Banco banco) {
		banco.setId(id);
		return bancoRepository.save(banco);
	}
	
	public List<Banco> allBancos(){
		return (List<Banco>) bancoRepository.findAll();
	}
	
	public Banco buscarPorCodigo(long codigo) {
		return bancoRepository.findByCodigo(codigo);
	}
}
