package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.EnderecoRepository;

@Service
public class EnderecoService {
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco salvar(Endereco endereco) {
		return enderecoRepository.save(endereco);
	}
	
	public Optional<Endereco> buscarDocumento(long id) {
		return enderecoRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco editarDados(long id, Endereco endereco) {
		endereco.setId(id);
		return enderecoRepository.save(endereco);
	}
	
	public List<Endereco> allBancos(){
		return (List<Endereco>) enderecoRepository.findAll();
	}
	
	public Endereco buscarPorUsuario(Usuario usuario) {
		return enderecoRepository.findByUsuarios(usuario);
	}
	
	public Endereco buscarPorAgencia(Agencia agencia) {
		return enderecoRepository.findByAgencias(agencia);
	}
	
	public Endereco buscarPorCidade(String cidade) {
		return enderecoRepository.findByCidade(cidade);
	}
	
	public Endereco buscarPorLogradouro(String logradouro) {
		return enderecoRepository.findByLogradouro(logradouro);
	}
}
