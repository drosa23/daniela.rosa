package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.Usuario;
import br.com.dbccompany.Repository.ContaRepository;

@Service
public class ContaService {
	
	@Autowired
	private ContaRepository contaRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Conta salvar(Conta conta) {
		return contaRepository.save(conta);
	}
	
	public Optional<Conta> buscarConta(long id) {
		return contaRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Conta editarDados(long id, Conta conta) {
		conta.setId(id);
		return contaRepository.save(conta);
	}
	
	public List<Conta> allContas(){
		return (List<Conta>) contaRepository.findAll();
	}
	
	public List<Conta> buscarPorUsuario(Usuario usuario){
		return contaRepository.findByUsuarios(usuario);
	}
	
	public Conta buscarPorEmprestimo(Emprestimo emprestimo) {
		return contaRepository.findByEmprestimos(emprestimo);
	}
	
	public Conta buscarPorMovimentacao(Movimentacao movimentacao) {
		return contaRepository.findByMovimentacoes(movimentacao);
	}
}
