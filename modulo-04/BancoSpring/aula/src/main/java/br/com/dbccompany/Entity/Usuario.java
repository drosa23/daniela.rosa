package br.com.dbccompany.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name= "CLIENTE")
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String nome;
	
	@ManyToMany
    @JoinTable(name = "agencia_usuario",
            joinColumns = {
                @JoinColumn(name = "id_usuario")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_agencia")})
    private List<Agencia> agencias = new ArrayList<>();
	
	@ManyToMany
    @JoinTable(name = "usuario_endereco",
            joinColumns = {
                @JoinColumn(name = "id_usuario")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_endereco")})
    private List<Endereco> enderecos = new ArrayList<>();
	
	@ManyToMany
    @JoinTable(name = "usuario_contato",
            joinColumns = {
                @JoinColumn(name = "id_usuario")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_contato")})
    private List<Contato> contatos = new ArrayList<>();
	
	@ManyToMany
    @JoinTable(name = "usuario_documento",
            joinColumns = {
                @JoinColumn(name = "id_usuario")},
            inverseJoinColumns = {
                @JoinColumn(name = "id_documento")})
    private List<Documento> documentos = new ArrayList<>();
	
	 
	 @ManyToMany
	 @JoinTable(name = "atribuicao",
	 	joinColumns = {
	 		@JoinColumn(name = "id_usuario")},
	        inverseJoinColumns = {
	        @JoinColumn(name = "id_tipo_usuario")})
	private List<TipoUsuario> tiposUsuario = new ArrayList<>(); 
	
	 //refere-se ao gerente ou gerente-geral que autoriza o emprestimo
	 @OneToMany(mappedBy = "usuario")
	private List<Emprestimo> emprestimosAprovados = new ArrayList<>();
	 
	 @ManyToMany
	 @JoinTable(name = "usuario_conta",
	 	joinColumns = {
	 		@JoinColumn(name = "id_usuario")},
	        inverseJoinColumns = {
	        @JoinColumn(name = "id_conta")})
	private List<Conta> contas = new ArrayList<>(); 
	 
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Agencia> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencia> agencias) {
		this.agencias = agencias;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}

	public List<Documento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<Documento> documentos) {
		this.documentos = documentos;
	}

	public List<TipoUsuario> getTiposUsuario() {
		return tiposUsuario;
	}

	public void setTiposUsuario(List<TipoUsuario> tiposUsuario) {
		this.tiposUsuario = tiposUsuario;
	}

	public List<Emprestimo> getEmprestimosAprovados() {
		return emprestimosAprovados;
	}

	public void setEmprestimosAprovados(List<Emprestimo> emprestimosAprovados) {
		this.emprestimosAprovados = emprestimosAprovados;
	}

	public List<Conta> getContas() {
		return contas;
	}

	public void setContas(List<Conta> contas) {
		this.contas = contas;
	}
	
	

	
}
