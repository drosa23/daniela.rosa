package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Usuario;

public interface ContatoRepository extends CrudRepository<Contato, Long> {
	Contato findByUsuarios(Usuario usuario);
	Contato findByTipo(String tipo);
	Contato findByValor(String valor);
}
