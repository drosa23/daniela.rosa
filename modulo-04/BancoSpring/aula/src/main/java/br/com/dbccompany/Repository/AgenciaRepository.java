package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Banco;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Usuario;

public interface AgenciaRepository extends CrudRepository<Agencia, Long>{
	Agencia findByCodigo(long codigo);
	Agencia findByUsuarios(Usuario usuario);
	Agencia findByEndereco(Endereco endereco);
	List<Agencia> findByBanco(Banco banco);
}
