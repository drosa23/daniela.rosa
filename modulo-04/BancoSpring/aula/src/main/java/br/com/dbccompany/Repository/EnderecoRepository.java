package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.Usuario;

public interface EnderecoRepository extends CrudRepository<Endereco, Long> {
	Endereco findByUsuarios(Usuario usuario);
	Endereco findByAgencias(Agencia agencia);
	Endereco findByCidade(String cidade);
	Endereco findByLogradouro(String logradouro);
}
