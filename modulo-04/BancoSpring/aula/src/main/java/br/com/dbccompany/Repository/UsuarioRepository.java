package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Agencia;
import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Contato;
import br.com.dbccompany.Entity.Documento;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Endereco;
import br.com.dbccompany.Entity.TipoUsuario;
import br.com.dbccompany.Entity.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	Usuario findByNome(String nome);
	List<Usuario> findByAgencias(Agencia agencia);
	Usuario findByEnderecos(Endereco endereco);
	Usuario findByContatos(Contato contato);
	Usuario findByDocumentos(Documento documento);
	List<Usuario> findByTiposUsuario(TipoUsuario tipoUsuario);
	Usuario findByEmprestimosAprovados(Emprestimo emprestimo);
	Usuario findByContas(Conta conta);
}
