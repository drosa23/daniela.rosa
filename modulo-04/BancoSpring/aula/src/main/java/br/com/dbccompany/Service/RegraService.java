package br.com.dbccompany.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Regra;
import br.com.dbccompany.Entity.TipoUsuario;
import br.com.dbccompany.Repository.RegraRepository;

@Service
public class RegraService {
	
	@Autowired
	private RegraRepository regraRepository;
	
	@Transactional(rollbackFor = Exception.class)
	public Regra salvar(Regra regra) {
		return regraRepository.save(regra);
	}
	
	public Optional<Regra> buscarDocumento(long id) {
		return regraRepository.findById(id);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Regra editarDados(long id, Regra regra) {
		regra.setId(id);
		return regraRepository.save(regra);
	}
	
	public List<Regra> allBancos(){
		return (List<Regra>) regraRepository.findAll();
	}
	
	public Regra buscarPorDescricao(String des) {
		return regraRepository.findByDescricao(des);
	}
	
	public Regra buscarPorTipoUsuario(TipoUsuario tipoUsuario) {
		return regraRepository.findByTipoUsuario(tipoUsuario);
	}
	
	public Regra buscarPorEmprestimo(Emprestimo emprestimo) {
		return regraRepository.findByEmprestimos(emprestimo);
	}
	
	
}
