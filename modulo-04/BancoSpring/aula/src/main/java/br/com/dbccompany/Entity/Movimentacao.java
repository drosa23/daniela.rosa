package br.com.dbccompany.Entity;

import java.util.Date;

import javax.persistence.*;

@Entity
public class Movimentacao {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;

	@Column(name = "data")
	private Date dataMovimentacao;

	private long valor;

	@ManyToOne
	@JoinColumn(name = "id_conta")
	private Conta conta;

	@ManyToOne
	@JoinColumn(name = "id_tipo_movimentacao")
	private TipoMovimentacao tipoMovimentacao;

	@OneToOne(mappedBy = "movimentacao")
	private Emprestimo emprestimo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDataMovimentacao() {
		return dataMovimentacao;
	}

	public void setDataMovimentacao(Date dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public TipoMovimentacao getTipoMovimentacao() {
		return tipoMovimentacao;
	}

	public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
		this.tipoMovimentacao = tipoMovimentacao;
	}

	public Emprestimo getEmprestimo() {
		return emprestimo;
	}

	public void setEmprestimo(Emprestimo emprestimo) {
		this.emprestimo = emprestimo;
	}
	
	

}
