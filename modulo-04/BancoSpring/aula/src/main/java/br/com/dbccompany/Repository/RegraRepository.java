package br.com.dbccompany.Repository;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Regra;
import br.com.dbccompany.Entity.TipoUsuario;

public interface RegraRepository extends CrudRepository<Regra, Long>{
	Regra findByDescricao(String descricao);
	Regra findByTipoUsuario(TipoUsuario tipoUsuario);
	Regra findByEmprestimos(Emprestimo emprestimo);

}
