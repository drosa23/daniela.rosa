package br.com.dbccompany.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.dbccompany.Entity.Conta;
import br.com.dbccompany.Entity.Emprestimo;
import br.com.dbccompany.Entity.Movimentacao;
import br.com.dbccompany.Entity.Usuario;

public interface ContaRepository extends CrudRepository<Conta, Long>{
	List<Conta> findByUsuarios(Usuario usuarios);
	List<Conta> findByUsuarios(List<Usuario> usuarios);
	Conta findByEmprestimos(Emprestimo emprestimo);
	List<Conta> findByEmprestimos(List<Emprestimo> emprestimo);
	Conta findByMovimentacoes(Movimentacao movimentacao);
	List<Conta> findByMovimentacoes(List<Movimentacao> movimentacao);
}
