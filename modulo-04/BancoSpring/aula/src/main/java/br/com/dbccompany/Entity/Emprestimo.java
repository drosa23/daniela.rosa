package br.com.dbccompany.Entity;

import javax.persistence.*;

@Entity
public class Emprestimo {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	private long valor;
	
	@ManyToOne
    @JoinColumn(name = "id_conta")
    private Conta conta;
	
	@ManyToOne
    @JoinColumn(name = "id_movimentacao")
    private Movimentacao movimentacao;
	
	@ManyToOne
    @JoinColumn(name = "id_regra")
    private Regra regra;
	
	//pessoa que aprova
	@ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getValor() {
		return valor;
	}

	public void setValor(long valor) {
		this.valor = valor;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	public Movimentacao getMovimentacao() {
		return movimentacao;
	}

	public void setMovimentacao(Movimentacao movimentacao) {
		this.movimentacao = movimentacao;
	}

	public Regra getRegra() {
		return regra;
	}

	public void setRegra(Regra regra) {
		this.regra = regra;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
